// Import the functions from the SDKs
import { initializeApp, getApp, getApps } from "firebase/app";
import { getFirestore } from "firebase/firestore";
import { getStorage } from "firebase/storage";

// Web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBzzt8bpURJGtXGBSh_RJMgzAlxtXCW-Xs",
  authDomain: "bwv-webapp.firebaseapp.com",
  projectId: "bwv-webapp",
  storageBucket: "bwv-webapp.appspot.com",
  messagingSenderId: "182217035318",
  appId: "1:182217035318:web:0c537515179bd6aa11d297",
  measurementId: "G-055GXSS0HB"
};

// Initialize Firebase
const app = !getApps().length ? initializeApp(firebaseConfig) : getApp();
const db = getFirestore();
const storage = getStorage();

export default app;
export { db, storage };