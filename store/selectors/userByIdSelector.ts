import { selectorFamily } from "recoil";
import { usersState } from "../atoms/usersAtom";

export const getUserById = selectorFamily({
  key: 'GetUserById',
  get: (userId) => ({get}) => {
    const users = get(usersState)
    return users.filter(user => user.id === userId)[0]
  }
})