import { atom } from 'recoil';
import { AdRequestParams } from "../../common/models";

export const adsHistoryRequestState = atom<AdRequestParams>({
  key: 'adsHistoryRequestParams',
  default: {
    userId: null,
    adStatus: 0,
    pageNumber: 1,
    pageSize: 10
  }
})