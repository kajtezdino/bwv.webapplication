import { atom } from 'recoil';
import { INewsletter } from '../../common/models';

const newslettersState = atom<INewsletter[]>({
  key: 'newsletters',
  default: [],
})

export default newslettersState;