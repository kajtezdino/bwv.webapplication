import { atom } from 'recoil';
import { IFuture } from '../../common/models';

export const futuresState = atom<IFuture[]>({
  key: 'futures',
  default: [],
})