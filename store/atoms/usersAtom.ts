import { atom, selector } from 'recoil';
import { IUser } from '../../common/models';

export const usersState = atom<IUser[]>({
  key: 'users',
  default: [],
})