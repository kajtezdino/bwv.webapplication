import { atom } from 'recoil';
import { AdsResponse } from '../../common/models';

export const adsHistoryState = atom<AdsResponse>({
  key: 'adsHistory',
  default: {
    paging: {
      currentPage: 1,
      totalPages: 1,
      pageSize: 10,
      totalCount: 0,
      hasPrevious: false,
      hasNext: false
    },
    ads: []
  },
})