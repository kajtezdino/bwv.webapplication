import { atom } from 'recoil';
import { ITypesGroupsAndConditions } from '../../common/models';

const defaultSharedObjectValues = [{
  id: '',
  name: ''
}]

export const typesGroupsAndConditions = atom<ITypesGroupsAndConditions>({
  key: 'typesGroupsAndConditions',
  default: {
    adGroups: defaultSharedObjectValues,
    adConditions: defaultSharedObjectValues,
    adTypes: defaultSharedObjectValues
  },
})