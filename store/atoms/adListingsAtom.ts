import { atom } from 'recoil';
import { IBuyAndSellListingsResponse } from '../../common/models';

const adListingsState = atom<IBuyAndSellListingsResponse>({
  key: 'ad-listings',
  default: {
    ads: [],
    totalNumber: 0
  },
})

export default adListingsState