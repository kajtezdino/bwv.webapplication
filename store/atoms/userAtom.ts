import { atom } from 'recoil';

export const userState: any = atom({
  key: 'userState',
  default: null
})