import { atom } from 'recoil';
import { IMarket } from '../../common/models';

export const marketsState = atom<IMarket[]>({
  key: 'markets',
  default: [],
})