import { atom } from 'recoil';
import { IBuyAndSellListingsFilters } from '../../common/models';

const adListingsFiltersState = atom<IBuyAndSellListingsFilters>({
  key: 'ad-listings-filters',
  default: {},
})

export default adListingsFiltersState