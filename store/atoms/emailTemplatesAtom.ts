import { atom } from 'recoil';
import { IEmailTemplate } from '../../common/models';

const emailTemplatesState = atom<IEmailTemplate[]>({
  key: 'templates',
  default: [],
})

export default emailTemplatesState