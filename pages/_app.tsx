import '../styles/globals.css'
import type { AppProps } from 'next/app'
import { SessionProvider, useSession } from "next-auth/react";
import { RecoilRoot } from 'recoil';
import { NextPageWithLayout } from '../common/models';
import { useEffect } from 'react';
import { useRouter } from 'next/router';
import { ExclamationIcon } from '@heroicons/react/solid';
import { resolveValue, Toaster } from 'react-hot-toast';
import { Transition } from '@headlessui/react';

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout<any> & { auth: any}
}

function Auth({ auth, children }: any) {
  const router = useRouter();
  const { data: session, status } = useSession()
  const isUser = !!session?.user
  const userSession: any = session;

  useEffect(() => {
    if (status === "loading") return
    if (!isUser) router.push('/auth/sign-in')
  }, [status, isUser, router])

  if (isUser) {
    if (auth.roles && !auth.roles.includes(userSession?.user?.roles)) {
      return (
        <div className="w-screen h-screen flex justify-center items-center">
          <div className="flex items-center gap-6">
            <ExclamationIcon
              width={48}
              height={48}
            />
            <p className="text-lg"><span className="font-semibold">404</span> Page Not Found</p>
          </div>
        </div>
      )
    }

    return children
  }

  return <></>
}

function MyApp({
  Component,
  pageProps: { session, ...pageProps }
}: AppPropsWithLayout) {
  // Use the layout defined at the page level, if available
  const getLayout = Component.getLayout ?? ((page) => page)

  return (
    <SessionProvider session={session}>
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSvYmKPMI5qLRJxahnaTBCiAs6RUhzOm8&libraries=places" async></script>
      <RecoilRoot>
        {
          Component.auth
            ? <Auth auth={Component.auth}>{getLayout(<Component {...pageProps} />)}</Auth>
            : getLayout(<Component {...pageProps} />)
        }
        <Toaster
          position="bottom-right"
          reverseOrder={true}
          gutter={16}
          toastOptions={{
            duration: 5000,
          }}
        >
          {(t) => (
            <Transition
              appear
              show={t.visible}
              className="transform p-0"
              enter="transition-all duration-150"
              enterFrom="opacity-0 scale-50"
              enterTo="opacity-100 scale-100"
              leave="transition-all duration-150"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-75"
            >
              {resolveValue(t.message, t)}
            </Transition>
          )}
        </Toaster>
        {/* <Notifications /> */}
      </RecoilRoot>
    </SessionProvider>
  )
}

export default MyApp
