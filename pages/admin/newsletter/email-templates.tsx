import Head from 'next/head';
import { ReactElement } from 'react';
import MainLayout from '../../../common/layouts/MainLayout';
import { NextPageWithLayout } from '../../../common/models';
import EmailTemplatesContent from '../../../modules/admin/newsletter/email-templates';

const EmailTemplates: NextPageWithLayout<null> = () => {
  return (
    <div className="flex w-full min-h-screen align-middle justify-center">
      <Head>
        <title>BWV</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <EmailTemplatesContent />
    </div>
  )
}

EmailTemplates.auth = true
EmailTemplates.getLayout = function getLayout(page: ReactElement) {
  return (
    <MainLayout title="Newsletter">
      {page}
    </MainLayout>
  )
}

export default EmailTemplates;