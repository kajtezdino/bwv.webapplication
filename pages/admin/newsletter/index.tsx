import Head from 'next/head';
import { ReactElement } from 'react';
import MainLayout from '../../../common/layouts/MainLayout';
import { NextPageWithLayout } from '../../../common/models';
import NewsletterContent from '../../../modules/admin/newsletter';

const Newsletter: NextPageWithLayout<null> = () => {

  return (
    <div className="flex w-full h-full align-middle justify-center">
      <Head>
        <title>BWV</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <NewsletterContent />
    </div>
  )
}

Newsletter.auth = true
Newsletter.getLayout = function getLayout(page: ReactElement) {
  return (
    <MainLayout title="Newsletter">
      {page}
    </MainLayout>
  )
}

export default Newsletter;