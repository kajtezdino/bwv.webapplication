import Head from 'next/head';
import { ReactElement } from 'react';
import MainLayout from '../../../common/layouts/MainLayout';
import { NextPageWithLayout } from '../../../common/models';
import ActivityFeedContent from '../../../modules/admin/newsletter/activity-feed';

const ActivityFeed: NextPageWithLayout<null> = () => {
  return (
    <div className="flex w-full min-h-screen align-middle justify-center">
      <Head>
        <title>BWV</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <ActivityFeedContent />
    </div>
  )
}

ActivityFeed.auth = true
ActivityFeed.getLayout = function getLayout(page: ReactElement) {
  return (
    <MainLayout title="Newsletter">
      {page}
    </MainLayout>
  )
}

export default ActivityFeed;