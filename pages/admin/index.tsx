import Head from 'next/head';
import { NextPageWithLayout } from '../../common/models';
import { ReactElement } from 'react';
import { MainAdminLayout } from '../../common/layouts';
import { UserRoles } from '../../common/enums';

const AdminDashboard: NextPageWithLayout<null> = () => {
  return (
    <div className="flex min-h-screen align-middle justify-center">
      <Head>
        <title>BWV</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
    </div>
  )
}

AdminDashboard.auth = {
  roles: [UserRoles.ADMIN]
}
AdminDashboard.getLayout = function getLayout(page: ReactElement) {
  return (
    <MainAdminLayout title="Analytics">
      {page}
    </MainAdminLayout>
  )
}

export default AdminDashboard;