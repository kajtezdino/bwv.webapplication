import { ReactElement } from "react";
import Head from "next/head";
import { NextPageWithLayout } from "../../../common/models";
import { MainAdminLayout } from "../../../common/layouts";
import AdminUsersIdContent from '../../../modules/admin/users/[id]';
import useUser from "../../../modules/admin/users/hooks/useUser";
import { useRouter } from "next/router";
import { UserRoles } from "../../../common/enums";

const UserInfo: NextPageWithLayout<null> = () => {
  const router = useRouter()
  const { id } = router.query
  const { data } = useUser(id as string)

  return(
    <div className="flex w-full px-8 py-10 justify-center">
      <Head>
        <title>BWV User Profile {data?.firstName + " " + data?.lastName}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      
      <AdminUsersIdContent />
    </div>
  )
}

UserInfo.auth = {
  roles: [UserRoles.ADMIN]
}
UserInfo.getLayout = function getLayout(page: ReactElement) {
  return (
    <MainAdminLayout title="User Management">
      {page}
    </MainAdminLayout>
  )
}

export default UserInfo;