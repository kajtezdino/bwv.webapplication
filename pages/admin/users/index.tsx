import { ReactElement } from 'react';
import Head from 'next/head';
import { NextPageWithLayout } from '../../../common/models';
import {
  MainAdminLayout
} from '../../../common/layouts';
import AdminUsersContent from '../../../modules/admin/users'
import { UserRoles } from '../../../common/enums';

const AdminUsers: NextPageWithLayout<null> = () => {
  return (
    <div className="lex w-full min-h-[calc(100vh-130px)] px-8 py-10">
      <Head>
        <title>BWV</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      
      <AdminUsersContent />
    </div>
  )
}

AdminUsers.auth = {
  roles: [UserRoles.ADMIN]
}
AdminUsers.getLayout = function getLayout(page: ReactElement) {
  return (
    <MainAdminLayout title="User Management">
      {page}
    </MainAdminLayout>
  )
}

export default AdminUsers;
