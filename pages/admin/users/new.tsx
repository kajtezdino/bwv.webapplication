import Head from 'next/head'
import { ReactElement } from "react";
import { MainAdminLayout } from "../../../common/layouts";
import { UserRoles } from '../../../common/enums';
import { NextPageWithLayout } from "../../../common/models";
import AdminUsersNewContent from '../../../modules/admin/users/new';

const NewUser: NextPageWithLayout<null> = () => {
  return (
    <div className="flex w-full px-8 py-10 justify-center">
      <Head>
        <title>BWV Add New User</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      
      <AdminUsersNewContent />
    </div>
  )
}

NewUser.auth = {
  roles: [UserRoles.ADMIN]
}
NewUser.getLayout = function getLayout(page: ReactElement) {
  return (
    <MainAdminLayout title="User Management">
      {page}
    </MainAdminLayout>
  )
}

export default NewUser;