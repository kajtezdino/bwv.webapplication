import Head from 'next/head';
import { ReactElement } from 'react';
import { UserRoles } from '../common/enums';
import MainLayout from '../common/layouts/MainLayout';
import { NextPageWithLayout } from '../common/models';
import FeedContent from '../modules/feed';

const Home: NextPageWithLayout<null> = () => {
  return (
    <div className="flex min-h-screen align-middle justify-center">
      <Head>
        <title>BWV</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <FeedContent />
    </div>
  )
}

Home.auth = {
  roles: [UserRoles.SUBSCRIBER]
}
Home.getLayout = function getLayout(page: ReactElement) {
  return (
    <MainLayout title="Hi user!">
      {page}
    </MainLayout>
  )
}

export default Home;
