import Head from "next/head";
import { ReactElement } from "react";
import AuthLayout from "../../common/layouts/AuthLayout";
import { NextPageWithLayout } from "../../common/models";
import AuthPasswordResetContent from "../../modules/auth/password-reset";

const PasswordReset: NextPageWithLayout<null> = () => {
  return (
    <>
      <Head>
        <title>BWV</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <AuthPasswordResetContent />
    </>
  )
}

PasswordReset.getLayout = function getLayout(page: ReactElement) {
  return (
    <AuthLayout>
      {page}
    </AuthLayout>
  )
}

export default PasswordReset