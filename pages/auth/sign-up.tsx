import Head from "next/head";
import { ReactElement } from "react";
import AuthLayout from "../../common/layouts/AuthLayout";
import {
  NextPageWithLayout,
} from "../../common/models";
import AuthSignUpContent from "../../modules/auth/sign-up";

const SignUp: NextPageWithLayout<null> = () => {
  return (
    <>
      <Head>
        <title>BWV</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <AuthSignUpContent />
    </>
  )
}

SignUp.getLayout = function getLayout(page: ReactElement) {
  return (
    <AuthLayout>
      {page}
    </AuthLayout>
  )
}

export default SignUp;