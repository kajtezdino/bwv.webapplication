import Head from "next/head";
import { ReactElement } from "react";
import AuthLayout from "../../common/layouts/AuthLayout";
import { NextPageWithLayout } from "../../common/models";
import AuthSignInContent from "../../modules/auth/sign-in";

const SignIn: NextPageWithLayout<null> = () => {
  return (
    <>
      <Head>
        <title>BWV</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <AuthSignInContent />
    </>
  )
}

SignIn.getLayout = function getLayout(page: ReactElement) {
  return (
    <AuthLayout>
      {page}
    </AuthLayout>
  )
}

export default SignIn;