import Head from 'next/head';
import { ReactElement } from 'react';
import MainLayout from '../../common/layouts/MainLayout';
import { NextPageWithLayout } from '../../common/models';
import AddNewListingContent from '../../modules/buy-and-sell/AddNewListingContent';


const AddNewListing: NextPageWithLayout<null> = () => {
  return (
    <div className="flex min-h-screen align-middle justify-center m-16">
      <Head>
        <title>BWV-Add New Listing</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <AddNewListingContent />
    </div>
  )
}

AddNewListing.auth = true
AddNewListing.getLayout = function getLayout(page: ReactElement) {
  return (
    <MainLayout title="Add New Listing">
      {page}
    </MainLayout>
  )
}

export default AddNewListing;