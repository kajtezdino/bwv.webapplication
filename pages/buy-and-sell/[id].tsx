import Head from 'next/head';
import { useRouter } from 'next/router';
import { ReactElement, useEffect, useState } from 'react';
import { UserRoles } from '../../common/enums';
import MainLayout from '../../common/layouts/MainLayout';
import { NextPageWithLayout } from '../../common/models';
import BuyAndSellAdContent from '../../modules/buy-and-sell/[id]';
import BuyAndSellApiService from '../../modules/buy-and-sell/api.service';

const BuyAndSellAd: NextPageWithLayout<{ adMeta: any }> = ({ adMeta }) => {
  const router = useRouter()
  const { id } = router.query
  const [ad, setAd] = useState<any>();

  useEffect(() => {
    getAdDetails()
  }, [])

  const getAdDetails = async () => {
    try {
      setAd(await BuyAndSellApiService.getListingDetails(id as string))
    } catch (error: any) {
      console.error(error)
    }
  }
  
  return (
    <div className="min-h-screen w-full p-12">
      <Head>
        <title>
          BWV Ad Listing {adMeta.title}
        </title>
        <link rel="icon" href="/favicon.ico" />
        <meta name="title" content={`BWV Ad Listing ${adMeta.title}`} />
        <meta name="description" content={adMeta?.description} />

        {/* Open Graph / Facebook */}
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://bwv-webapplication-git-master-kajtezdino.vercel.app/" />
        <meta property="og:title" content={`BWV Ad Listing ${adMeta.title}`} />
        <meta property="og:description" content={adMeta?.description} />
        <meta property="og:image" content={adMeta?.coverImageUrl || ""} />

        {/* Twitter */}
        <meta property="twitter:card" content="summary_large_image" />
        <meta property="twitter:url" content="https://bwv-webapplication-git-master-kajtezdino.vercel.app/" />
        <meta property="twitter:title" content={`BWV Ad Listing ${adMeta.title}`} />
        <meta property="twitter:description" content={adMeta?.description} />
        <meta property="twitter:image" content={adMeta?.coverImageUrl || ""} />
      </Head>

      {
        ad && <BuyAndSellAdContent ad={ad} />
      }
    </div>
  )
}

BuyAndSellAd.auth = {
  roles: [UserRoles.SUBSCRIBER]
}
BuyAndSellAd.getLayout = function getLayout(page: ReactElement) {
  return (
    <MainLayout title="Buy & Sell Ad">
      {page}
    </MainLayout>
  )
}

export default BuyAndSellAd;

export async function getServerSideProps(context: any) {
  const ad = await BuyAndSellApiService.getListingDetails(context.query.id, process.env.NEXT_BASE_DOMAIN_URL)
  
  return {
    props: {
      adMeta: ad,
    },
  };
}
