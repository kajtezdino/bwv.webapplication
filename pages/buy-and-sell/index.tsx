import Head from 'next/head';
import { ReactElement } from 'react';
import { UserRoles } from '../../common/enums';
import MainLayout from '../../common/layouts/MainLayout';
import { NextPageWithLayout } from '../../common/models';
import BuyAndSellContent from '../../modules/buy-and-sell';

const BuyAndSell: NextPageWithLayout<null> = () => {
  return (
    <div className="min-h-screen w-full p-12">
      <Head>
        <title>BWV</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <BuyAndSellContent />
    </div>
  )
}

BuyAndSell.auth = {
  roles: [UserRoles.SUBSCRIBER]
}
BuyAndSell.getLayout = function getLayout(page: ReactElement) {
  return (
    <MainLayout title="Buy & Sell">
      {page}
    </MainLayout>
  )
}

export default BuyAndSell;
