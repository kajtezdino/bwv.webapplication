import AdsTable from "../../modules/admin/ads/components/AdsTable";
import { ReactElement, useEffect } from "react";
import MainLayout from "../../common/layouts/MainLayout";
import { AdsResponse, NextPageWithLayout } from "../../common/models";
import adsHistoryService from "../../modules/admin/ads/ads-history.service";
import { useRecoilState } from "recoil";
import { adsHistoryRequestState } from "../../store/atoms/adsHistoryRequestAtom";
import { adsHistoryState } from "../../store/atoms/adsHistoryAtom";

const AdsManagement: NextPageWithLayout<null>  = () => {
  const [, setAdsHistory] = useRecoilState(adsHistoryState)
  const [adRequestParams] = useRecoilState(adsHistoryRequestState)
  useEffect(() => {
    adsHistoryService.fetchAds(adRequestParams)
      .then((res: AdsResponse)=> {
        setAdsHistory(res)
      })
  }, [adRequestParams])


  return (
    <div className="min-h-screen w-full px-12">
      <AdsTable />
    </div>
  )
};

AdsManagement.getLayout = function getLayout(page: ReactElement) {
  return (
    <MainLayout title="Ads Management">
      {page}
    </MainLayout>
  )
}

export default AdsManagement;