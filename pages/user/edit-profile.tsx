import Head from "next/head";
import { ReactElement } from "react";
import MainLayout from "../../common/layouts/MainLayout";
import { NextPageWithLayout } from "../../common/models";
import UserEditProfileContent from "../../modules/user/edit-profile";

const EditProfile: NextPageWithLayout<null> = () => {
  return (
    <div className="flex w-full min-h-[calc(100vh-130px)] px-8 py-10 justify-center">
      <Head>
        <title>Edit Profile Page</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      
      <UserEditProfileContent />
    </div>
  )
}

EditProfile.auth = true
EditProfile.getLayout = function getLayout(page: ReactElement) {
  return (
    <MainLayout title="Hi User!">
      {page}
    </MainLayout>
  )
}

export default EditProfile;