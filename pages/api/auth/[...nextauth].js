import NextAuth from "next-auth";
import CredentialsProvider from "next-auth/providers/credentials";
import AuthApiService from '../../../modules/auth/api.service';
import UserProfileApiService from '../../../modules/user/api.service';
import jwt_decode from 'jwt-decode';

export default NextAuth({
  secret: process.env.NEXTAUTH_SECRET,
  providers: [
    CredentialsProvider({
      name: 'Custom email login',
      authorize: async (credentials) => {
        try {
          const user = await AuthApiService.login({
            url: process.env.NEXT_BASE_DOMAIN_URL,
            email: credentials.email,
            password: credentials.password,
            rememberMe: credentials.rememberMe
          })
          const userProfile  = await UserProfileApiService.getUserProfile(user.userId, process.env.NEXT_BASE_DOMAIN_URL)

          return {
            name: userProfile.firstName + ' ' + userProfile.lastName,
            firstName: userProfile.firstName,
            lastName: userProfile.lastName,
            accessToken: user.accessToken,
            userId: user.userId,
            email: credentials.email,
            image: '',
            roles: jwt_decode(user.accessToken).roles
          }
        } catch (e) {
          throw new Error(e)
        }
      }
    })
  ],
  callbacks: {
    jwt: async ({ token, user }) => {
      user && (token.user = user)
      return token
    },
    session: async ({ session, token }) => {
      session.user = token.user
      session.token = token.user.accessToken
      return session;
    },
  }
});