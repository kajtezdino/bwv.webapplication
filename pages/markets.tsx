import Head from 'next/head';
import { ReactElement } from 'react';
import MainLayout from '../common/layouts/MainLayout';
import { NextPageWithLayout } from '../common/models';
import MarketsContent from '../modules/markets';

const Markets: NextPageWithLayout<null> = () => {
  return (
    <div className="flex min-h-screen align-middle justify-center">
      <Head>
        <title>BWV - Markets</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <MarketsContent />

    </div>
  )
}

Markets.auth = true
Markets.getLayout = function getLayout(page: ReactElement) {
  return (
    <MainLayout title="Markets">
      {page}
    </MainLayout>
  )
}

export default Markets;