import Head from "next/head";
import { useRouter } from "next/router"
import { ReactElement, useEffect, useState } from "react"
import { MainLayout } from "../../common/components"
import { NextPageWithLayout } from "../../common/models"
import FeedPostContent from "../../modules/feed/[id]";
import { doc, getDoc, onSnapshot } from "firebase/firestore";
import { db } from "../../firebase";

const FeedPost: NextPageWithLayout<{ postMeta: any, postUser: any }> = ({ postMeta, postUser }) => {
  const router = useRouter()
  const { id } = router.query
  const [post, setPost] = useState<any>();

  useEffect(
    () => {
      if (!db || !id) return 

      onSnapshot(doc(db, "posts", id as string), (snapshot) => {
        setPost(snapshot.data());
      }) 
    },
    [id]
  );

  return(
    <div className="flex min-h-screen align-middle justify-center">
      <Head>
        <title>
          {postUser.firstName} on BWV feed reel
        </title>
        <link rel="icon" href="/favicon.ico" />
        <meta name="title" content={`${postUser.firstName} on BWV feed reel`} />
        <meta name="description" content={postMeta?.text} />

        {/* Open Graph / Facebook */}
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://bwv-webapplication-git-master-kajtezdino.vercel.app/" />
        <meta property="og:title" content={`${postUser.firstName} on BWV feed reel`} />
        <meta property="og:description" content={postMeta?.text} />
        <meta property="og:image" content={postMeta?.video || (postMeta?.images && postMeta?.images[0]) || ""} />

        {/* Twitter */}
        <meta property="twitter:card" content="summary_large_image" />
        <meta property="twitter:url" content="https://bwv-webapplication-git-master-kajtezdino.vercel.app/" />
        <meta property="twitter:title" content={`${postUser.firstName} on BWV feed reel`} />
        <meta property="twitter:description" content={postMeta?.text} />
        <meta property="twitter:image" content={postMeta?.video || (postMeta?.images && postMeta?.images[0]) || ""} />
      </Head>
      {
        post && <FeedPostContent id={id as string} post={post} />
      }
    </div>
  )
}

FeedPost.getLayout = function getLayout(page: ReactElement) {
  return (
    <MainLayout title="Feed">
      {page}
    </MainLayout>
  )
}

export default FeedPost;

export async function getServerSideProps(context: any) {
  const docRef = doc(db, "posts", context.query.id);
  const docSnap = await getDoc(docRef);

  const userDocRef = doc(db, "users", (docSnap.data() as any).userId);
  const userDocSnap = await getDoc(userDocRef);

  return {
    props: {
      postMeta: JSON.parse(JSON.stringify(docSnap.data() as any)),
      postUser: JSON.parse(JSON.stringify(userDocSnap.data() as any)),
    },
  };
}