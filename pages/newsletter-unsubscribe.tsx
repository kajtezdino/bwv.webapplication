import Head from "next/head"
import { NextPageWithLayout } from "../common/models"
import NewsletterUnsubscribeContent from "../modules/newsletter/components/NewsletterUnsubscribeContent"

const NewsletterUnsubscribe: NextPageWithLayout<null> = () => {
  return (
    <div className="flex min-h-screen align-middle justify-center">
      <Head>
        <title>BWV - Newsletter Unsubscribe</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <NewsletterUnsubscribeContent />

    </div>
  )
}

export default NewsletterUnsubscribe;