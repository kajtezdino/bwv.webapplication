/** @type {import('next').NextConfig} */
module.exports = {
  images: {
    domains: [
      'www.deere.co.in',
      'firebasestorage.googleapis.com',
      'us-east-2-production-thumbnail-bucket.s3.amazonaws.com',
      'bwvstoragedev.blob.core.windows.net',
      'devbwvstorage.blob.core.windows.net'
    ],
  },
  reactStrictMode: true,
  async rewrites() {
    return [
      {
        source: '/apis/:path*',
        destination: process.env.NEXT_PUBLIC_BASE_API_URL + '/api/:path*'
      },
      {
        source: '/email-api/:path*',
        destination: process.env.NEXT_PUBLIC_EMAIL_API_URL + '/api/:path*'
      },
      {
        source: '/buy-sell-api/:path*',
        destination: process.env.NEXT_PUBLIC_BUY_SELL_API_URL + '/api/:path*'
      }
    ]
  }
}