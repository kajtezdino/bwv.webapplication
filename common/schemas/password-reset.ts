import * as yup from "yup";

export const PasswordResetSchema = yup.object({
  email: yup.string().email('Must be a valid email').required('Email is required'),
}).required();

export const NewPasswordSchema = yup.object({
  password: yup.string().required('Password is required'),
  confirmPassword: yup.string().required().oneOf([yup.ref('password')], 'Passwords does not match'),
}).required();