import * as yup from "yup";

export const SignUpSchema = yup.object({
  email: yup.string().email('Must be a valid email').required('Email is required'),
  firstName: yup.string().required('Name is required'),
  lastName: yup.string(),
  password: yup.string().required('Password is required'),
  confirmPassword: yup.string().required('Confirm password is required').oneOf([yup.ref('password')], 'Passwords does not match'),
  acceptTermsAndConditions: yup.boolean().required().oneOf([true], 'Terms and conditions must be checked'),
}).required();