import * as yup from "yup";

export const NewsletterActivityFeedShema = yup.object({
  searchQuery: yup.string(),
  from: yup.date(),
  to: yup.date()
}).required();