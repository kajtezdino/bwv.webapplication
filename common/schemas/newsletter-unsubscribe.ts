import * as yup from "yup";

export const NewsletterUnsubscribeShema = yup.object({
  emailAddress: yup.string().email('Must be a valid email').required('Email is required'),
  predefinedReasons: yup.string().required()
}).required();