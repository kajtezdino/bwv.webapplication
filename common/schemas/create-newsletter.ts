import * as yup from "yup";

export const CreateNewsletterSchema = yup.object({
  name: yup.string().required("Newsletter name is a required field"),
  subject: yup.string().required("Newsletter subject is a required field"),
}).required();