import * as yup from "yup";

export const AddNewUserSchema = yup.object({
  email: yup.string().email('Must be a valid email').required('Email is required'),
  firstName: yup.string().required('Name is required'),
  lastName: yup.string(),
  password: yup.string().required('Password is required'),
  confirmPassword: yup.string().required().oneOf([yup.ref('password')], 'Passwords does not match'),
  gender: yup.string().required(),
  role: yup.string().required(),
}).required();