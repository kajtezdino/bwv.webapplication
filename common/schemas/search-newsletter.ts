import * as yup from "yup";

export const SearchNewsletterSchema = yup.object({
  searchNewsletter: yup.string(),
}).required();