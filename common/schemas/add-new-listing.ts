import * as yup from "yup";

export const AddNewListingSchema = yup.object({
  title: yup.string().required("Title is required !"),
  adConditionID: yup.string().required("Condition is required !"),
  description: yup.string().required("Description is required !"),
  adTypeID: yup.string().required("Ad type is required !"),
  adGroupID: yup.string().required("Ad group is required !"),
  price: yup.number().when('adGroupID',(adGroupID,schema) => {
    return adGroupID === "227e3c70-23b3-4073-9876-80a46c21c849" ? schema.required('Price is required') : schema
  }),
  categoryID: yup.string().required("Category is required !"),
  subcategoryID: yup.string().required("Subcategory is required !"),
}).required();