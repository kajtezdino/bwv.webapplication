import * as yup from "yup";

export const ChangePasswordSchema = yup.object({
  oldPassword: yup.string().required('Current password is required'),
  password: yup.string().required('New password is required'),
  confirmPassword: yup
    .string()
    .required('Please confirm new password')
    .oneOf([yup.ref('password')], 'Password does not match with the new password'),
}).required();