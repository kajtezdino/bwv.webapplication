import { Children, cloneElement } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';

const NavLink: React.FC<any> = ({ children, activeClassName, ...props }) => {
  const { asPath } = useRouter();
  const child = Children.only(children);
  const childClassName = child.props.className || '';
  const isActive = asPath === props.href || asPath === props.as;
  const className = isActive
    ? `${childClassName} ${activeClassName}`.trim()
    : childClassName;

  return (
    <Link {...props}>
      {cloneElement(child, { 
        className: className || null
      })}
    </Link>
  );
}

export default NavLink