import Image from 'next/image';
import React, { SyntheticEvent, useState } from 'react';
import { UseFormStateReturn } from 'react-hook-form';

interface IPasswordInput {
  name: string
  placeholder: string
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void
  onBlur: () => void
  formState: UseFormStateReturn<any>
  classes?: string
}

const PasswordInput: React.FC<IPasswordInput> = ({
  name,
  placeholder,
  onChange,
  onBlur,
  formState,
  classes
}) => {
  const [isVisible, setIsVisible] = useState<boolean>(false);

  return (
    <>
      <input
        className={`${classes} input-password ${formState?.errors[name] ? "input-error" : ""}`}
        id={name}
        type={isVisible ? "input" : "password"}
        name={name}
        onChange={onChange}
        onBlur={onBlur}
        placeholder={placeholder}
      />
      <div
        className="absolute right-6 top-0 h-full pl-[25px] flex justify-center cursor-pointer opacity-70 hover:opacity-100"
        onClick={() => setIsVisible(!isVisible)}
      >
        <Image
          src={isVisible? "/icons/bwv-hide-password-icon.svg" : "/icons/bwv-show-password-icon.svg"}
          width={20}
          height={15}
          alt="Hide password icon"
        />
      </div>
    </>
  )
};

export default PasswordInput;