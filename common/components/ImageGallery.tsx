import { useEffect, useState } from 'react';
import Gallery from 'react-image-gallery';
import Image from "next/image";
import { NaturalImage } from '.';

const ImageGallery: React.FC<{ list: any, startIndex: number }> = ({ list, startIndex }) => {
  const [images, setImages] = useState<{original: string, thumbnail: string}[]>([])

  useEffect(() => {
    setImages(list.map((image: string) => {
      return {
        original: image,
        thumbnail: image,
        thumbnailClass: 'rounded-xl overflow-hidden',
        renderItem: () => (
          <NaturalImage
            className='max-h-full max-w-full rounded-3xl'
            src={image}
            blurDataURL={image}
            alt=""
          />
        )
      }
    }))
  }, [list])

  return (
    <div className="w-full h-full">
      <Gallery
        additionalClass='max-h-full'
        items={images}
        lazyLoad={false}
        slideDuration={300}
        startIndex={startIndex}
        showFullscreenButton={false}
        showPlayButton={false}
        renderLeftNav={(onClick, disabled) => (
          <span
            className='absolute flex justify-center items-center z-20 rounded-full w-8 h-8 top-1/2 left-3 hover:left-2 transition-all p-2 bg-stone-900/60 cursor-pointer'
            onClick={onClick}
          >
            <Image
              src="/icons/bwv-arrow-white-icon.svg"
              width={8}
              height={18}
              alt="Arrow"
            />
          </span>
        )}
        renderRightNav={(onClick, disabled) => (
          <span
            className='absolute flex justify-center items-center z-20 rounded-full w-8 h-8 top-1/2 right-3 hover:right-2 transition-all p-2 bg-stone-900/60 cursor-pointer' 
            onClick={onClick}
          >
            <Image
              className="rotate-180"
              src="/icons/bwv-arrow-white-icon.svg"
              width={8}
              height={18}
              alt="Arrow"
            />
          </span>
        )}
      />
    </div>
  )
}

export default ImageGallery;