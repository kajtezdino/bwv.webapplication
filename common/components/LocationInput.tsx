import MapboxGeocoder from '@mapbox/mapbox-gl-geocoder';
import mapboxgl from 'mapbox-gl';
import { Fragment, useEffect, useState } from 'react';

import usePlacesAutocomplete, {
  getGeocode,
  getLatLng,
} from "use-places-autocomplete";
import { Combobox, Transition } from '@headlessui/react';
import Image from 'next/image';

interface IComboboxInput {
  placeholder: string
  onChange: (value: string | { location: string, latitude: number, longitude: number}) => void
  onBlur: (value: string) => void
  label?: string
  src?: string
  alt?: string
  width?: number
  height?: number
  required?: boolean
  defaultValue: any,
  withCoordinates?: boolean
}

const LocationInput: React.FC<IComboboxInput>= ({
  placeholder,
  onChange,
  onBlur,
  label,
  src,
  alt,
  width = 14,
  height = 20,
  required,
  defaultValue,
  withCoordinates
}) => {
  useEffect(() => {
    if (defaultValue) {
      onBlur(defaultValue);
      onChange(defaultValue);
    }
  }, [defaultValue, onChange, onBlur])

  const {
    value,
    suggestions: { status, data },
    setValue,
  } = usePlacesAutocomplete();

  useEffect(() => {
    if (defaultValue) {
      setValue(defaultValue);
    }
  }, [defaultValue])


  const handleInput = (e: any) => {
    setValue(e.target.value);
  };

  const handleSelect = (val: any) => {
    setValue(val?.description, false);

    if (!withCoordinates) {
      onChange(val?.description);
    }

    if (val && withCoordinates) {
      getGeocode({ placeId: val.place_id }).then((results) => {
        try {
          const { lat, lng } = getLatLng(results[0]);
          onChange({
            location: val?.description,
            latitude: lat,
            longitude: lng
          });
          return { lat, lng };
        } catch (error) {
          console.error("Error: ", error);
        }
      });
    }
  };

  return (
    <div>
      <div className="flex items-start">
        {
          label ? (
            <label className="uppercase mb-2 font-semibold text-sm text-bwv-label inline-block">{label}</label>
          ) : null
        }
        {
          required ? (
            <Image src="/icons/bwv-required-icon.svg" width={7} height={7} alt="required icon" />
          ) : null
        }
      </div>
      <Combobox value={value} onChange={handleSelect} nullable>
        <div className="relative">
          {src
            ? <div className="absolute h-full pl-[25px] flex justify-center z-50">
              <Image
                src={src}
                width={width}
                height={height}
                alt={alt}
              />
            </div>
            : null
          }
          <div>
            <Combobox.Input
              className={`input-small ${src ? '' : 'pl-4'} relative w-full text-left bg-white cursor-text focus:outline-none focus-visible:ring-2 focus-visible:ring-opacity-75 focus-visible:ring-white focus-visible:ring-offset-2 focus-visible:border-bwv-accent`}
              displayValue={() => value}
              onChange={handleInput}
              placeholder={placeholder}
            />
          </div>
          <Transition
            as={Fragment}
            leave="transition ease-in duration-100"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Combobox.Options className="absolute z-[9999] w-full py-1 mt-1 overflow-auto text-base bg-white rounded-md shadow-lg max-h-60 ring-1 ring-black ring-opacity-5 focus:outline-none">
              {
                status === 'OK' && data.map((option: any) => (
                  <Combobox.Option
                    key={option.place_id}
                    className={({ active }) =>
                      `
                        cursor-pointer select-none relative py-2 pl-4 pr-4
                        ${active ? 'text-bwv-accent-dark bg-bwv-accent-light' : 'text-gray-900'}
                      `
                    }
                    value={option}
                  >
                    {() => (
                      <span
                        className={`block truncate 'font-normal'`}
                      >
                        {option.description}
                      </span>
                    )}
                  </Combobox.Option>
                ))
              }
            </Combobox.Options>
          </Transition>
        </div>
      </Combobox>
    </div>
  )
};

export default LocationInput;