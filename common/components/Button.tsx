import { useEffect, useState } from "react"

interface IButtonProps {
  disabled?: boolean
  loading?: boolean
  type?: "button" | "submit" | "reset" | undefined
  text: string
  loadingText?: string
  onClick?: any
  size?: "xs" | "small" | "normal" | "medium"
  color?: "primary" | "secondary" | "error"
  style?: "solid" | "outline"
}

const Button: React.FC<IButtonProps> = ({
  disabled = false,
  loading,
  type,
  text,
  loadingText,
  onClick,
  size = 'normal',
  color = 'primary',
  style = 'solid'
}) => {
  const displayText = (): string => {
    if (loading) {
      return loadingText || text
    }
    return text
  }

  const buttonStyles = {
    primary: {
      solid: "text-white bg-bwv-accent hover:bg-bwv-accent-dark",
      outline: "border-bwv-accent hover:bg-bwv-accent text-bwv-accent hover:text-white"
    },
    secondary: {
      solid: "text-bwv-accent bg-bwv-accent-light hover:bg-bwv-accent hover:text-white",
      outline: "border-bwv-accent-light hover:bwv-accent-light text-bwv-accent-light hover:!text-white",
    },
    error: {
      solid: "text-white bg-bwv-error hover:bg-bwv-error-dark",
      outline: "border-bwv-error hover:bg-bwv-error text-bwv-error hover:text-white",
    }
  }

  const sizeToClasses = (): string => {
    if (size === 'xs') {
      return 'py-[8px] px-[10px] text-sm text-center leading-[18px] rounded-[10px] font-semibold'
    }

    if (size === 'small') {
      return 'py-[10px] px-[15px] text-center leading-[20px] rounded-[15px] font-semibold'
    }

    return 'w-full py-[20px] text-center leading-[20px] rounded-[15px] font-semibold'
  }

  const colorToClasses = (): string => {
    return buttonStyles[color][style]
  }

  const disabledToClasses = (): string => {
    if (style === 'outline') {
      return '!border-[#8E98A9] !text-[#8E98A9]'
    }

    return '!bg-[#8E98A9] !text-white'
  }

  const styleToClasses = (): string => {
    return style === 'outline' ? 'bg-opacity-0 border hover:bg-opacity-1' : ''
  }

  const applyDisableOrOther = (): string => {
    return disabled ? `${disabledToClasses()} cursor-not-allowed pointer-events-none` : colorToClasses()
  }

  return (
    <button
      className={`
        ${applyDisableOrOther()}
        ${sizeToClasses()}
        ${styleToClasses()}
        inline-flex items-center justify-center transition-all
      `}
      type={type}
      onClick={onClick}
    >
      {
        loading ? (
          <svg className="animate-spin -ml-1 mr-3 h-5 w-5 text-white" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
            <circle className="opacity-25" cx="12" cy="12" r="10" stroke={style === "outline" ? "#8E98A9" : "#FFF"} strokeWidth="4"></circle>
            <path className="opacity-75" fill={style === "outline" ? "#8E98A9" : "#FFF"} d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"></path>
          </svg>
        ) : null
      }
      {displayText()}
    </button>
  )
}

export default Button;