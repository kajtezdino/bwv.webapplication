import { NavLink } from ".";
import { ISubNavLink } from "../models";

const SubNavLink: React.FC<ISubNavLink> = ({ url, title }) => {
  return (
    <NavLink href={url} activeClassName="active-sub-nav-link">
      <a className="text-sm font-semibold text-black opacity-70 px-5 pb-4 hover:text-bwv-accent">
        {title}
      </a>
    </NavLink>
      
  )
}

export default SubNavLink;