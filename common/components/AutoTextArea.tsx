import React, {
  useState,
  useEffect,
  useRef
} from "react";
import Image from 'next/image'

interface IAuroTextArea {
  className: string
  value?: string
  onChange: (e: React.ChangeEvent<HTMLTextAreaElement>) => void
  onPaste?: (e: any) => void
  placeholder: string
  label?: string
  required?: boolean
  disabled?: boolean
}

const AutoTextArea: React.FC<IAuroTextArea> = ({
  className,
  value,
  onChange,
  onPaste,
  placeholder,
  label,
  required,
  disabled
}) => {
  const textAreaRef = useRef<HTMLTextAreaElement>(null);
  const [text, setText] = useState("");
  const [textAreaHeight, setTextAreaHeight] = useState("auto");

  useEffect(() => {
    setTextAreaHeight(!value?.length || !text.length ? 'auto' : `${textAreaRef.current!.scrollHeight}px`);
  }, [text, value]);

  const onChangeHandler = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    setTextAreaHeight("auto");
    setText(event.target.value);

    if (onChange) {
      onChange(event);
    }
  };

  return (
    <>
      <div className="flex items-start">
        {
          label ? (
            <label className="uppercase mb-2 font-semibold text-sm text-bwv-label inline-block">{label}</label>
          ) : null
        }
        {
          required ? (
            <Image src="/icons/bwv-required-icon.svg" width={7} height={7} alt="required icon" />
          ) : null
        }
      </div>
      <textarea
        className={className}
        placeholder={placeholder}
        ref={textAreaRef}
        value={value}
        rows={1}
        style={{
          height: textAreaHeight,
        }}
        onChange={onChangeHandler}
        onPaste={onPaste}
        readOnly={disabled}
      />
    </>
  );
};

export default AutoTextArea;