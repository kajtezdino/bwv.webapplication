import { useEffect, useRef, useState } from "react";
import Image from "next/image";
import toast from "react-hot-toast";
import { CustomToast } from ".";
import { ToastTypes } from "../enums";

const MAX_LENGTH = 5;

interface IImageBrowseProps {
  disabled: boolean,
  files: File[] | null,
  onChange: (files: File[]) => void,
  maxLength?: number,
  label?: string,
  classes?: string,
  width?: number,
  height?: number,
  icon: string,
}

const ImageBrowse: React.FC<IImageBrowseProps> = ({
  disabled,
  onChange,
  files,
  maxLength = MAX_LENGTH,
  label,
  classes,
  width,
  height,
  icon
}) => {
  const filePickerRef = useRef(null);
  const [selectedFiles, setSelectedFiles] = useState<File[] | null>(null);

  useEffect(() => {
    setSelectedFiles(files)
  }, [files])

  /**
   * Sets selectedFiles state on files input.
   * Maximum number of files is 5. It checks if there are already selected files,
   * sums it up with files input. If number is greater than 5, toast warning is shown and
   * method returns. If number is equal or less than 5, function proceeds to setting selectedFiles state.
   * 
   * @param {React.ChangeEvent<HTMLInputElement>} e files input event
  */
  const addImageToPost = (e: React.ChangeEvent<HTMLInputElement>): void => {
    // any is needed here because of TS error "Type 'FileList | null' is not an array type"
    let fileList = [...(e.target.files as any)]

    fileList.forEach(file => {
      // Get file size in MB
      const fileSize = file.size / 1024 / 1024
      // Check if file size is greater than 2MB
      if (fileSize > 2) {
        fileList = fileList.filter(f => f.name !== file.name)
        toast((t) => (
          <CustomToast
            type={ToastTypes.ERROR}
            title="File size over 2MB"
            message={`${file.name} is larger than 2MB`}
            t={t}
          />
        ))
      }
    })

    const totalNumberOfFiles = selectedFiles
      ? selectedFiles.length + fileList.length
      : fileList.length

    if (totalNumberOfFiles > maxLength) {
      e.preventDefault();
      toast((t) => (
        <CustomToast
          type={ToastTypes.WARNING}
          title="Maximum number of files limit"
          message={`Cannot add more than ${maxLength} files`}
          t={t}
        />
      ));

      return;
    }
    
    const selectedFilesList = selectedFiles ? [...selectedFiles] : []
    onChange([...selectedFilesList, ...fileList])
    e.target.files = null;
  };

  return (
    <div
      className={`flex gap-2 cursor-pointer items-center 
        ${disabled ? "opacity-50 cursor-default select-none pointer-events-none" : ""}
        ${classes}
      `}
      onClick={() => (filePickerRef.current as any).click()}
    >
      <div className="flex items-center">
        <Image
          src={icon}
          width={width}
          height={height}
          className="h-[22px] text-[#1d9bf0]"
          alt="Image upload icon"
        />
        <input
          type="file"
          hidden
          multiple
          accept="image/png, image/jpeg, image/webp, image/jpg"
          onChange={addImageToPost}
          ref={filePickerRef}
        />
      </div>
      <span className="text text-sm text-[#252525]">{label}</span>
    </div>
  )
}

export default ImageBrowse;