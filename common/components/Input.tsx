import Image from 'next/image';
import { useEffect } from 'react';
import { UseFormStateReturn } from 'react-hook-form';
import PasswordInput from './PasswordInput';

interface IInput {
  iconUrl?: string
  width?: number
  height?: number
  classes?: string
  alt?: string
  onChange: (value: string) => void
  onBlur: () => void
  formState: UseFormStateReturn<any>
  name: string
  placeholder: string
  type?: string
  size?: 'small' | 'normal'
  label?: string
  defaultValue?: any
  readOnly?: boolean
  maxLength?: number
  required?: boolean
  hideErrorEl?: boolean
}

const Input: React.FC<IInput> = ({
  iconUrl,
  width,
  height,
  classes,
  alt,
  name,
  onChange,
  onBlur,
  formState,
  placeholder,
  type = 'input',
  size = 'normal',
  label,
  defaultValue,
  readOnly,
  maxLength,
  required,
  hideErrorEl
}) => {
  useEffect(() => {
    if (defaultValue) {
      onBlur();
      onChange(defaultValue);
    }
  }, [defaultValue, onChange, onBlur])

  const handleOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    onChange(e.target.value);
  }

  const inputSizeClass = (): string => {
    switch (size) {
      case 'normal':
        return 'input'
      case 'small':
        return 'input-small'
      default:
        return 'input'
    }
  }

  return (
    <div>
      <div className="flex items-start">
        {
          label ? (
            <label className="uppercase mb-2 font-semibold text-sm text-bwv-label inline-block">{label}</label>
          ) : null
        }
        {
          required ? (
            <Image src="/icons/bwv-required-icon.svg" width={7} height={7} alt="required icon" />
          ) : null
        }
      </div>
      <div className={`relative ${classes} ${formState?.errors[name] ? "input-wrapper-error" : "mb-4"} transition-all`}>
        {
          iconUrl ?
            <div className="absolute h-full pl-[25px] flex justify-center">
              <Image
                src={iconUrl}
                alt={alt || ""}
                width={width || 18}
                height={height || 14}
              />
            </div>
            : null
        }

        {
          type === 'password' ? (
            <PasswordInput
              name={name}
              onChange={handleOnChange}
              onBlur={onBlur}
              placeholder={placeholder}
              formState={formState}
              classes={size}
            />
          ) : (
            <input
              id={name}
              name={name}
              onChange={handleOnChange}
              onBlur={onBlur}
              placeholder={placeholder}
              className={`
                ${!iconUrl ? "p-4" : ""} ${inputSizeClass()} 
                ${formState?.errors[name]?.message ? "input-error" : ""} 
                ${readOnly ? "cursor-default bg-gray-100 focus:border-bwv-light-gray" : ""}
                `}
              type={type}
              autoComplete="off"
              defaultValue={defaultValue || ""}
              readOnly={readOnly}
              maxLength={maxLength}
            />
          )
        }
        {
          !hideErrorEl ? (
            <span className="input-error-message">
              {formState?.errors[name]?.message}
            </span>
          ) : null
        }
      </div>
    </div>
  )
}

export default Input;