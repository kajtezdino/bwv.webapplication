import React from 'react';
import { UseFormStateReturn } from 'react-hook-form';

export interface IRadioButton {
  name: string
  id: string
  onChange: () => void
  onBlur?: () => void
  formState?: UseFormStateReturn<any>
  label: string
}

const RadioButton: React.FC<IRadioButton> = ({
  id,
  name,
  onChange,
  onBlur,
  label,
}) => {

  return (
    <div className="flex justify-start p-3">
      <div className="form-check">
        <input
          className="form-check-input appearance-none rounded-full h-4 w-4 border border-gray-300 bg-white checked:bg-bwv-accent checked:border-bwv-accent focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
          id={id}
          type="radio"
          name={name}
          onChange={onChange}
          onBlur={onBlur}
        />
        <label className="form-check-label inline-block text-gray-800" htmlFor={id}>
          {label}
        </label>
      </div>
    </div>
  )
}

export default RadioButton;