import { signOut, useSession } from "next-auth/react";
import { useRouter } from "next/router";
import { useRecoilValue } from "recoil";
import { userState } from "../../store/atoms/userAtom";
import Avatar from "./Avatar";
import DropdownMenu from "./DropdownMenu";

const UserMenu: React.FC = () => {
  const { data: session } = useSession();
  const userData: any = useRecoilValue(userState);
  const router = useRouter();

  const menuBtn = () => {
    return (
      <Avatar size={50} user={userData?.firstName ? userData : session?.user} />
    )
  }

  const menuItems = [
    {
      name: "Edit Profile",
      onClick: () => router.push("/user/edit-profile")
    },
    {
      name: "Sign out",
      onClick: () => { signOut({ redirect: false }); router.push("/auth/sign-in") }
    }
  ]

  return (
    <DropdownMenu menuBtn={menuBtn} menuItems={menuItems} />
  )
}

export default UserMenu;