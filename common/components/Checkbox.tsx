import React, { useState } from 'react';
import Image from 'next/image';

interface IChackBox {
  name: string
  placeholder: string
  defaultValue?: boolean
  onChange: (checked: boolean) => void
  onBlur?: () => void
  labelColor?: string
  disabled?: boolean
}

const Checkbox: React.FC<IChackBox> = ({
  name,
  placeholder,
  defaultValue,
  onChange,
  onBlur,
  labelColor,
  disabled
}) => {
  const [checked, setChecked] = useState<boolean>(defaultValue || false)
  const handleOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setChecked(e.target.checked);
    onChange(e.target.checked);
  }

  return (
    <div>
      <input
        className="hidden"
        id={name}
        type="checkbox"
        name={name}
        onChange={handleOnChange}
        onBlur={onBlur}
        checked={checked}
        disabled={disabled}
      />
      <label
        className="font-medium leading-[24px] text-[#C1C7D0] cursor-pointer flex justify-center gap-4"
        htmlFor={name}
      >
        {checked ? (
          <Image
            src="/icons/bwv-checkbox-checked-icon.svg"
            width={24}
            height={24}
            alt="Checkbox Icon"
          />
        ) : (
          <Image
            src="/icons/bwv-checkbox-icon.svg"
            width={24}
            height={24}
            alt="Checkbox Icon"
          />
        )}
        <span className={`select-none ${labelColor ? `text-${labelColor}` : "text-bwv-gray"} text-base`}>{placeholder}</span>
      </label>
    </div>
  )
}

export default Checkbox;