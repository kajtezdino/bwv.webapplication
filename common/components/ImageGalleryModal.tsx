import { Dialog, Transition } from "@headlessui/react";
import { Fragment, useEffect, useState } from "react";
import Image from "next/image";
import { ImageGallery } from '.'

const ImageGalleryModal: React.FC<{ isOpen: boolean, onClose: () => void, imageList: any, startIndex: number }> = ({
  isOpen,
  imageList,
  onClose,
  startIndex
}) => {
  const [openModal, setOpenModal] = useState(false);
  
  const closeModal = () => {
    onClose()
  }

  useEffect(() => {
    setOpenModal(isOpen)
  }, [isOpen])

  return (
    <Transition appear show={openModal} as={Fragment}>
      <Dialog as="div" className="relative z-10" onClose={closeModal}>
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-black bg-opacity-50" />
        </Transition.Child>

        <div className="fixed inset-0 overflow-y-auto">
          <div className="flex min-h-full items-center justify-center p-4 text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <Dialog.Panel className="w-[780px] max-w-full max-h-full rounded-3xl bg-white shadow-xl transition-all">
                <div className="p-6 pb-0 flex justify-end items-center">
                  <button
                    onClick={closeModal}
                  >
                    <Image src="/icons/bwv-gray-x-icon.svg" width={14} height={14} alt="Close modal icon" />
                  </button>
                </div>

                <Dialog.Description as="div" className="p-7 flex flex-col gap-7">
                  <ImageGallery list={imageList} startIndex={startIndex} />
                </Dialog.Description>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition>
  )
}

export default ImageGalleryModal;