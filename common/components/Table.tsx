import {
  TableInstance,
  usePagination,
  UsePaginationInstanceProps,
  UsePaginationState,
  useFilters,
  UseSortByInstanceProps,
  useTable,
  useRowSelect,
  UseRowSelectInstanceProps,
  UseFiltersOptions,
  UseRowSelectState,
  UseFiltersInstanceProps
} from 'react-table'
import Image from 'next/image'
import { forwardRef, useEffect, useRef, useState } from 'react';

export type TableInstanceWithHooks<T extends object> = TableInstance<T> &
  UsePaginationInstanceProps<T> &
  UseRowSelectInstanceProps<T> &
  UseFiltersOptions<T> &
  UseFiltersInstanceProps<T> &
  UseSortByInstanceProps<T> & {
    state: UsePaginationState<T> & UseRowSelectState<T>;
  };

// eslint-disable-next-line react/display-name
const IndeterminateCheckbox: React.FC<any> = forwardRef(
  ({ row, indeterminate, ...rest }, ref) => {
    const [checked, setChecked] = useState<boolean>(false);
    const defaultRef: any = useRef()
    const resolvedRef: any = ref || defaultRef

    useEffect(() => {
      resolvedRef.current.indeterminate = indeterminate
      setChecked(indeterminate)
    }, [resolvedRef, indeterminate])

    return (
      <div>
        <input className="hidden" name="check" id={row?.id || "indeterminate"} type="checkbox" ref={resolvedRef} {...rest} />
        <label
          className="font-medium leading-[24px] text-[#C1C7D0] cursor-pointer flex justify-center gap-4"
          htmlFor={row?.id || "indeterminate"}
        >
          {rest.checked ? (
            <Image
              src="/icons/bwv-checkbox-checked-icon.svg"
              width={24}
              height={24}
              alt="Checkbox Icon"
            />
          ) : (
            <Image
              src="/icons/bwv-checkbox-icon.svg"
              width={24}
              height={24}
              alt="Checkbox Icon"
            />
          )}
        </label>
      </div>
    )
  }
)

const Table: React.FC<{ columns: any, data: any, checkbox?: boolean }> = ({ columns, data, checkbox = true }) => {
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page, // Instead of using 'rows', we'll use page,
    // which has only the rows for the active page

    // The rest of these things are super handy, too ;)
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    selectedFlatRows,
    setFilter,
    state: { pageIndex, pageSize, selectedRowIds },
  } = useTable<any>({
    columns,
    data,
  },
  useFilters,
  usePagination,
  useRowSelect,
  hooks => {
    checkbox ?
      hooks.visibleColumns.push(columns => [
        // Let's make a column for selection
        {
          id: 'selection',
          // The header can use the table's getToggleAllRowsSelectedProps method
          // to render a checkbox
          Header: ({ getToggleAllPageRowsSelectedProps }: any) => (
            <div>
              <IndeterminateCheckbox {...getToggleAllPageRowsSelectedProps()} />
            </div>
          ),
          // The cell can use the individual row's getToggleRowSelectedProps method
          // to the render a checkbox
          Cell: ({ row }: any) => (
            <div className="w-[20px]">
              <IndeterminateCheckbox row={row} {...row.getToggleRowSelectedProps()} />
            </div>
          ),
          className: "!w-[0.0000000001%] pr-6"
        },
        ...columns,
      ])
      : null
  }
  ) as TableInstanceWithHooks<any>;

  return (
    <>
      <table className="block w-full relative" {...getTableProps()}>
        <thead>
          {headerGroups.map((headerGroup, index) => (
            <tr {...headerGroup.getHeaderGroupProps()} key={index}>
              {headerGroup.headers.map((column: any, index) => {
                return (
                  column.id === "selection_placeholder_0"
                    ? null
                    : column.originalId === "toolbar"
                      ? <th className='w-full' {...column.getHeaderProps()} colSpan={100} key={index}>{column.render('Header')}</th>
                      : <th className={`table-cell w-[1%] text-sm font-semibold uppercase ${column.className || ''} ${column.headerClass || ''} !text-[#D4D4D4]`} {...column.getHeaderProps()} key={index}>{column.render('Header')}</th>
                )
              })}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps}>
          {page.map((row: any, i: number) => {
            prepareRow(row)
            return (
              <tr {...row.getRowProps()} key={i}>
                {row.cells.map((cell: any, index: number) => {
                  return <td
                    {...cell.getCellProps()}
                    className={`table-cell  text-base text-[#B8B8B8] ${cell.column.className || ''} ${cell.column.cellClass || ''}`}
                    key={index}
                  >
                    {cell.render('Cell')}
                  </td>
                })}
              </tr>
            )
          })}
        </tbody>
      </table>
    </>
  )
}

export default Table;