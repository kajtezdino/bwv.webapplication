import { Toast, toast } from "react-hot-toast";
import Image from "next/image";

type ToatsType = 'success' | 'error' | 'warning' | 'info';

const ToastConfig = {
  success: {
    icon: "/icons/bwv-success-outline-icon.svg",
    color: "bg-[#e7f4da]"
  },
  error: {
    icon: "/icons/bwv-error-outline-icon.svg",
    color: "bg-[#ffded7]"
  },
  warning: {
    icon: "/icons/bwv-warning-outline-icon.svg",
    color: "bg-[#fff1cd]"

  },
  info: {
    icon: "/icons/bwv-info-outline-icon.svg",
    color: "bg-[#d4eef9]"
  }
}

const CustomToast: React.FC<{ t: Toast, type: ToatsType, message: string | (() => JSX.Element), title: string }> = ({
  t,
  type,
  message,
  title
}) => {

  const dismissToast = () => {
    toast.dismiss(t.id)
  }

  return (
    <div className={`${ToastConfig[type].color} relative w-[500px] flex justify-between items-center px-[20px] py-[10px] rounded-[15px]`}>
      <div className="flex justify-center items-center gap-3">
        <Image
          src={ToastConfig[type].icon}
          width={24}
          height={24}
          alt="success-outline-icon"
        />
        <div className="text-[#252525]">
          <h3 className="text-base font-semibold">{title}</h3>
          {
            typeof message === 'string' ? (
              <p className="text-base">{message}</p>
            ) : (
              message()
            )
          }
        </div>
      </div>

      <Image
        src="/icons/bwv-x-icon.svg"
        width={20}
        height={20}
        alt="success-outline-icon"
        className="cursor-pointer"
        onClick={dismissToast}
      />
    </div>
  )
}

export default CustomToast;