import { Menu, Transition } from "@headlessui/react";
import { Fragment } from "react";
import Image from "next/image";

interface IMenuItem {
  name: string
  onClick: () => void
  disabled?: boolean
  icon?: {
    src: string
    width: number
    height: number
    alt: string
  }
}

const DropdownMenu: React.FC<{ menuBtn: {}, menuItems: IMenuItem[], width?: string, position?: string }> = ({
  menuBtn,
  menuItems,
  width = "w-32",
  position = "right"
}) => {
  return (
    <div>
      <Menu as="div" className="relative inline-block text-left">
        <Menu.Button onClick={(e: any) => e.stopPropagation()}>
          {menuBtn}
        </Menu.Button>
        <Transition
          as={Fragment}
          enter="transition ease-out duration-100"
          enterFrom="transform opacity-0 scale-95"
          enterTo="transform opacity-100 scale-100"
          leave="transition ease-in duration-75"
          leaveFrom="transform opacity-100 scale-100"
          leaveTo="transform opacity-0 scale-95"
        >
          <Menu.Items className={`${width === 'auto' ? "" : width} absolute z-10 ${position === 'left' ? 'left-0' : 'right-0'}  mt-2 origin-top-right bg-white divide-y divide-gray-100 rounded-md shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none`}>

            {menuItems.map((menuItem: IMenuItem) => {
              return (
                <div className="px-1 py-1" key={menuItem.name}>
                  <Menu.Item>
                    <button
                      onClick={(e: any) => { e.stopPropagation(); menuItem.onClick() }}
                      className={`${menuItem.disabled ? "text-bwv-gray cursor-not-allowed pointer-events-none" : "hover:bg-bwv-accent hover:text-white"} 
                        group flex ${menuItem.icon ? "gap-2" : ""} rounded-md items-center w-full px-2 py-2 text-sm`}
                    >
                      {
                        menuItem.icon ? (
                          <div className="flex justify-center items-center w-[20px]">
                            <Image
                              src={menuItem.icon.src}
                              width={menuItem.icon.width}
                              height={menuItem.icon.height}
                              alt={menuItem.icon.alt}
                            />
                          </div>
                        ) : null
                      }
                      {menuItem.name}
                    </button>
                  </Menu.Item>
                </div>
              )
            })}
          </Menu.Items>
        </Transition>
      </Menu>
    </div>
  )
}

export default DropdownMenu;