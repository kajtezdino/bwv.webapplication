import DatePicker, { ReactDatePickerProps } from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

const Datepicker: React.FC<ReactDatePickerProps> = ({ onChange, selected }) => {
  return (
    <div className="w-[200px]">
      <DatePicker
        dateFormat="MM/dd/yyyy"
        selected={selected}
        startDate={selected}
        isClearable={false}
        className="border rounded-2xl flex flex-row input-small"
        onChange={onChange}
        placeholderText={'mm/dd/yyyy'}
      />
    </div>
  );
};

export default Datepicker;