import Image from 'next/image'
import Avvvatars from 'avvvatars-react'
import { IAvatar } from '../models'

const Avatar: React.FC<IAvatar> = ({ user, size }) => {
  return user && (user.image || user.imageUrl) ? (
    <div style={{ width: size, height: size }}>
      <Image
        className="rounded-full object-cover w-full h-full"
        src={user?.image || user?.imageUrl}
        alt={user.firstName ? user.firstName + " " + user.lastName : user.name || "User"}
        width={size}
        height={size}
      />
    </div>
  ) : (
    <Avvvatars value={user?.firstName ? (user?.firstName + " " + user?.lastName as string) : user?.name as string || user?.email as string} size={size} />
  )
}

export default Avatar
