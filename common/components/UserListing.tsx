import Image from "next/image";
interface IUserListing {
    url: string
    imgUrl: string
    title: string
}

const UserListing: React.FC<IUserListing> = ({ imgUrl, url, title }) => {
    return (
        <a href={url} className="p-[0px] flex items-center gap-[16px] font-medium text-[14px] mb-[16px]">
            <Image 
              src={imgUrl}
              alt={title}
              width={36}
              height={36}
              className="rounded-[6px]"
            />
              {title}
            </a>
    )
};

export default UserListing;