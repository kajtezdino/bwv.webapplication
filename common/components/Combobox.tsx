import { Fragment, useEffect, useState } from 'react';
import { UseFormStateReturn } from 'react-hook-form';
import { Combobox, Transition } from '@headlessui/react';
import { CheckIcon, SelectorIcon } from '@heroicons/react/solid';
import Image from 'next/image';

interface IComboboxInput {
  name: string
  placeholder: string
  options: any[]
  onChange: (value: string) => void
  onBlur: () => void
  formState: UseFormStateReturn<any>
  label?: string
  defaultValue: any
  valueKey?: string
  labelKey?: string
  src?: string
  alt?: string
  width?: number
  height?: number
  required?: boolean
}

const ComboboxInput: React.FC<IComboboxInput> = ({
  name,
  placeholder,
  options,
  onChange,
  onBlur,
  formState,
  label,
  defaultValue,
  valueKey,
  labelKey,
  src,
  alt,
  width = 14,
  height = 20,
  required
}) => {
  const [selected, setSelected] = useState(null)
  const [query, setQuery] = useState('')

  useEffect(() => {
    if (defaultValue) {
      onBlur();
      onChange(defaultValue);
    }
  }, [defaultValue, onChange, onBlur])

  useEffect(() => {
    if (options.length > 0 && defaultValue) {
      setSelected(defaultValue);
    }
  }, [defaultValue, options.length])

  const filteredOptions =
    query === ''
      ? options
      : options.filter((option) => {
        const value = labelKey ? (option as any)[labelKey] : option
        return value
          .toLowerCase()
          .replace(/\s+/g, '')
          .includes(query.toLowerCase().replace(/\s+/g, ''))
      })

  const handleOnChange = (value: any) => {
    setSelected(value);
    onChange(value);
  }

  const displaySelectedLabel = (): string => {
    const selectedOption: any = getSelectedFromOptions()

    if (selectedOption) {
      return labelKey ? selectedOption[labelKey] : selectedOption
    }

    return ''
  }

  const getSelectedFromOptions = (): any => {
    return options.filter((option: any) => {
      const value = valueKey ? option[valueKey] : option
      return value === selected
    })[0]
  }

  return (
    <div>
      <div className="flex items-start">
        {
          label ? (
            <label className="uppercase mb-2 font-semibold text-sm text-bwv-label inline-block">{label}</label>
          ) : null
        }
        {
          required ? (
            <Image src="/icons/bwv-required-icon.svg" width={7} height={7} alt="required icon" />
          ) : null
        }
      </div>
      <Combobox value={selected} onChange={handleOnChange} nullable disabled={!options.length}>
        <div className="relative">
          {src
            ? <div className="absolute h-full pl-[25px] flex justify-center z-50">
              <Image
                src={src}
                width={width}
                height={height}
                alt={alt}
              />
            </div>
            : null
          }
          <div>
            <Combobox.Input
              className={`input-small ${src ? '' : 'pl-4'} relative w-full text-left bg-white cursor-default focus:outline-none focus-visible:ring-2 focus-visible:ring-opacity-75 focus-visible:ring-white focus-visible:ring-offset-2 focus-visible:border-bwv-accent`}
              displayValue={() => displaySelectedLabel()}
              onChange={(event) => setQuery(event.target.value)}
              placeholder={placeholder}
            />
            <Combobox.Button className="absolute inset-y-0 right-0 flex items-center pr-2">
              <SelectorIcon
                className="w-5 h-5 text-gray-400"
                aria-hidden="true"
              />
            </Combobox.Button>
          </div>
          <Transition
            as={Fragment}
            leave="transition ease-in duration-100"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
            afterLeave={() => setQuery('')}
          >
            <Combobox.Options className="absolute z-[9999] w-full py-1 mt-1 overflow-auto text-base bg-white rounded-md shadow-lg max-h-60 ring-1 ring-black ring-opacity-5 focus:outline-none">
              {filteredOptions.length === 0 && query !== '' ? (
                <div className="cursor-default select-none relative py-2 px-4 text-gray-700">
                  Nothing found.
                </div>
              ) : (
                filteredOptions.map((option: any, index: number) => (
                  <Combobox.Option
                    key={index}
                    className={({ active }) =>
                      `cursor-default select-none relative py-2 pl-[60px] pr-4 ${active ? 'text-bwv-accent-dark bg-bwv-accent-light' : 'text-gray-900'
                      }`
                    }
                    value={valueKey ? (option as { [key: string]: any })[valueKey] : option}
                  >
                    {({ selected, active }) => (
                      <>
                        <span
                          className={`block truncate ${selected ? 'font-medium' : 'font-normal'}`}
                        >
                          {labelKey ? (option as any)[labelKey] : option}
                        </span>
                        {selected ? (
                          <span
                            className={`absolute inset-y-0 left-3 flex items-center pl-3 text-bwv-accent`}
                          >
                            <CheckIcon className="w-5 h-5" aria-hidden="true" />
                          </span>
                        ) : null}
                      </>
                    )}
                  </Combobox.Option>
                ))
              )}
            </Combobox.Options>
          </Transition>
        </div>
      </Combobox>
    </div>
  )
};

export default ComboboxInput;