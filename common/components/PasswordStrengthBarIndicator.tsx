import { useEffect, useState } from "react"
import zxcvbn from "zxcvbn"

interface IPasswordStrengthBarIndicator {
  passwordLength: string
  testResultScore:number
}

const PasswordStrengthBarIndicator: React.FC<IPasswordStrengthBarIndicator> = ({passwordLength, testResultScore})=>{
const [password, setPassword] = useState('')
const [testResult, setTestResult] = useState<any>(null)
const passwordStrengthLevels = [{color:'bg-bwv-red', score: 0}, {color:'bg-bwv-orange-dark', score: 1}, {color:'bg-bwv-orange', score: 2}, {color:'bg-bwv-yellow', score: 3}, {color:'bg-bwv-accent', score: 4}]

useEffect(() => {
  setTestResult(zxcvbn(password))
}, [password])

return (
    <div className="w-full flex gap-4 p-6 mb-5">
      { passwordStrengthLevels.map((item) => (
        <span
          className={`
            ${
              passwordLength && testResultScore >= item.score
                ? `${item.color}`
                : 'bg-bwv-light-gray'
            }
            inline-block h-1 w-1/5 rounded-[6px] transition ease-in-out relative
          `}
          key={item.score}
        >
          { item.score === 0 ? (
            <span className="absolute pt-4 w-full text-center text-xs font-semibold capitalize text-slate-400">weak</span>
          ) : null}
          { item.score === 4 ? (
            <span className="absolute pt-4 w-full text-center text-xs font-semibold capitalize text-slate-400">secure</span>
          ) : null}
      </span>
      ))}
    </div>
  )
}

export default PasswordStrengthBarIndicator