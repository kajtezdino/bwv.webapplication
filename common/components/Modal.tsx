import { Dialog, Transition } from '@headlessui/react';
import { useState, useEffect, Fragment } from 'react';
import Image from 'next/image';
import Button from './Button';

interface IModal2 {
  title: string
  isOpen: boolean
  content: any
  customFooter?: any
  onClose: () => void
  cancelBtn?: {
    name: string
    onClick?: Function
    disabled?: boolean
  },
  proceedBtn?: {
    name: string
    onClick: Function
    disabled?: boolean
  }
  proceedBtnDisabled?: boolean
  proceedBtnLoading?: boolean
  proceedBtnLoadingText?: string
}

const Modal: React.FC<IModal2> = ({
  title,
  isOpen,
  content,
  customFooter,
  onClose,
  cancelBtn,
  proceedBtn,
  proceedBtnDisabled,
  proceedBtnLoading,
  proceedBtnLoadingText
}) => {
  const [openModal, setOpenModal] = useState(false);

  const closeModal = () => {
    onClose()
  }

  useEffect(() => {
    setOpenModal(isOpen)
  }, [isOpen])

  return (
    <Transition appear show={openModal} as={Fragment}>
      <Dialog as="div" className="relative z-10" onClose={closeModal}>
        <Transition.Child
          as={Fragment}
          enter="ease-out duration-300"
          enterFrom="opacity-0"
          enterTo="opacity-100"
          leave="ease-in duration-200"
          leaveFrom="opacity-100"
          leaveTo="opacity-0"
        >
          <div className="fixed inset-0 bg-black bg-opacity-25" />
        </Transition.Child>

        <div className="fixed inset-0 overflow-y-auto">
          <div className="flex min-h-full items-center justify-center p-4 text-center">
            <Transition.Child
              as={Fragment}
              enter="ease-out duration-300"
              enterFrom="opacity-0 scale-95"
              enterTo="opacity-100 scale-100"
              leave="ease-in duration-200"
              leaveFrom="opacity-100 scale-100"
              leaveTo="opacity-0 scale-95"
            >
              <Dialog.Panel className="w-full max-w-[680px] transform rounded-3xl bg-white text-left align-middle shadow-xl transition-all">
                <div className="p-6 flex justify-between items-center border-b border-bwv-light-gray">
                  <Dialog.Title
                    as="h3"
                    className="text-xl font-semibold leading-6 text-bwv-dark"
                  >
                    {title}
                  </Dialog.Title>
                  <button
                    onClick={closeModal}
                  >
                    <Image src="/icons/bwv-gray-x-icon.svg" width={14} height={14} alt="Close modal icon" />
                  </button>
                </div>

                <Dialog.Description as="div" className="p-7 flex flex-col gap-6">
                  {content}
                </Dialog.Description>

                <div className="flex justify-center px-7">
                  {
                    customFooter 
                      ?
                      (customFooter) :
                      <div className="pb-7 flex gap-4">
                        <Button
                          text={cancelBtn?.name as string}
                          onClick={cancelBtn?.onClick || closeModal}
                          disabled={cancelBtn?.disabled}
                          style="outline"
                          size="small"
                          color="error"
                        />
                        <Button
                          text={proceedBtn?.name as string}
                          onClick={proceedBtn?.onClick}
                          disabled={proceedBtnDisabled}
                          loading={proceedBtnLoading}
                          loadingText={proceedBtnLoadingText}
                          style="outline"
                          size="small"
                        />
                      </div>
                  } 
                </div>
              </Dialog.Panel>
            </Transition.Child>
          </div>
        </div>
      </Dialog>
    </Transition>
  )
}

export default Modal;