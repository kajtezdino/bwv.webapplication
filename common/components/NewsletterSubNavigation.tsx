import { SubNavLink } from "."

const NewsletterSubNavigation: React.FC = () => {
  return (
    <div className="flex border-1 border-b border-b-bwv-light-gray mx-6 mb-4 px-3">
      <SubNavLink url="/admin/newsletter" title="Newsletter" />
      <SubNavLink url="/admin/newsletter/activity-feed" title="Activity Feed" />
      <SubNavLink url="/admin/newsletter/email-templates" title="Email Templates" />
    </div>
  )
}

export default NewsletterSubNavigation;