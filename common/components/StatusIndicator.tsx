const StatusIndicator: React.FC<{ label: string, color: string, classes?: string }> = ({ label, color, classes }) => {

  return (
    <div className={`status-indicator ${classes}`}>
      <span className={`inline-block w-[7px] h-[7px] rounded-full ${color}`} />
      <span className="inline-block text-base leading-5 font-normal">{label}</span>
    </div>
  )
}

export default StatusIndicator;