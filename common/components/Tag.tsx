import Image from "next/image";
import { TagType } from "../models";

// const mapTags: Record<TagType, ITag> = {
//   event: {
//     type: 'event',
//     title: 'Event',
//     icon: "/icons/bwv-event-green-icon.svg",
//     style: 'text-bwv-accent',
//     background: 'bg-bwv-accent-medium',
//     iconHeight: 11
//   },
//   marketplace: {
//     type: 'marketplace',
//     title: 'Marketplace item',
//     icon: "/icons/bwv-marketplace-orange-icon.svg",
//     style: 'text-bwv-orange',
//     background: 'bg-bwv-orange-light'
//   }
// }
interface ITag {
  icon?: string
  label: string
  type?: 'outline' | 'solid'
  color?: string
  textColor?: string
  fontSize?: number 
  iconWidth?:number
  iconHeight?:number
}

const Tag: React.FC<ITag> = ({
  icon,
  label,
  type = "outline",
  color = "bwv-dark",
  fontSize,
  iconWidth,
  iconHeight,
  textColor
}) =>{

  const tagStyle = (): string => {
    if (type === 'solid') {
      return `bg-${color} text-${textColor || 'white'}`
    } else {
      return `border border-${color} text-${textColor || color}`
    }
  }

  return(
    <div className={`
      ${tagStyle()}
      rounded-[20px] py-[3px] px-[10px] inline-flex items-center gap-[5px]
    `}>
      {
        icon ? (
          <Image
            src={icon}
            width={iconWidth || 10}
            height={iconHeight || 10}
            alt={label}
          />
        ) : null
      }

      <p className={`text-[${fontSize || 10}px] font-medium`}>
        {label}
      </p>
  </div>
  )
}

export default Tag

