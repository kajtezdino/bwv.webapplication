import { Fragment, useEffect, useState } from 'react';
import { UseFormStateReturn } from 'react-hook-form';
import { Listbox, Transition } from '@headlessui/react'
import Image from 'next/image';
import { CheckIcon, SelectorIcon } from '@heroicons/react/solid'

interface ISelectInput {
  name: string
  placeholder: string
  options: any[]
  onChange: (value: string) => void
  onBlur: () => void
  formState?: UseFormStateReturn<any>
  label?: string
  defaultValue: any
  valueKey?: string
  labelKey?: string
  required?: boolean
}

const SelectInput: React.FC<ISelectInput> = ({
  name,
  placeholder,
  options,
  onChange,
  onBlur,
  formState,
  label,
  defaultValue,
  valueKey,
  labelKey,
  required
}) => {
  const [selected, setSelected] = useState(defaultValue)

  useEffect(() => {
    if (defaultValue) {
      onBlur();
      onChange(defaultValue);
    }
  }, [defaultValue, onChange, onBlur])

  const handleOnChange = (value: string) => {
    setSelected(value);
    onChange(value);
  }

  const displaySelectedLabel = (): string => {
    const selectedOption: any = getSelectedFromOptions()

    if (selectedOption) {
      return labelKey ? selectedOption[labelKey] : selectedOption
    }

    return ''
  }

  const getSelectedFromOptions = (): any => {
    return options.filter((opt: any) => {
      const value = valueKey ? opt[valueKey] : opt
      return value === selected
    })[0]
  }

  return (
    <div>
      <div className="flex items-start">
        {
          label ? (
            <label className="uppercase mb-2 font-semibold text-sm text-bwv-label inline-block">{label}</label>
          ) : null
        }
        {
          required ? (
            <Image src="/icons/bwv-required-icon.svg" width={7} height={7} alt="required icon" />
          ) : null
        }
      </div>
      <Listbox value={selected} onChange={handleOnChange}>
        <div className="relative">
          <div className="absolute h-full pl-[25px] flex justify-center z-50">
            <Image
              src="/icons/bwv-gender-black-icon.svg"
              width={18}
              height={18}
              alt="Gender Icon"
            />
          </div>
          <Listbox.Button className="input-small relative w-full text-left bg-white cursor-pointer focus:outline-none focus-visible:ring-2 focus-visible:ring-opacity-75 focus-visible:ring-white focus-visible:ring-offset-orange-300 focus-visible:ring-offset-2 focus-visible:border-indigo-500">
            <span className="block truncate">{displaySelectedLabel()}</span>
            <span className="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
              <SelectorIcon
                className="w-5 h-5 text-gray-400"
                aria-hidden="true"
              />
            </span>
          </Listbox.Button>

          <Transition
            as={Fragment}
            leave="transition ease-in duration-100"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Listbox.Options className="absolute z-[9999] w-full py-1 mt-1 overflow-auto text-base bg-white rounded-md shadow-lg max-h-60 ring-1 ring-black ring-opacity-5 focus:outline-none">
              {options.map((option, optionIdx) => (
                <Listbox.Option
                  key={optionIdx}
                  className={({ active }) =>
                    `cursor-pointer select-none relative py-2 pl-[60px] pr-4 ${
                      active ? 'text-bwv-accent-dark bg-bwv-accent-light' : 'text-gray-900'
                    }`
                  }
                  value={valueKey ? (option as { [key: string]: any })[valueKey] : option}
                >
                  {({ selected }) => (
                    <>
                      <span
                        className={`block truncate ${selected ? 'font-medium' : 'font-normal'}`}
                      >
                        {labelKey ? (option as { [key: string]: any })[labelKey] : option}
                      </span>
                      {selected ? (
                        <span className="absolute inset-y-0 left-3 flex items-center pl-3 text-bwv-accent">
                          <CheckIcon className="w-5 h-5" aria-hidden="true" />
                        </span>
                      ) : null}
                    </>
                  )}
                </Listbox.Option>
              ))}
            </Listbox.Options>
          </Transition>
        </div>
      </Listbox>
    </div>
  )
};

export default SelectInput;