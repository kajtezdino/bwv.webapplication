import { useEffect, useState } from "react";
import { XIcon } from "@heroicons/react/solid";

const ImagePreview: React.FC<{file: File, onImageRemove: (file: File) => void, previewOnly: boolean}> = ({
  file,
  onImageRemove,
  previewOnly
}) => {
  const [image, setImage] = useState<any>(previewOnly ? file : null);

  const handleImageRemove = () => {
    setImage(null)
    onImageRemove(file)
  }

  useEffect(() => {
    if (file && !previewOnly) {
      const reader = new FileReader();
      reader.readAsDataURL(file);
  
      reader.onload = (readerEvent) => {
        setImage(readerEvent?.target?.result);
      };
    }
  }, [file, previewOnly])

  if (!image) return (<></>)
  
  return (
    <div className="relative overflow-hidden rounded-2xl w-full h-full">
      {
        !previewOnly && (
          <div
            className="absolute z-10 w-6 h-6 bg-[#15181c] hover:bg-[#272c26] bg-opaciti-75 rounded-full flex items-center justify-center top-1 right-1 cursor-pointer"
            onClick={handleImageRemove}
          >
            <XIcon className="text-white h-5" />
          </div>
        )
      }
      <img 
        src={image}
        alt=""
        className="w-full h-full object-cover"
      />
    </div>
  )
}

export default ImagePreview;