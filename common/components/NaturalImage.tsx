import Image, { ImageProps } from "next/image";
import { useState } from "react";

const NaturalImage: React.FC<ImageProps> = (props: ImageProps) => {
  const [ratio, setRatio] = useState(16/9)
  const [vertical, setVertical] = useState(false)

  return (
    <Image
      {...props}
      alt=""
      loading="eager"
      placeholder="blur"
      width={vertical ? (720 / ratio) : 720}
      height={vertical ? 720 : (720 / ratio)}
      onLoadingComplete={({ naturalWidth, naturalHeight }) => {
        let ratio = naturalWidth / naturalHeight

        if (naturalWidth < naturalHeight) {
          ratio = naturalHeight / naturalWidth
        }

        setVertical(naturalWidth < naturalHeight)
        setRatio(ratio)
      }}
    />
  )
}

export default NaturalImage;