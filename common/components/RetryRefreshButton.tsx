import Image from "next/image";

interface IRetryRefreshButton {
  onClick: () => void
  type: 'retry' | 'refresh'
}

interface IRetryRefreshDataMap {
  label: string
  src: string
}

const RetryRefreshButton: React.FC<IRetryRefreshButton> = ({ onClick, type }) => {

  const buttonType: { [key: string]: IRetryRefreshDataMap } = {
    "retry": {
      label: "Retry",
      src: "/icons/bwv-retry-icon.svg"
    },
    "refresh": {
      label: "Refresh list",
      src: "/icons/bwv-refresh-icon.svg"
    }
  }
  return (
    <div className="flex gap-2 cursor-pointer" onClick={onClick}>
      <Image src={buttonType[type].src} width={22} height={22} alt={buttonType[type].label} />
      <label className="text-bwv-accent text-[14px] font-semibold">{buttonType[type].label}</label>
    </div>
  )
}

export default RetryRefreshButton;