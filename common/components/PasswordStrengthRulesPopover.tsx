import PopoverBlock from "./Popover"

const PasswordStrengthRulesPopover: React.FC = () => {
  return (
    <div className="flex">
      <p className="mr-1 text-base font-medium">Your password is weak.</p>
      <PopoverBlock triggerLabel={'Check password requirements!'} >
        <div className="p-6 bg-bwv-white-dirty rounded-[15px]">
          <p className="text-base leading-5 font-semibold mb-[10px]">Password Strenght Rules</p>
          <p className="font-medium text-base leading-5">
            1. At laest 12 characters (required for your Muhlenberg password) - the
            more characters, the better.<br/>
            2. A mixture of both uppercase and lowercase letters.<br/>
            3. A mixture of letters and numbers.<br/>
            4. Inclusion of at least one special character, e.g., ! @ # ?
          </p>
        </div>
      </PopoverBlock>
    </div>
  )
}

export default PasswordStrengthRulesPopover