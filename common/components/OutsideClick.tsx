import React, { useRef, useEffect, ReactNode } from "react";

export interface IOutsideWrapperProps {
  onClick: any,
  children: ReactNode
}
export const OutsideWrapper: React.FC<IOutsideWrapperProps> = ({
  onClick,
  children
}) => {

  const useOutsideAlerter = (ref: any) => {
    useEffect(() => {
      const handleClickOutside = (event: any) => {
        if (ref.current && !ref.current.contains(event.target)) {
          onClick()
        }
      }
      document.addEventListener("mousedown", handleClickOutside);
      return () => {
        document.removeEventListener("mousedown", handleClickOutside);
      };
    }, [ref]);
  }

  const wrapperRef = useRef(null);
  useOutsideAlerter(wrapperRef);

  return <div ref={wrapperRef}>{children}</div>;
}