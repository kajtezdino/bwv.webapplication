import { useState } from "react";
import Image from 'next/image';
import Link from 'next/link';
import { NavLink } from '../components'

interface IMainNavLink {
    url: string
    src: string
    hoverSrc: string
    alt: string
    title: string
}

const MainNavLink: React.FC<IMainNavLink> = ({ url, src, hoverSrc, alt, title }) =>
  (
    <NavLink href={url} activeClassName="active-nav-link">
      <a className="nav-link font-semibold flex gap-[16px] w-full p-4 rounded-2xl items-center hover:bg-bwv-accent hover:text-white capitalize">
        <div className="nav-link-icon w-[20px] h-[20px]">
          <Image
            src={src}
            alt={alt}
            width={20}
            height={20}
            className="fill-bwv-dark"
          />
        </div>
        <div className="nav-link-hover-icon w-[20px] h-[20px]">
          <Image
            src={hoverSrc}
            alt={alt}
            width={20}
            height={20}
            className="fill-bwv-dark"
          />
        </div>
        {title}
      </a>
    </NavLink>
  )

export default MainNavLink;