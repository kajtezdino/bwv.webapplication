import { IStatus } from "../models";

export   const statusToDataMap: { [key: string]: IStatus } = {
  "Pending": {
    label: "Pending",
    color: "bg-bwv-orange"
  },
  "Active": {
    label: "Active",
    color: "bg-bwv-accent"
  },
  "Rejected": {
    label: "Rejected",
    color: "bg-bwv-error"
  },
  "Closed": {
    label: "Closed",
    color: "bg-bwv-error"
  },
  null: {
    label: "",
    color: ""
  },
  0: {
    label: "",
    color: ""
  },
}