export const removeColumn = (columns: any, columnAccesor: string) => {
  for( let i = 0; i < columns.length; i++){
    if ( columns[i].accessor === columnAccesor) {
      columns.splice(i, 1);
    }
  }
}