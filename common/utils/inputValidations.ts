export const onlySpaces = (str: string) => {
  return str.trim().length === 0;
}