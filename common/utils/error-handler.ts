import axios, { AxiosError } from 'axios'
import { ExtendedRequestOptions } from '../models/request-options'

export const errorHandler = (reject: Function, options: ExtendedRequestOptions<any>) =>
  (error: AxiosError) => {
    if (axios.isCancel(error)) {
      return
    }
    
    return reject(error)
  }

const isError = (err: AxiosError, status: number) =>
  err.response && err.response.status === status

export const handleResponseError = (error: AxiosError) => {
  if (isError(error, 403)) {
    return handle403Error(error)
  }

  return Promise.reject(error)
}

const handle403Error = (error: AxiosError) => {
  console.error('Handle 403 status error!!!')
}