import axios from 'axios';
import { ExtendedRequestOptions } from '../models/request-options';
import {
  cancelRequestIfNecessary,
  cancelToken,
  removeCanceler
} from './cancel-request';
import { errorHandler, handleResponseError } from './error-handler';
import { getSession } from 'next-auth/react'

const successHandler = (res: any) => {
  return res.data;
}

const requestHandler = async(request: any) => {
  request.headers['Content-Type'] = 'application/json'
  // const session: any = await getSession();
  // console.log('SESSION', session);
  // if (session) {
  //   if (!request.headers['Authorization'] && session.jwt) {
  //     request.headers['Authorization'] = `Bearer ${session.jwt}`
  //   }
  //   if (!request.headers['Content-Type'] && session.jwt) {
  //     request.headers['Content-Type'] = 'application/json'
  //   }
  // }
  
  return request
}

axios.interceptors.request.use(requestHandler)
axios.interceptors.response.use((response) => response, handleResponseError)

export const http = <T>(options: ExtendedRequestOptions<any>, responseMapper: Function = successHandler): Promise<T> => {
  return new Promise((resolve, reject) => {
    const { url, method, body, headers, queryParams, responseType, timeout, paramsSerializer } = options
    cancelRequestIfNecessary(url)
    axios.request<T>({
      url,
      method,
      data: body,
      headers,
      params: queryParams,
      paramsSerializer: paramsSerializer,
      responseType,
      cancelToken: cancelToken(url),
      timeout: timeout || 60000
    }).then(res => resolve(responseMapper(res)))
      .catch(errorHandler(reject, options))
      .finally(() => removeCanceler(url))
  })
}