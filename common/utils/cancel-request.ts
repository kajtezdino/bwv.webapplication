import axios, { Canceler } from 'axios'

export const cancelers: Map<string, Canceler> = new Map<string, Canceler>()

export const cancelToken = (url: string) => new axios.CancelToken(c => cancelers.set(url, c))

// If there is a pending and a new request which both have the same url, this function cancels the previous one
export const cancelRequestIfNecessary = (url: string) => {
  let cancel = cancelers.get(url)

  if (cancel) {
    cancel()
  }
}

export const removeCanceler = (url: string) => {
  cancelers.delete(url)
}
