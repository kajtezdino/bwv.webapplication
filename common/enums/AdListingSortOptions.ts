export enum AdListingsSortOptions {
  NEWEST_TO_OLDEST = 1,
  OLDEST_TO_NEWEST = 2,
  PRICE_LOW_TO_HIGH = 3,
  PRICE_HIGH_TO_LOW = 4
}