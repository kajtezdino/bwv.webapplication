export enum UserRoles {
  ADMIN = "Administrator",
  SUBSCRIBER = "SubscribedUser"
}