export enum AdStatus {
    REJECTED = 3,
    APPROVED = 2,
    CLOSED = 4,
    PENDING = 1
}