export enum UserAccountStatus {
  ACTIVE = 1,
  ADS_APPROVAL_PENDING = 2,
  SUSPENDED = 3
}