import Image from 'next/image';
import { Input, UserMenu } from '../../components';
import { useRouter } from 'next/router';

interface ITopHeader {
  title?: string
  compact?: boolean
}

const TopHeader: React.FC<ITopHeader> = ({ title, compact = true }) => {
  const router = useRouter();

  return (
    <header className="flex h-[110px] w-full py-8 px-4 justify-center sticky top-0 self-start z-10 bg-white">
      <div className="flex w-[290px]">
        <Image
          src="/icons/logo.svg"
          alt="logo"
          width={240}
          height={46}
        />
      </div>

      <div className={`flex ${compact ? "w-[740px]" : "w-full flex-1" } justify-between align-middle`}>
        <h1 className="text-4xl font-semibold">{title}</h1>
        <Input
          iconUrl="/icons/bwv-search-icon.svg"
          width={20}
          height={20}
          name="search"
          type="text"
          placeholder="Search"
          size="small"
          classes="content-center"
          onBlur={() => null}
          onChange={() => null}
          formState={{ errors: { 'search-box': '' } } as any}
        />
      </div>

      <div className="flex justify-end w-[290px] gap-[45px]">
        <Image
          src="/icons/bwv-bell-icon.svg"
          alt="bell-icon"
          width={24}
          height={24}
        />

        <Image
          src="/icons/bwv-comment-icon.svg"
          alt="comment-icon"
          width={24}
          height={24}
        />

        <div className="flex items-center align-middle w-[50px] h-[50px] bg-slate-100 rounded-full">
          <UserMenu />
        </div>
      </div>
    </header>
  )
}

export default TopHeader
