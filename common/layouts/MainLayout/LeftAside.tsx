import MainNavLink from "../../components/MainNavLink";
import UserListing from "../../components/UserListing";
import Avatar from "../../components/Avatar";
import { getSession, useSession } from "next-auth/react";
import { useRecoilValue, useSetRecoilState } from "recoil";
import { userState } from "../../../store/atoms/userAtom";
import { useEffect } from "react";
import UserProfileApiService from '../../../modules/user/api.service';
import { IUser } from "../../models";
import { HasRole } from "../../hoc";
import { UserRoles } from "../../enums";
import { useRouter } from "next/router";
import { MenuItem } from "../../models";


const LeftAside: React.FC = () => {
  const router = useRouter()
  const { data: session } = useSession();
  const user: any = useRecoilValue(userState);
  const setUser = useSetRecoilState(userState);

  const getUser = async() => {
    const session: any = await getSession();
    return await UserProfileApiService.getUserProfile((session?.user as any).userId)
  }
  const linkArray: MenuItem[] = [
    {
      url: "/admin/users",
      src: "/icons/bwv-users-icon.svg",
      hoverSrc: "/icons/bwv-users-white-icon.svg",
      alt: "Users Icon",
      title: "users",
      roles: [UserRoles.ADMIN]
    },
    {
      url: "/",
      src: "/icons/bwv-feed-icon.svg",
      hoverSrc: "/icons/bwv-hover-feed-icon.svg",
      alt: "bwv feed icon",
      title: "Feed",
      roles: [UserRoles.SUBSCRIBER]
    },
    {
      url: "",
      src: "/icons/bwv-event-icon.svg",
      hoverSrc: "/icons/bwv-hover-event-icon.svg",
      alt: "bwv events icon",
      title: "Events"
    },
    {
      url: "",
      src: "/icons/bwv-news-icon.svg",
      hoverSrc: "/icons/bwv-hover-news-icon.svg",
      alt: "bwv news icon",
      title: "News"
    },
    {
      url: "/admin/newsletter",
      src: "/icons/bwv-newsletter-icon.svg",
      hoverSrc: "/icons/bwv-hover-newsletter-icon.svg",
      alt: "Newsletter Icon",
      title: "Newsletter",
      roles: [UserRoles.ADMIN]
    },
    {
      url: "/buy-and-sell",
      src: "/icons/bwv-marketplace-icon.svg",
      hoverSrc: "/icons/bwv-hover-marketplace-icon.svg",
      alt: "bwv buy & sell icon",
      title: "Buy & Sell",
      roles: [UserRoles.SUBSCRIBER]
    },
    {
      url: "/buy-and-sell/ads-management",
      src: "/icons/bwv-marketplace-icon.svg",
      hoverSrc: "/icons/bwv-hover-marketplace-icon.svg",
      alt: "bwv buy & sell icon",
      title: "Ads Management",
      roles: [UserRoles.ADMIN]
    },
    {
      url: "/markets",
      src: "/icons/bwv-markets-icon.svg",
      hoverSrc: "/icons/bwv-hover-markets-icon.svg",
      alt: "bwv markets icon",
      title: "Markets"
    }
  ];

  useEffect(() => {
    getUser().then((res: IUser) => setUser(res))
  }, [setUser])

  return (
    <aside className="w-[300px] pl-4 pr-12 sticky top-[110px] self-start">
      <div className="h-full w-full">
        <div className="w-full h-[72px] bg-[#F8F8F8] p-[16px] rounded-2xl flex items-center mb-[26px]" >
          <div className="w-[40px] h-[40px] mr-[16px] relative">
            {<Avatar size={40} user={user?.firstName ? user : session?.user} />}
            <span className="w-[10px] h-[10px] bg-bwv-accent rounded-full absolute bottom-0 right-0"></span>
          </div>
          <p className="text-lg font-semibold text-bwv-dark">
            {user?.firstName}
          </p>
        </div>

        {linkArray.map((value, index) =>
          <HasRole roles={value.roles} key={index}>
            <MainNavLink key={index} url={value.url} src={value.src} hoverSrc={value.hoverSrc} alt={value.alt} title={value.title} />
          </HasRole>
        )}

        {/* { TODO: this is only for demo purpose } */}
        <div className="flex flex-wrap px-[16px]">
          <p className="text-[#A0A0A0] uppercase text-xs my-[16px] font-semibold">Your listings</p>
          <UserListing url="#0" title="Used Tractor for Sale" imgUrl="/images/userListing-placeholder.jpg" />
          <UserListing url="#0" title="Harvester Klaas 2120" imgUrl="/images/userListing1-placeholder.jpg" />
        </div>     

        {/* { TODO: this is only for demo purpose } */}
        <div className="flex flex-wrap px-[16px] gap-[8px]">
          <p className="text-[#A0A0A0] uppercase text-xs mb-[8px] pt-[8px] font-semibold">Quicklinks</p>
          <a href="#0" className="font-medium text-[14px] border rounded-xl py-[5px] px-[10px]">Sprayers & Applicators</a>
          <a href="#0" className="font-medium text-[14px] border rounded-xl py-[5px] px-[10px]">Tractors</a>
          <a href="#0" className="font-medium text-[14px] border rounded-xl py-[5px] px-[10px]">Harvesting</a>
        </div>
      </div>
    </aside>
  )
};

export default LeftAside;