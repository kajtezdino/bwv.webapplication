import { useSession } from 'next-auth/react'
import { UserRoles } from '../../enums'
import LeftAside from './LeftAside'
import RightAside from './RightAside'
import TopBanner from './TopBanner'
import TopHeader from './TopHeader'
import { HasRole } from '../../hoc'

type MainLayoutProps = {
  children: React.ReactNode
  title: string
}

const MainAdminLayout: React.FC<MainLayoutProps> = ({ children, title }) => {
  return (
    <div className="h-full w-full overflow-hidden pb-10">
      <HasRole roles={[UserRoles.SUBSCRIBER]}>
        <TopBanner />
      </HasRole>

      <TopHeader title={title} compact={false} />

      <div className="flex w-full flex-wrap">
        <LeftAside />

        <main className="flex w-full flex-1 justify-center rounded-3xl bg-[#F8F8F8]">
          {children}
        </main>

        <RightAside />
      </div>
    </div>
  )
}

export default MainAdminLayout
