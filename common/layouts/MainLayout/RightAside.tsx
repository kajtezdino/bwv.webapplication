import Image from 'next/image'
import { WeatherWidget } from '../../components'
import MarketWidget from '../../../modules/markets/widgets/markets'

const RightAside: React.FC = () => {
  return (
    <aside className="w-[300px] pl-12 pr-4 sticky top-[110px] self-start">
      <div className="h-full w-full">
        <div className="flex flex-col">
          <p className="mb-4 text-xs font-semibold uppercase text-[#A0A0A0]">
            Latest news
          </p>
          <div className="flex items-center justify-center gap-[12px]">
            <div className="min-h-[84px] min-w-[84px]">
              <Image
                src="/images/news-placeholder.png"
                width={84}
                height={84}
                alt="news placeholder"
              />
            </div>
            <p className="text-[12px]">
              Wheat prices are skyrocketing! Expert explains why.
            </p>
          </div>
          <div className="flex items-center justify-center gap-[12px]">
            <div className="min-h-[84px] min-w-[84px]">
              <Image
                src="/images/news2-placeholder.png"
                width={84}
                height={84}
                alt="news placeholder"
              />
            </div>
            <p className="text-[12px]">
              Growing berries this season? Read 9 tips that will double your
              harvest!
            </p>
          </div>
        </div>

        <div className="mb-[24px] flex flex-col">
          <p className="mt-[16px] text-xs font-semibold uppercase text-[#A0A0A0]">
            Newsletter
          </p>
          <div className="flex flex-col">
            <p className="my-[8px] text-[12px] text-[#252525]">
              Get news and updates on your email!
            </p>
            <input
              type="email"
              name="email"
              placeholder="Insert your email address..."
              className="color-[#b8b8b8] min-h-[40px] rounded-[15px] bg-[#f8f8f8] px-[16px] text-[14px] font-medium focus:border focus:border-bwv-accent focus:outline-0"
            />
            <button className="mt-[8px] w-fit self-end rounded-[12px] bg-bwv-accent py-[6px] px-[10px] text-[12px] font-semibold text-[#fff]">
              Submit
            </button>
          </div>
        </div>

        <div className="mb-[24px] flex flex-col">
          <WeatherWidget />
        </div>

        <div className="flex flex-col">
          <p className="text-[#A0A0A0] uppercase text-xs mb-[8px] font-semibold">Markets</p>
          <MarketWidget />
        </div>

      </div>
    </aside>
  )
}

export default RightAside
