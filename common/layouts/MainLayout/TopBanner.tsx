import Image from 'next/image';

const TopBanner: React.FC = () => {
  return (
    <section className='h-[150px] w-full flex justify-center'>
      <Image
        src='/images/top-banner-placeholder.png'
        alt='top banner'
        width={3000}
        height={150}
        className='object-cover'
      />
    </section>
  )
};

export default TopBanner;