import { useSession } from 'next-auth/react'
import { UserRoles } from '../../enums'
import LeftAside from './LeftAside'
import RightAside from './RightAside'
import TopBanner from './TopBanner'
import TopHeader from './TopHeader'
import { HasRole } from '../../hoc'

type MainLayoutProps = {
  children: React.ReactNode
  title: string
}

const MainLayout: React.FC<MainLayoutProps> = ({ children, title }) => {
  const { data: session } = useSession();
  
  return (
    <div className="h-full w-full pb-10">
      <HasRole roles={[UserRoles.SUBSCRIBER]}>
        <TopBanner />
      </HasRole>

      <TopHeader title={title} compact={false} />

      <div className="relative flex w-full min-h-screen flex-wrap justify-center">
        <LeftAside />

        <main className="flex w-full flex-1 justify-center rounded-3xl bg-[#F8F8F8] py-5">
          {children}
        </main>

        <RightAside />
      </div>
    </div>
  )
}

export default MainLayout
