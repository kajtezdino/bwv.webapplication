import Image from 'next/image';

type AuthLayoutProps = {
  children: React.ReactNode,
};

const AuthLayout: React.FC<AuthLayoutProps> = ({ children }) => {
  return (
    <main className="flex min-h-screen">
      <section className="w-1/2 px-4 max-w-full bg-slate-400 bg-[url('/images/login-background.jpeg')] bg-cover bg-no-repeat flex justify-center items-center">
        <div className="p-8 bg-white/80 rounded-[25px]">
          <Image
            src="/icons/logo.svg"
            width={482}
            height={84}
            alt="BWV Logo"
            className="max-w-full"
          />
          <h3 className="text-3xl font-semibold text-bwv-accent">Connecting Farmers and Ranchers</h3>
        </div>
      </section>
      <section className="relative w-1/2 flex flex-col justify-center items-center">
        {children}
      </section>
    </main>
  )
}

export default AuthLayout;