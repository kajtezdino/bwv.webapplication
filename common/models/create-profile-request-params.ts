import { IAddNewUser } from "./add-new-user";

export interface CreateProfileRequestParams extends IAddNewUser {}