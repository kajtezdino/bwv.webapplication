export interface IBuyAndSellListingsFilters {
  buy?: boolean
  sell?: boolean
  categoryId?: string
  subcategories?: string[]
  searchString?: string
  sortOrder?: number
}