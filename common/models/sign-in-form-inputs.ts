export interface ISignInFormInput {
  email: string;
  password: string;
  rememberMe: boolean;
}