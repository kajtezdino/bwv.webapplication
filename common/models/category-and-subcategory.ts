export interface ICategoryAndSubcategory {
  categoryID: string | null,
  name: string,
  adType?: string,
  subcategories?: ISubcategory[]
}

export interface ISubcategory {
  subcategoryID: string,
  name: string,
  categoryID: string
}