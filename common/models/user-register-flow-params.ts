export interface IUserRegisterRequestParams {
  userName: string
  email: string
  password: string
  acceptTermsAndConditions: boolean
  subscribeOnNewsletter?: boolean
  role: string
}

export interface IUserRegisterResponseParams {
  userId: string
}