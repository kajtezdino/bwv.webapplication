export interface IStatus {
  label: string,
  color: string
}