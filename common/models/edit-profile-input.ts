export interface IEditProfileInput {
  firstName: string
  lastName: string
  email: string
  location: string
  gender: number
  phoneNumber: string
  imageUrl: File
}