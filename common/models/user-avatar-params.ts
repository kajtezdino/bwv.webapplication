import { IUser } from "./user"

export interface IAvatar {
  user: IUser,
  size: number
}