export interface IUpdateProfileResponse {
  id: string
  firstName: string
  lastName: string
  imageUrl: string
  email: string
}