export interface IUserProfile {
  id:	string
  email	:string
  firstName:	string
  lastName:	string
  location:	string
  gender:	number
  phoneNumber?:	string
  imageUrl?:	string
}