export interface ISuspendActivateUser {
  id: string | string[] | undefined
  accountStatus: number
  suspensionReason: string
  suspendedBy: string
}