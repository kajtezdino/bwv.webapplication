export interface IActivityFeedResponse {
  activityFeeds: Array<IActivityFeedTableParams>;
  totalNumberOfRecipients: number;
  totalNumberOfSubscribedUsers: number;
}

export interface IActivityFeedTableParams {
  newsletterNotificationID: string,
  recipient: string,
  notificationDate: string,
  newsletterName: string,
  mailStatus: string,
  subject: string
}