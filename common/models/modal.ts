export interface IModal {
  headerTitle: string
  isOpen: boolean
  onClose: () => void
  content: {}
  onClick: () => void
  footer: {}
}