import { NextPage } from "next";
import { JSXElementConstructor, ReactElement } from "react";

export type NextPageWithLayout<T> = NextPage<T> & {
  getLayout?: (page: ReactElement<any, string | JSXElementConstructor<any>>) => JSX.Element
} & { auth?: any }