export interface INewsletter {
  name: string
  creationDate: string
  emailTemplateID: string
  subject: string
  status: string
  newsletterID: string
}