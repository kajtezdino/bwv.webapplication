export interface MenuItem {
  url: string,
  src: string,
  hoverSrc: string,
  alt: string,
  title: string,
  roles?: string[],
  children?: SubMenuItem[]
}

export interface SubMenuItem {
      title: string,
      onClick: any
}