export interface IAddNewListing {
  title: string,
  location: string,
  adConditionID: string,
  price: number,
  coverImageUrl: File | null,
  adImages: File[] | null
  description: string,
  adTypeID: string,
  adGroupID: string,
  categoryID: string,
  subcategoryID: string,
  createdBy: string,
  longitude: number,
  latitude: number,
}

export interface IAddNewListingForm {
  title: string,
  location: {
    location: string,
    longitude: number,
    latitude: number,
  },
  adConditionID: string,
  price: number,
  coverImageUrl: File | null,
  adImages: File[] | null
  description: string,
  adTypeID: string,
  adGroupID: string,
  categoryID: string,
  subcategoryID: string,
  createdBy: string,
}