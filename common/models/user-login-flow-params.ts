export interface IUserLoginRequestParams {
  email: string
  password: string
  rememberMe: boolean
  url?: string
}

export interface IUserLoginResponseParams {
  userId: number
  userName: string
  access_token: string
}