export interface IUnsubscribeReason {
  id: string,
  name: string
}