export interface IEmailTemplate {
  thumbnailUrl: string
  name: string
  status: string
  creationDate: string
  templateId: string
}