type HttpMethod = 'get' | 'post' | 'put' | 'delete' | 'patch'

export type QueryParams = {
  [key: string]: string | number | boolean | undefined | null
}

export interface RequestOptions {
  headers?: {
    [key: string]: string
  }
  queryParams?: QueryParams
  paramsSerializer?: any
  responseType?: 'arraybuffer' | 'blob' | 'document' | 'json' | 'text' | 'stream' | undefined
  disableErrorMsg?: boolean
  timeout?: number
  disableErrorForStatuses?: Array<{ status: number, callback?: Function }>
}

export interface ExtendedRequestOptions<T> extends RequestOptions {
  method: HttpMethod
  url: string
  body?: T
}
