export interface ITypesGroupsAndConditions {
  adConditions: IBasicObjectStructure[],
  adGroups: IBasicObjectStructure[],
  adTypes: IBasicObjectStructure[]
}

export interface IBasicObjectStructure {
  id: string,
  name: string
}