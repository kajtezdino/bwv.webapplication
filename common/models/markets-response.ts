export interface IMarketsResponse {
  quoteResponse: {
    result: Array<{
      shortName: string
      regularMarketPrice: number
      regularMarketChange: number
      regularMarketChangePercent: number
      regularMarketTime: number
      symbol: string
      regularMarketDayHigh: number
      regularMarketDayLow: number
      regularMarketOpen: number
    }>
  }
}