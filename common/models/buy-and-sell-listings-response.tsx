import { IBuyAndSellListing } from "./buy-and-sell-listing";

export interface IBuyAndSellListingsResponse {
  ads: IBuyAndSellListing[],
  totalNumber: number
}