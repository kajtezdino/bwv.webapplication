export interface ICoverSelection {
  coverFile: File | null,
  onChange: (file: File | null) => void;
}