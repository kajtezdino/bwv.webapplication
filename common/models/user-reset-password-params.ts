export interface IUserResetPasswordParams {
  password: string
  confirmPassword: string
  email: string
  code: string
}