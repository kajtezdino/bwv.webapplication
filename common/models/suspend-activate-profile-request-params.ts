import { ISuspendActivateUser } from "./suspend-activate-user";

export interface SuspendActivateProfileRequestParams extends ISuspendActivateUser { }