export interface ISubNavLink {
  url: string
  title: string
}