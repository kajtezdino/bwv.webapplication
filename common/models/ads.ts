import { Pagination } from "./index";

export interface AdsResponse {
    ads: IAd[],
    paging: Pagination
}

export interface IAd {
    adID: string
    coverImageUrl: string
    title: string
    location?: string,
    price: number,
    description: string,
    adType: string,
    adGroup: string,
    creationDate: string,
    adStatus: string,
    category: string,
    subcategory: string
}

export interface AdRequestParams {
    userId?: string | null,
    pageNumber: number,
    pageSize: number,
    adStatus?: number
}

export interface UpdateAdRequestParams {
    adID: string,
    adStatus: number,
    reason: string
}