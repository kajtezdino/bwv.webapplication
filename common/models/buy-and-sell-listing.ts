export interface IBuyAndSellListing {
  adID: string
  title: string
  location: string
  price: number
  coverImageUrl: string
  description: string
  adGroup: string
  category: string
  subcategory: string
}