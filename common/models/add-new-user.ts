export interface IAddNewUser {
  firstName: string
  lastName: string
  email: string
  location: string
  phoneNumber: string
  imageUrl: File
  password: string
  confirmPassword: string
  gender: number
  role: 'Administrator' | 'SubscribedUser'
}