import { IMarket } from "./market"

export interface IFuture extends IMarket {
  category: string
  regularMarketOpen: number
  regularMarketDayLow: number
  regularMarketDayHigh: number
  time: number
}