export interface IActivityFeed {
  newsletterId: string | null,
  from: string | null,
  to: string | null
}