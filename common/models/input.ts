export interface IInput {
  placeholder: string
  onChange?: (e: any) => void
}