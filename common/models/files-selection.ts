export interface IFilesSelection {
  files: File[] | null,
  onChange: (file: File[] | null) => void;
}