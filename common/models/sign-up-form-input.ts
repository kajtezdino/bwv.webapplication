export interface ISignUpFormInput {
  email: string;
  firstName: string;
  lastName: string;
  password: string;
  confirmPassword: string;
  acceptTermsAndConditions: boolean;
  subscribeOnNewsletter: boolean;
}