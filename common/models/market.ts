export interface IMarket {
  regularMarketPrice: number
  regularMarketChange: number
  regularMarketChangePercent: number
  regularMarketTime: number
  regularMarketOpen: number
  regularMarketDayHigh: number
  regularMarketDayLow: number
  shortName: string
  category?: string
  symbol: string
}

export interface IMarketItem {
  config: {
    shortName: string
    regularPriceDecimals: number
    changeDecimals: number
    changePercentDecimals: number
    source: string
    currency: string
  }
  values: IMarket
}