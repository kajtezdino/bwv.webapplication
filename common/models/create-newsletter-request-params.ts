import { ICreateNewsletter } from "./create-newsletter";

export interface CreateNewsletterRequestParams extends ICreateNewsletter { }