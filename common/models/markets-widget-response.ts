export interface IMarketsWidget {
    quoteResponse: {
        result: Array<{
            shortName: string
            regularMarketPrice: number
            regularMarketChange: number
            regularMarketChangePercent: number
            regularMarketTime: number
            symbol: string
        }>
    }
}