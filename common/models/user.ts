export interface IUser {
  id:	string
  email	:string
  firstName:	string
  lastName:	string
  name: string,
  location:	string
  gender:	number
  phoneNumber:	string
  imageUrl:	string
  accountStatus: 1 | 2 | 3
  creationDate: string
  image: string
}