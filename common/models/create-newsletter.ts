export interface ICreateNewsletter {
  name: string 
  emailTemplateID: string 
  subject: string 
}