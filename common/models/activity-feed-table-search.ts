export interface ITableSearch {
  searchQuery: string,
  dateFrom: Date,
  dateTo: Date
}