export interface IUnsubscribeForm {
  emailAddress: string
  predefinedReasons: string[]
  unsubscribeReason?: string
}