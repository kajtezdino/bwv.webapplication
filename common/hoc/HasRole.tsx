import { useSession } from "next-auth/react";

const HasRole: React.FC<{roles?: string[]}> = ({ roles,  children }) => {
  const {data: session} = useSession()
  const userSession: any = session;

  if (roles && !roles.includes(userSession?.user?.roles)) return <></>

  return (
    <>
      {children}
    </>
  )
}

export default HasRole;