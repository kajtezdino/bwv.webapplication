import { doc, setDoc } from "firebase/firestore";
import { IUser, IUpdateProfileResponse } from "../models";
import { db } from "../../firebase";

class FirestoreUserService {
  setUser = (user: IUser | IUpdateProfileResponse): Promise<any> =>
    setDoc(doc(db, 'users', user.id), {
      id: user.id,
      firstName: user.firstName,
      lastName: user.lastName,
      imageUrl: user.imageUrl,
      email: user.email
    })
}

export default new FirestoreUserService();