import { AxiosResponse } from 'axios';
import { http } from '../../common/utils/http';

class UserApiService {
  getUserProfile = (userId: string, ...args: any): Promise<any> => 
    http<AxiosResponse>({
      url: args.length ? `${args[0]}/apis/Profile/GetProfile/${userId}` : '/apis/Profile/GetProfile/' + userId,
      method: 'get',
      timeout: 6000000
    })
}

export default new UserApiService();