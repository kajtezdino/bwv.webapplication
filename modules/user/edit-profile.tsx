import {
  EditProfileForm,
  EditProfileTabs,
  ProfileImage
} from "./components";

const UserEditProfileContent: React.FC = () => {
  return (
    <div className='min-h-screen w-[876px] max-w-full p-8 rounded-[20px] bg-white'>
      <h1 className="text-3xl font-semibold pb-6">Edit Profile</h1>

      <EditProfileForm />

      <div className="mt-[50px]">
        <EditProfileTabs />
      </div>
    </div>
  )
};

export default UserEditProfileContent;