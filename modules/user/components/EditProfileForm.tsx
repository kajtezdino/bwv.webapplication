import { yupResolver } from "@hookform/resolvers/yup";
import { useState } from "react";
import { useForm, Controller } from "react-hook-form";
import toast from "react-hot-toast";
import { useRecoilValue, useSetRecoilState } from "recoil";
import {
  Input,
  SelectInput,
  ComboboxInput,
  Button,
  CustomToast,
  LocationInput,
} from "../../../common/components";
import { ToastTypes } from "../../../common/enums";
import { IEditProfileInput } from "../../../common/models";
import { EditProfileSchema } from "../../../common/schemas";
import { userState } from "../../../store/atoms/userAtom";
import UserProfileApiService from '../api.service';
import ProfileImage from "./ProfileImage";
import { FirestoreUserService } from "../../../common/services";

const EditProfileForm: React.FC = () => {
  const [image, setImage] = useState<File | null>(null)
  const userData: any = useRecoilValue(userState);
  const setUserData = useSetRecoilState(userState);
  const { handleSubmit, control, formState: { errors, isValid } } = useForm<IEditProfileInput>({
    mode: 'onChange',
    reValidateMode: 'onChange',
    resolver: yupResolver(EditProfileSchema)
  });

  const onSubmit = async (data: IEditProfileInput) => {
    const params = { ...userData, ...data };
    Object.keys(params).forEach((key: string) => !(params as { [k: string]: any })[key] ? delete (params as { [k: string]: any })[key] : {});

    !image && delete params.imageUrl

    try {
      const response = await UserProfileApiService.updateUserProfile({
        ...params,
        ...(image && { ImageUrlFormFile: image })
      })

      // Update user in firebase
      await FirestoreUserService.setUser(response)

      setUserData(await UserProfileApiService.getUserProfile(userData.id))
      toast((t) => (
        <CustomToast
          type={ToastTypes.SUCCESS}
          title="Success"
          message="You have successfully edited user. Yay!"
          t={t}
        />
      ))
    } catch (e) {
      const error = String(e)
      const modifiedError = error.split(':')[1]

      toast((t) => (
        <CustomToast
          type={ToastTypes.ERROR}
          title="Error"
          message={`${modifiedError}`}
          t={t}
        />
      ))
    }
  }

  if (!userData) return <>Loading user data...</>

  return (
    <>
      <ProfileImage onChange={setImage} />
      <form
        className="pt-9"
        onSubmit={handleSubmit(onSubmit)}
      >
        <div className="grid grid-cols-2 gap-4">
          <Controller
            control={control}
            name="firstName"
            render={({ formState, field: { onBlur, onChange } }) => (
              <Input
                iconUrl="/icons/bwv-wheat-black-icon.svg"
                width={22}
                height={22}
                name="firstName"
                placeholder="User"
                onBlur={onBlur}
                onChange={onChange}
                maxLength={16}
                formState={formState}
                size="small"
                label="First name"
                defaultValue={userData.firstName}
              />
            )}
          />

          <Controller
            control={control}
            name="email"
            render={({ formState, field: { onBlur, onChange } }) => (
              <Input
                iconUrl="/icons/bwv-email-black-icon.svg"
                width={18}
                height={18}
                name="email"
                type="email"
                placeholder="Your Email"
                onBlur={onBlur}
                onChange={onChange}
                formState={formState}
                size="small"
                label="Email"
                defaultValue={userData.email}
                readOnly
              />
            )}
          />

          <Controller
            control={control}
            name="lastName"
            render={({ formState, field: { onBlur, onChange } }) => (
              <Input
                iconUrl="/icons/bwv-wheat-black-icon.svg"
                width={22}
                height={22}
                maxLength={16}
                name="lastName"
                placeholder="Last name"
                onBlur={onBlur}
                onChange={onChange}
                formState={formState}
                size="small"
                label="Last name"
                defaultValue={userData.lastName}
              />
            )}
          />

          <Controller
            control={control}
            name="location"
            render={({ field: { onBlur, onChange } }) => (
              <LocationInput
                src="/icons/bwv-location-black-icon.svg"
                onBlur={onBlur}
                onChange={onChange}
                placeholder="Select location"
                defaultValue={userData.location || ''}
                label="Location"
                alt="Location Icon"
              />
            )}
          />

          <Controller
            control={control}
            name="gender"
            render={({ formState, field: { onBlur, onChange } }) => (
              <SelectInput
                name="gender"
                placeholder="Gender"
                options={[
                  { id: 1, name: 'Male' },
                  { id: 2, name: 'Female' },
                  { id: 3, name: 'Other' },
                ]}
                valueKey="id"
                labelKey="name"
                onBlur={onBlur}
                onChange={onChange}
                formState={formState}
                label="Gender"
                defaultValue={userData.gender || 1}
              />
            )}
          />

          <Controller
            control={control}
            name="phoneNumber"
            render={({ formState, field: { onBlur, onChange } }) => (
              <Input
                iconUrl="/icons/bwv-phone-black-icon.svg"
                width={18}
                height={18}
                name="phoneNumber"
                placeholder="Contact phone"
                onBlur={onBlur}
                onChange={onChange}
                formState={formState}
                size="small"
                label="Contact phone"
              />
            )}
          />
        </div>

        <div className="w-full flex justify-end gap-4 pt-4">
          <Button
            text="Cancel"
            size="small"
            color="secondary"
            type='button'
          />
          <Button
            text="Save"
            type="submit"
            size="small"
          />
        </div>
      </form>
    </>
  )
};

export default EditProfileForm;