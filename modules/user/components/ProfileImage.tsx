import { useEffect, useRef, useState } from "react";
import Image from "next/image";
import Avvvatars from "avvvatars-react";
import { useRecoilValue } from "recoil";
import { userState } from '../../../store/atoms/userAtom';

const ProfileImage: React.FC<{ onChange: (image: File) => void }> = ({ onChange }) => {
  const userData: any = useRecoilValue(userState);
  const [selectedImage, setSelectedImage] = useState<any>("");
  const imagePickerRef = useRef(null);

  useEffect(() => {
    setSelectedImage(userData.imageUrl)
  }, [userData.imageUrl])

  const addProfileImage = (e: any) => {
    const reader = new FileReader();
    if (e.target.files[0]) {
      reader.readAsDataURL(e.target.files[0]);
    }

    reader.onload = (readerEvent) => {
      setSelectedImage(readerEvent?.target?.result);
      onChange(e.target.files[0]);
    };
  }

  return (
    <div>
      <div className="relative inline-block">
        { selectedImage ? (
          <Image
            src={selectedImage}
            width={140}
            height={140}
            alt=""
            className="bg-slate-200 rounded-full object-cover w-[140px] h-[140px]"
          />
        ) : (
          <div className="w-[140px] h-[140px] object-contain bg-slate-200 rounded-full overflow-hidden">
            <Avvvatars value={userData.firstName ? userData?.firstName + ' ' + userData?.lastName : userData.email} style="character" size={140} />
          </div>
        )}

        <div
          className="absolute bottom-0 right-0 cursor-pointer"
          onClick={() => (imagePickerRef.current as any).click()}
        >
          <Image
            src="/icons/bwv-edit-green-icon.svg"
            width={36}
            height={36}
            alt="Edit Icon"
          />
          <input
              type="file"
              hidden
              onChange={addProfileImage}
              ref={imagePickerRef}
              accept="jpg, webp, png, JPEG, JPG"
          />
        </div>
      </div>
      <h2 className="text-2xl pt-4">
        { userData.firstName + ' ' + userData.lastName }
      </h2>
    </div>
  )
}

export default ProfileImage;