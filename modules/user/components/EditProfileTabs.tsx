import { useState } from 'react';
import { Tab } from '@headlessui/react';
import { ChangePasswordTab } from './Tabs';
import NotificationsTab from './Tabs/Notifications';

function classNames(...classes: any) {
  return classes.filter(Boolean).join(' ')
}

const EditProfileTabs: React.FC = () => {
  const [selectedIndex, setSelectedIndex] = useState<number>(2) 
  const [tabs] = useState([
    {
      disabled: false,
      label: 'Notifications',
      content: () => {
        return <NotificationsTab />
      }
    },
    {
      disabled: true,
      label: 'Payment & Billing',
      content: () => {
        return (
          <></>
        );
      },
    },
    {
      disabled: false,
      label: 'Change password',
      content: () => {
        return <ChangePasswordTab />;
      },
    }
  ])

  return (
    <div className="w-full">
      <Tab.Group selectedIndex={selectedIndex} onChange={setSelectedIndex}>
        <Tab.List className="flex gap-4 border-b pb-4">
          {tabs.map((tab) => (
            <Tab
              key={tab.label}
              disabled={tab.disabled}
              className={({ selected }) =>
                classNames(
                  'py-[6px] px-[12px] text-base leading-5 font-medium rounded-[12px]',
                  'focus:outline-none transition-all',
                  selected
                    ? 'text-bwv-accent bg-bwv-accent-light'
                    : 'text-bwv-label bg-bwv-white-dirty hover:text-bwv-accent hover:bg-bwv-accent-light'
                )
              }
            >
              {tab.label}
            </Tab>
          ))}
        </Tab.List>
        <Tab.Panels className="mt-4">
          {tabs.map((tab, idx) => (
            <Tab.Panel
              key={idx}
              className={classNames(
                'bg-white rounded-xl p-3',
                'focus:outline-none'
              )}
            >
              {tab.content}
            </Tab.Panel>
          ))}
        </Tab.Panels>
      </Tab.Group>
    </div>
  )
}

export default EditProfileTabs;
