import router from "next/router";
import { useState } from "react";
import toast from "react-hot-toast";
import { useRecoilValue, useSetRecoilState } from "recoil";
import { Checkbox, CustomToast } from "../../../../common/components"
import { ToastTypes } from "../../../../common/enums";
import { userState } from "../../../../store/atoms/userAtom";
import UserProfileApiService from '../../api.service';

const NotificationsTab: React.FC = () => {
  const userData: any = useRecoilValue(userState);
  const setUserData = useSetRecoilState(userState);
  const [busy, setBusy] = useState<boolean>(false)

  const handleOnSubscribe = () => {
    if (userData.isSubscribed) {
      router.push('/newsletter-unsubscribe')
    } else {
      subscribeUserToNewsletter()
    }
  }

  const subscribeUserToNewsletter = async () => {
    setBusy(true)
    try {
      const response = await UserProfileApiService.updateUserProfile({ ...userData, isSubscribed: ['dsad', 'sdada'] })
      setUserData({ ...userData, isSubscribed: true });
      toast((t) => (
        <CustomToast
          type={ToastTypes.SUCCESS}
          title="Success"
          message="You have successfully subscribed to newsletter."
          t={t}
        />
      ))
    } catch (e: any) {
      toast((t) => (
        <CustomToast
          type={ToastTypes.ERROR}
          title="Error"
          message={`${e.response.data.title}`}
          t={t}
        />
      ))
    } finally {
      setBusy(false)
    }
  }

  return (
    <div className="flex flex-col">
      <p className="font-semibold text-sm text-bwv-label pb-4">NOTIFICATIONS</p>
      <div className="flex py-2">
        <Checkbox name={"Allow desktop notifications"} placeholder="Allow desktop notifications" onChange={() => { }} labelColor={"bwv-dark"} />
      </div>
      <div className="flex py-2">
        <Checkbox name={"Enable notifications"} placeholder="Enable notifications" onChange={() => { }} labelColor={"bwv-dark"} />
      </div>
      <div className="flex py-2">
        <Checkbox name={"Get notifications for my own activity"} placeholder="Get notifications for my own activity" onChange={() => { }} labelColor={"bwv-dark"} />
      </div>
      <div className="flex py-2">
        <Checkbox name={"Do not disturb"} placeholder="Do not disturb" onChange={() => { }} labelColor={"bwv-dark"} />
      </div>
      <div className="flex py-2">
        <Checkbox
          defaultValue={userData.isSubscribed}
          name={"Allow receiving Newsletters"}
          placeholder="Allow receiving Newsletters"
          onChange={handleOnSubscribe}
          labelColor={"bwv-dark"}
          disabled={busy}
        />
      </div>
    </div>
  )
}

export default NotificationsTab