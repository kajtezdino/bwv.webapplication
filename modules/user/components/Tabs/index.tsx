import ChangePasswordTab from './ChangePassword';
import NotificationsTab from './Notifications';

export {
  ChangePasswordTab,
  NotificationsTab
}