import { useEffect, useState } from "react"
import { Controller, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { ChangePasswordSchema } from "../../../../common/schemas";
import { 
  Input, 
  Button, 
  CustomToast, 
  PasswordStrengthBarIndicator, 
  PasswordStrengthRulesPopover 
} from "../../../../common/components";
import zxcvbn from 'zxcvbn';
import UserProfileApiService from "../../api.service";
import { useRecoilValue } from "recoil";
import { userState } from "../../../../store/atoms/userAtom";
import { ToastTypes } from "../../../../common/enums";
import toast from "react-hot-toast";

const ChangePasswordTab: React.FC = () => {
  const userData: any = useRecoilValue(userState);
  const [busy, setBusy] = useState<boolean>(false);
  const [password, setPassword] = useState('')
  const [testResult, setTestResult] = useState<any>(null)

  useEffect(() => {
    setTestResult(zxcvbn(password))
  }, [password])
  
  const { handleSubmit, control, formState: { errors, isValid } } = useForm<{
    oldPassword: string, password: string, confirmPassword: string
  }>({
    mode: 'onChange',
    reValidateMode: 'onChange',
    resolver: yupResolver(ChangePasswordSchema)
  });

  const onSubmit = async (data: any) => {
    setBusy(true)
    try {
      await UserProfileApiService.checkPassword({
        id: userData.id,
        password: data.oldPassword
      })

      await UserProfileApiService.updateUserProfile({
        ...userData,
        password: data.password,
        confirmPassword: data.confirmPassword
      })
      toast((t) => (
        <CustomToast
          type={ToastTypes.SUCCESS}
          title="SUCCESS"
          message="You have successfully changed your password. Yay!"
          t={t}
        />
      ))
    } catch (e) {
      toast((t) => (
        <CustomToast
          type={ToastTypes.ERROR}
          title="Error"
          message={`${e}`}
          t={t}
        />
      ))
    }
    setBusy(false)
  }

  return (
    <section className="w-full">
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className="w-[394px] max-w-full">
          <Controller
            control={control}
            name="oldPassword"
            render={({ formState, field: { onBlur, onChange } }) => (
              <Input
                iconUrl="/icons/bwv-lock-black-icon.svg"
                width={16}
                height={20}
                name="oldPassword"
                placeholder="Password"
                onBlur={onBlur}
                onChange={onChange}
                formState={formState}
                type="password"
                size="small"
                label="Old password"
              />
            )}
          />

          <Controller
            control={control}
            name="password"
            render={({ formState, field: { onBlur, onChange } }) => (
              <Input
                iconUrl="/icons/bwv-lock-black-icon.svg"
                width={16}
                height={20}
                name="password"
                placeholder="New Password"
                onBlur={()=>{
                  if (testResult?.score < 4){
                    toast((t) => (
                      <CustomToast
                        type={ToastTypes.WARNING}
                        title="Warning"
                        message={() => (
                          <PasswordStrengthRulesPopover />
                        )}
                        t={t}
                      />
                    ))
                  }
                }}
                onChange={(e: string) => { setPassword(e); onChange(e) }}
                formState={formState}
                type="password"
                size="small"
                label="New password"
              />
            )}
          />

          <Controller
            control={control}
            name="confirmPassword"
            render={({ formState, field: { onBlur, onChange } }) => (
              <Input
                iconUrl="/icons/bwv-lock-black-icon.svg"
                width={16}
                height={20}
                name="confirmPassword"
                placeholder="Confirm Password"
                onBlur={onBlur}
                onChange={onChange}
                formState={formState}
                type="password"
                size="small"
                label="Confirm password"
              />
            )}
          />

          <PasswordStrengthBarIndicator passwordLength={password} testResultScore={testResult?.score} />

        </div>

        <div className="flex justify-end">
          <Button
            text="Update password"
            size="small"
            type="submit"
            disabled={!isValid || busy || testResult?.score < 4}
            loading={busy}
          />
        </div>
      </form>
    </section>
  )
};

export default ChangePasswordTab;