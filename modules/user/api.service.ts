import { AxiosResponse } from 'axios';
import { http } from '../../common/utils/http';
import {
  ICheckCurrentPassword,
  IUserProfile,
  IUpdateProfileResponse
} from '../../common/models';

class UserProfileApiService {
  checkPassword = (params: ICheckCurrentPassword): Promise<AxiosResponse> =>
    http<AxiosResponse>({
      url: '/apis/Auth/Check-Password',
      method: 'post',
      body: { ...params }
    })

  getUserProfile = (userId: string, ...args: any): Promise<any> => 
    http<AxiosResponse>({
      url: args.length ? `${args[0]}/apis/Profile/GetProfile/${userId}` : '/apis/Profile/GetProfile/' + userId,
      method: 'get',
      timeout: 6000000
    })

  updateUserProfile = (params: IUserProfile): Promise<IUpdateProfileResponse> => {
    const formData = new FormData();

    for (const param in params) {
      formData.append(param , (params as {[key: string]: any})[param])
    }

    return http<IUpdateProfileResponse>({
      url: '/apis/Profile/UpdateProfile',
      method: 'put',
      headers: {
        'content-type': 'multipart/form-data'
      },
      body: formData
    })
  }
}

export default new UserProfileApiService();