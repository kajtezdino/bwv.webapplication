import { MarketsOverview, MarketsFutures } from "./components";

const MarketsContent: React.FC = () => {

  return (
    <div className="flex flex-col gap-[32px] w-[876px] mt-[84px]">
      <MarketsOverview />
      <MarketsFutures />
    </div>
  )
}

export default MarketsContent;