import Image from "next/image";
import { IMarketItem } from "../../../../../common/models";
import Moment from "react-moment";

const MarketItem: React.FC<IMarketItem> = ({ values, config }) => {
  const priceUpIcon = "/icons/markets/bwv-price-up-icon.svg";
  const priceDownIcon = "/icons/markets/bwv-price-down-icon.svg";

  return (
    <div className="bg-bwv-white-dirty rounded-[10px] flex flex-col mb-[8px] py-[6px] px-[12px]">
      <div className="flex w-[100%] justify-between">
        <div className="flex w-[48px] h-[48px] mr-[8px]">
          <Image src={config.source} width={48} height={48} alt={`market ${config.shortName} icon`} />
        </div>
        <div className="flex flex-col grow">
          <div className="flex justify-between items-center w-[100%]">
            <div className="flex flex-col">
              <p className="font-bold text-[10px] uppercase">{config.shortName}</p>
              <div className="flex flex-wrap text-[10px] text-semibold">
                <div className="mr-[8px]">
                  <p className={values.regularMarketChange < 0 ? "text-bwv-error" : "text-bwv-accent"}>
                    <Image src={values.regularMarketChange < 0 ? priceDownIcon : priceUpIcon} height={6} width={6} alt="price change icon" />
                    {values.regularMarketChange.toFixed(config.changeDecimals)}
                  </p>
                </div>
                <div className="mr-[8px]">
                  <p className={values.regularMarketChangePercent < 0 ? "text-bwv-error" : "text-bwv-accent"}>
                    {values.regularMarketChangePercent.toFixed(config.changePercentDecimals)}%
                  </p>
                </div>
              </div>
            </div>
            <div className="flex border-l pl-[4px] ml-[4px] border-[#fff]">
              <p className="text-[10px] font-semibold mr-[4px]">{config.currency}</p>
              <p className="text-[20px] font-bold leading-none">
                {values.regularMarketPrice.toFixed(config.regularPriceDecimals)}
              </p>
            </div>
          </div>
          <div className="flex">
            <p className="text-[10px] mt-[4px]">Updated: <Moment format="MM/DD/YY, h:mm:ss a">{values.regularMarketTime * 1000}</Moment></p>
          </div>

        </div>
      </div>
    </div>
  )
}

export default MarketItem;