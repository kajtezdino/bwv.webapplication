import { useEffect, useState } from "react";
import MarketsApiService from "../../api.service";
import MarketItem from "./components/MarketItem";
import marketsConfig from "../../config/markets-config.json";
import futuresConfig from '../../config/futures-config.json';
import { IMarket } from "../../../../common/models";
import { useRecoilState } from "recoil";
import { marketsState } from '../../../../store/atoms/marketsAtom'
import toast from "react-hot-toast";
import { CustomToast } from "../../../../common/components";
import { ToastTypes } from "../../../../common/enums";

const MarketWidget: React.FC = () => {

  const [markets, setMarkets] = useRecoilState(marketsState);

  const fetchMarketsData = async () => {
  try {
    const response = await MarketsApiService.getMarketsData();
    const data = response.quoteResponse.result;
    const marketArr: IMarket[] = data.map((market) => {
      return {
        symbol: market.symbol,
        shortName: (marketsConfig as { [key: string]: any })[market.symbol].shortName,
        regularMarketPrice: market.regularMarketPrice,
        regularMarketChange: market.regularMarketChange,
        regularMarketChangePercent: market.regularMarketChangePercent,
        regularMarketTime: market.regularMarketTime,
        regularMarketOpen: market.regularMarketOpen,
        regularMarketDayHigh: market.regularMarketDayHigh,
        regularMarketDayLow: market.regularMarketDayLow,
        category: (futuresConfig as { [key: string]: any })[market.symbol].category
      }
    })
      setMarkets(marketArr);
  } catch (error) {
      toast((t) => (
        <CustomToast
          type={ToastTypes.ERROR}
          title="Error"
          message="There is a problem with the Market Widget. Market is not updated!"
          t={t}
        />
      ))
    }
  }

  useEffect(() => {
    fetchMarketsData()
  }, []);

  return (
    <div>
      {markets.length > 0 &&
        <div>
          {markets.map((market) => {
            return <MarketItem
              key={market.symbol}
              config={(marketsConfig as { [key: string]: any })[market.symbol]}
              values={market}
            />
          })
          }
        </div>
      }
    </div>
  )
}

export default MarketWidget;