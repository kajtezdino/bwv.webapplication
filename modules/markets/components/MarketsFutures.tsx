import { Table } from "../../../common/components";
import { useMemo } from "react";
import Image from "next/image";
import MarketsFuturesTableToolbar from "./MarketsFuturesTableToolbar";
import { IFuture } from "../../../common/models";
import Moment from "react-moment";
import { useRecoilValue } from "recoil";
import { marketsState } from "../../../store/atoms/marketsAtom";

const MarketsFutures: React.FC = () => {

  const futures = useRecoilValue(marketsState);

  const columns = useMemo(
    () => [
      {
        id: "toolbar",
        className: "w-full",
        Header: (table: any) => {
          return (
            <MarketsFuturesTableToolbar table={table} />
          )
        },
        columns: [
          {
            Header: "Contract",
            accessor: "shortName",
            className: "min-w-[160px]",
            cellClass: "!text-bwv-accent font-semibold",
          },
          {
            Header: "Month",
            accessor: "regularMarketTime",
            cellClass: "font-medium",
            Cell: ({ row }: { row: { original: IFuture } }) => (
              <Moment format="MMM DD">{row.original.regularMarketTime * 1000}</Moment>
            )
          },
          {
            Header: "Open",
            accessor: "regularMarketOpen",
            cellClass: "font-medium"
          },
          {
            Header: "High",
            accessor: "regularMarketDayHigh",
            cellClass: "font-medium"
          },
          {
            Header: "Low",
            accessor: "regularMarketDayLow",
            cellClass: "font-medium"
          },
          {
            Header: "Last",
            accessor: "regularMarketPrice",
            cellClass: "font-medium"
          },
          {
            Header: "Change",
            accessor: "regularMarketChange",
            cellClass: "font-medium !text-bwv-error"
          },
          {
            Header: "Time",
            accessor: "time",
            cellClass: "!text-bwv-dark font-semibold",
            Cell: ({ row }: { row: { original: IFuture } }) => (
              <Moment format="HH:mm">{row.original.regularMarketTime * 1000}</Moment>
            )
          },
          {
            Header: () => null,
            id: "info",
            Cell: () => (
              <div className="w-[21px] h-[21px] cursor-pointer opacity-75 hover:opacity-100">
                <Image
                  src="/icons/bwv-info-icon.svg"
                  width={21}
                  height={21}
                  alt="Info icon"
                />
              </div>
            ),
            className: "!w-[0.0000000001%] pl-6"
          },
          {
            Header: "",
            accessor: "category",
            Cell: () => null
          }
        ]
      }
    ],
    []
  )

  return (
    <div className="flex flex-col">
      <p className="text-xl text-bwv-dark font-semibold mb-5">Futures</p>
      <div className="bg-white rounded-[20px] px-6 py-8">
        <Table columns={columns} data={futures} checkbox={false} />
      </div>
    </div>
  )
}

export default MarketsFutures;