import { IMarketItem } from "../../../common/models";
import { useRecoilValue } from "recoil";
import { marketsState } from "../../../store/atoms/marketsAtom";
import MarketsOverviewItem from "./MarketsOverviewItem";
import marketsConfig from '../config/markets-config.json'

const MarketsOverview: React.FC = () => {

  const markets = useRecoilValue(marketsState)

  return (
    <div className="flex flex-col">
      <p className="text-2xl pb-5 text-bwv-dark font-semibold">Overview</p>
      <div className="flex flex-wrap gap-8">
        {markets.slice(0, 4).map((market) => {
          return <MarketsOverviewItem
            key={market.symbol}
            config={(marketsConfig as { [key: string]: any })[market.symbol]}
            values={market}
          />
        })
        }
      </div>
    </div>
  )
}

export default MarketsOverview;