import { IMarketItem } from "../../../common/models"
import Image from "next/image"

const MarketsOverviewItem: React.FC<IMarketItem> = ({ config, values }) => {

  const priceUpIcon = "/icons/markets/bwv-price-up-icon.svg";
  const priceDownIcon = "/icons/markets/bwv-price-down-icon.svg";

  return (
    <div className="bg-white rounded-[20px] p-4 flex-grow">
      <p className="text-[18px] font-semibold leading-6 ">{config.shortName}</p>
      <p className="mt-6 font-semibold text-4xl leading-[44px]">{values.regularMarketPrice.toFixed(config.regularPriceDecimals)}</p>
      <div className="flex flex-row g-2">
        <p className={values.regularMarketChange < 0 ? "text-bwv-error" : "text-bwv-accent"}>
          <Image src={values.regularMarketChange < 0 ? priceDownIcon : priceUpIcon} height={6} width={6} alt="price change icon" />
          {values.regularMarketChange.toFixed(config.changeDecimals)}
        </p>
        <p className={values.regularMarketChangePercent < 0 ? "text-bwv-error ml-5" : "text-bwv-accent ml-5"}>
          {values.regularMarketChangePercent.toFixed(config.changePercentDecimals)}%
        </p>
      </div>
    </div>
  )
}

export default MarketsOverviewItem