import MarketsFuturesFilters from "./MarketsFuturesFilters";

const MarketsFuturesTableToolbar: React.FC<{ table: any }> = ({ table }) => {

  const filterFutures = (value: string) => {
    table.setFilter("category", value)
  }

  return (
    <div className="flex w-full justify-center items-center border-b border-[#f1f1f1] pb-6">
      <MarketsFuturesFilters onChange={filterFutures}  />
    </div>
  )
}

export default MarketsFuturesTableToolbar;