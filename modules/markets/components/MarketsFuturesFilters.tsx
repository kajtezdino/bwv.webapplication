import { useMemo, useState } from "react"

const MarketsFuturesFilters: React.FC<{ onChange: (value: string) => void }> = ({ onChange }) => {

  const [activeFilter, setActiveFilter] = useState<string>("")
  const filters = useMemo(() => {
    return ["", "Commodities", "Livestock", "Dairy", "Grain", "Oilseeds", "Fertilizer"]
  }, [])

  const onFilterChange = (value: string) => {
    onChange(value)
    setActiveFilter(value)
  }

  return (
    <>
      {filters.map(filter => (
        <div key={filter} onClick={() => onFilterChange(filter)}>
          {
            <div className={`cursor-pointer px-3 py-2 rounded-xl font-medium leading-5 mx-2 ${activeFilter === filter ? "text-bwv-accent bg-bwv-accent/20" : "bg-bwv-white-dirty text-bwv-label"}`}>
              <span>{filter === '' ? 'All Futures' : filter}</span>
            </div>
          }
        </div>
      )
      )}
    </>
  )
}

export default MarketsFuturesFilters;