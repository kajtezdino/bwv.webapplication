import { IMarketsResponse  } from "../../common/models";
import { http } from "../../common/utils/http";

class MarketsApiService {
  getMarketsData = (): Promise<IMarketsResponse> =>
    http<IMarketsResponse>({
      url: "https://yfapi.net/v6/finance/quote?region=US&lang=en&symbols=CADUSD%3DX%2CGF%3DF%2CLE%3DF%2CHE%3DF%2CKE%3DF%2CZO%3DF%2CZC%3DF%2CZS%3DF%2CCL%3DF%2CNG%3DF",
      headers: {
        "X-API-KEY": "8EXFxooAEq7j4kc95TojD2QejjdXP7L5UeCUS1ie",
      },
      method: "get"
    })
}

export default new MarketsApiService();