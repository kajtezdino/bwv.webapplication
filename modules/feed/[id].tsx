import { ChevronLeftIcon } from "@heroicons/react/solid";
import { collection, onSnapshot, orderBy, query } from "firebase/firestore";
import { useRouter } from 'next/router';
import { useEffect, useState } from "react";
import { db } from "../../firebase";
import { Post, Comment } from "./components";

const FeedPostContent: React.FC<{ id: string, post: any }> = ({ id, post }) => {
  const router = useRouter();
  const [comments, setComments] = useState<any>([]);
  
  useEffect(
    () =>
      onSnapshot(
        query(
          collection(db, "posts", id as string, "comments"),
          orderBy("timestamp", "desc")
        ),
        (snapshot) => setComments(snapshot.docs)
      ),
    [id]
  );
  
  return (
    <div className="flex flex-col gap-6 w-[700px] group rounded-xl">
      <div
        className="flex gap-4 items-center text-bwv-dark-gray transition-all hover:text-bwv-dark cursor-pointer"
        onClick={() => router.push('/')}
      >
        <ChevronLeftIcon
          height={32}
        />
        <span>Back to feed</span>
      </div>
      
      <Post id={id} post={post} />

      {comments.length > 0 && (
        <div className="-mt-9 pt-12 rounded-b-2xl bg-white">
          {comments.map((comment: any) => (
            <Comment
              key={comment.id}
              commentId={comment.id}
              postId={id}
              comment={comment.data()}
            />
          ))}
        </div>
      )}
    </div>
  )
}

export default FeedPostContent;