import Moment from "react-moment";
import Image from "next/image";
import { Avatar } from "../../../common/components";
import { Fragment, useEffect, useState } from "react";
import { collection, deleteDoc, doc, onSnapshot, orderBy, query, setDoc } from "firebase/firestore";
import { db } from "../../../firebase";
import { useRecoilValue } from "recoil";
import { userState } from "../../../store/atoms/userAtom";
import { CommentReplyInput, CommentReply } from ".";
import { Transition } from "@headlessui/react";

const Comment: React.FC<{commentId: string, comment: any, postId: string}> = ({
  commentId,
  comment,
  postId
}) => {
  const [liked, setLiked] = useState(false);
  const [likes, setLikes] = useState([]);
  const [disliked, setDisliked] = useState(false);
  const [dislikes, setDislikes] = useState([]);
  const [replies, setReplies] = useState<any>([]);
  const [showReplyInput, setShowReplyInput] = useState<boolean>(false)
  const [showReplies, setShowReplies] = useState<boolean>(false)
  const [commentUser, setCommentUser] = useState<any>({})
  const user: any = useRecoilValue(userState);

  useEffect(() => {
    onSnapshot(doc(db, "users", comment.userId), (snapshot) => {
      setCommentUser(snapshot.data());
    }) 
  }, [comment.userId])

  useEffect(
    () =>
      onSnapshot(collection(db, 'posts', postId, 'comments', commentId, 'likes'), (snapshot) =>
        setLikes(snapshot.docs as [])
      ),
    [commentId, postId]
  );

  useEffect(
    () =>
      onSnapshot(collection(db, 'posts', postId, 'comments', commentId, 'dislikes'), (snapshot) =>
        setDislikes(snapshot.docs as [])
      ),
    [commentId, postId]
  );

  useEffect(
    () =>
      onSnapshot(
        query(
          collection(db, 'posts', postId, 'comments', commentId, 'replies'),
          orderBy("timestamp", "asc")
        ),
        (snapshot) => setReplies(snapshot.docs)
      ),
    [commentId, postId]
  );

  useEffect(
    () =>
      setLiked(
        likes.findIndex((like: any) => like.id === user?.id) !== -1
      ),
    [likes, user?.id]
  );

  useEffect(
    () =>
      setDisliked(
        dislikes.findIndex((dislike: any) => dislike.id === user?.id) !== -1
      ),
    [dislikes, user?.id]
  );

  const likeComment = async (toggleDislike?: boolean) => {
    if (disliked && toggleDislike) dislikeComment();

    if (liked) {
      await deleteDoc(doc(db, 'posts', postId, 'comments', commentId, 'likes', user?.id))
    } else {
      await setDoc(doc(db, 'posts', postId, 'comments', commentId, 'likes', user?.id), {
        username: user.firstName
      })
    }
  }

  const dislikeComment = async (toggleLike?: boolean) => {
    if (liked && toggleLike) likeComment();

    if (disliked) {
      await deleteDoc(doc(db, 'posts', postId, 'comments', commentId, 'dislikes', user?.id))
    } else {
      await setDoc(doc(db, 'posts', postId, 'comments', commentId, 'dislikes', user?.id), {
        username: user.firstName
      })
    }
  }
  
  return (
    <div className="px-6 pb-4">
      <div className="flex bg-white gap-2 pb-2">
        <Avatar
          user={commentUser}
          size={48}
        />

        <div className="flex flex-col w-full gap-1">
          <div className="flex flex-col px-4 py-2 w-full bg-[#F8F8F8] rounded-2xl">
            <div className="inline-block">
              <h4 className="font-semibold text-[#222] text-sm inline-block">
                {commentUser.firstName} {" "} {commentUser.lastName}
              </h4>
              <span className="text-sm text-bwv-gray pl-2">
                <Moment fromNow>{comment?.timestamp?.toDate()}</Moment>
              </span>
            </div>
            <p className="mt-1 text-base text-[#333]">
              {comment?.comment}
            </p>
          </div>

          <div className="flex gap-2 items-center">
            <div className="flex items-center">
              <div
                className="flex items-center cursor-pointer"
                onClick={(e) => {
                  e.stopPropagation();
                  likeComment(true);
                }}
              >
                <div className="icon">
                  <Image
                    src={liked ? "/icons/bwv-like-green-icon.svg" : "/icons/bwv-like-icon.svg" }
                    width={15}
                    height={16}
                    alt="Like Icon"
                  />
                </div>
                {
                  likes.length ? (
                    <span className={`text-xs font-semibold ${liked ? "text-bwv-accent" : "text-bwv-dark-gray" }`}>
                      {likes.length}
                    </span>
                  ) : null
                }
              </div>
              <div
                className="icon"
                onClick={(e) => {
                  e.stopPropagation();
                  dislikeComment(true);
                }}
              >
                <Image
                  src={disliked ? "/icons/bwv-dislike-black-icon.svg" : "/icons/bwv-dislike-icon.svg"}
                  width={15}
                  height={16}
                  alt="Like Icon"
                />
              </div>
            </div>

            <span
              className={`text-xs font-semibold ${showReplyInput ? 'text-bwv-accent' : 'text-[#77808B]'} cursor-pointer hover:text-bwv-accent select-none`}
              onClick={() => setShowReplyInput(!showReplyInput)}
            >
              Reply
            </span>

            {
              replies.length > 0 &&
              <span
                className={`text-xs font-semibold pl-2 text-[#77808B] cursor-pointer hover:text-bwv-accent select-none`}
                onClick={() => setShowReplies(!showReplies)}
              >
                {!showReplies ? "View" : "Hide" } {replies.length} {replies.length > 1 ? "replies" : "reply" }
              </span> 
            }
          </div>
        </div>
      </div>
      {
        showReplyInput 
          && <CommentReplyInput
              user={user}
              postId={postId}
              commentId={commentId}
              onReplyEnd={() => { setShowReplies(true); setTimeout(() => setShowReplyInput(false), 200) }}
            />
      }

      <Transition appear show={showReplies} as={Fragment}>
        <div>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0 h-0"
            enterTo="opacity-100"
            leave="ease-out duration-100"
            leaveFrom="opacity-100"
            leaveTo="opacity-0 h-0"
          >
            <div>
              {
                replies.map((reply: any, index: number) => (
                  <CommentReply key={index} reply={reply.data()} />
              ))
              }
            </div>
          </Transition.Child>
        </div>
      </Transition>
    </div>
  )
}

export default Comment;

function setUser(arg0: import("@firebase/firestore").DocumentData | undefined) {
  throw new Error("Function not implemented.");
}
