import { useRecoilValue } from "recoil";
import { useState } from "react";
import { userState } from "../../../store/atoms/userAtom";
import {
  Avatar,
  Button,
  AutoTextArea,
  ImageBrowse
} from "../../../common/components";
import { Picker } from "emoji-mart";
import "emoji-mart/css/emoji-mart.css";
import Image from "next/image";
import { Menu } from "@headlessui/react";
import PostImagesGrid from './PostImagesGrid';
import ReactPlayer from 'react-player'
import VideoUplaod from './VideoUpload';
import { LinkPreview } from '@dhaiwat10/react-link-preview';
import { XIcon } from "@heroicons/react/solid";
import VideoUploadToggle from "./VideoUploadToggle";
import { db, storage } from "../../../firebase";
import {
  addDoc,
  collection,
  doc,
  serverTimestamp,
  updateDoc,
} from "@firebase/firestore";
import { getDownloadURL, ref, uploadBytes } from "@firebase/storage";
import { FirestoreUserService } from "../../../common/services";
import {onlySpaces} from "../../../common/utils";

const CreatePostInput: React.FC = () => {
  const user: any = useRecoilValue(userState)
  const [saving, setSaving] = useState<boolean>(false)
  const [input, setInput] = useState<string>('')
  const [selectedFiles, setSelectedFiles] = useState<File[] | null>(null)
  const [previewLink, setPreviewLink] = useState<string | null>(null)

  const [videoBrowse, showVideoBrowse] = useState<boolean>(false)
  const [selectedVideo, setSelectedVideo] = useState<string | { file: File, objectUrl: string } | null>(null)

  const addVideoToPost = (video: string | { file: File, objectUrl: string }) => {
    setSelectedVideo(video)
  };

  const addEmoji = (e: any) => {
    const sym = e.unified.split("-");
    const codesArray: any = [];
    sym.forEach((el: any) => codesArray.push("0x" + el));
    const emoji = String.fromCodePoint(...codesArray);
    setInput(input + emoji);
  };

  const hanledOnChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    setInput(e.target.value)
  }

  const handlePaste = (e: any) => {
    e.clipboardData.items[0].getAsString((text: string) => {
      if (
        isValidUrl(text)
        && !previewLink
        && !selectedVideo
        && !selectedFiles
      ) {
        setPreviewLink(text)
        showVideoBrowse(false);
      }
    })
  }

  const isValidUrl = (_string: string): boolean => {
    let url_string;
    try {
      url_string = new URL(_string);
    } catch (_) {
      return false;
    }
    return url_string.protocol === "http:" || url_string.protocol === "https:" ;
  }

  const savePost = async () => {
    setSaving(true)

    // Set user in users collection
    await FirestoreUserService.setUser(user)

    const docRef = await addDoc(collection(db, 'posts'), {
      id: user.id,
      userId: user.id,
      text: input,
      timestamp: serverTimestamp(),
      ...(previewLink && {previewLink: previewLink}),
      ...(selectedVideo && typeof selectedVideo === 'string' && {video: selectedVideo})
    });

    if (selectedFiles) {
      const imagesList: string[] = []
      let downloadURL, imageRef;

      for (const file of selectedFiles) {
        imageRef = ref(storage, `posts/${docRef.id}/${file.name}`);
        await uploadBytes(imageRef, file)
        downloadURL = await getDownloadURL(imageRef);
        imagesList.push(downloadURL)
      }
      await updateDoc(doc(db, "posts", docRef.id), {
        images: imagesList
      });
    }

    if (selectedVideo && typeof selectedVideo !== 'string') {
      const videoRef = ref(storage, `posts/${docRef.id}/video`);
      await uploadBytes(videoRef, (selectedVideo as any).file).then(async () => {
        const downloadURL = await getDownloadURL(videoRef);
        await updateDoc(doc(db, "posts", docRef.id), {
          video: downloadURL,
        });
      });
    }

    setSaving(false);
    setInput('');
    setSelectedFiles(null);
    setSelectedVideo(null)
    setPreviewLink(null)
  }

  const getThumbnailName = (str: string): string => {
    var idx = str.lastIndexOf(".");
    if (idx > -1) return str.substr(0, idx) + " _320x320.webp";
    return str
  }

  return (
    <div className="px-6 py-8 rounded-2xl bg-white">
      <div className="flex flex-col gap-4 items-start">
        <div className="flex flex-row justify-start w-full gap-3">
          <Avatar user={user} size={40} />

          <AutoTextArea
            className="bg-transparent resize-y outline-none px-2 py-[10px] text-base text-[#222] placeholder-[#A0A0A0] tracking-wide w-full flex-1 min-h-[50px]"
            value={input}
            onChange={hanledOnChange}
            onPaste={handlePaste}
            placeholder={`What’s happening, ${user?.firstName}?`}
          />
        </div>

        {
          previewLink && (
            <div className="w-full relative">
              <LinkPreview
                url={previewLink}
                width='100%'
                height='400px'
                showLoader
              />
              <div
                className="absolute z-10 w-6 h-6 bg-[#15181c] hover:bg-[#272c26] bg-opaciti-75 border border-white/20 rounded-full flex items-center justify-center top-1 right-1 cursor-pointer"
                onClick={() => setPreviewLink(null)}
              >
                <XIcon className="text-white h-5" />
              </div>
            </div>
          )
        }

        {
          selectedFiles && (
            <PostImagesGrid files={selectedFiles} onFileRemove={(e: File[]) => { setSelectedFiles(e.length ? e : null); }} />
          )
        }

        {
          videoBrowse && (
            <VideoUplaod
              onChange={(e: string | { file: File, objectUrl: string }) => { showVideoBrowse(false); addVideoToPost(e) }}
              onClose={() => showVideoBrowse(false)}
            />
          )
        }

        {
          selectedVideo && (
            <div className="relative rounded-2xl w-full pt-[56.25%] overflow-hidden">
              <ReactPlayer
                className="absolute top-0 left-0"
                width='100%'
                height='100%'
                controls
                url={(selectedVideo as any).objectUrl || selectedVideo}
              />

              <div
                className="absolute z-10 w-6 h-6 bg-[#15181c] hover:bg-[#272c26] bg-opaciti-75 border border-white/20 rounded-full flex items-center justify-center top-1 right-1 cursor-pointer"
                onClick={() => setSelectedVideo(null)}
              >
                <XIcon className="text-white h-5" />
              </div>
            </div>
          )
        }
      </div>

      <div className="flex justify-between items-center border-t border-[#F8F8F8] pt-[10px] mt-[10px]">
        <div className="flex gap-7">
          <ImageBrowse
            disabled={selectedFiles && selectedFiles.length > 4 || !!selectedVideo || !!previewLink}
            files={selectedFiles}
            onChange={(e: File[]) => { setSelectedFiles(e.length ? e : null); showVideoBrowse(false); }}
            icon="/icons/bwv-image-green-icon.svg"
            width={20}
            height={20}
            label="Add photos"
          />

          <VideoUploadToggle
            disabled={!!selectedFiles || !!previewLink}
            onClick={() => showVideoBrowse(true)}
          />

          <Menu as="div" className="relative flex items-center">
            <Menu.Button>
              <div className="flex gap-2 cursor-pointer relative items-center">
                <Image
                  src="/icons/bwv-emoji-green-icon.svg"
                  width={20}
                  height={20}
                  className="h-[22px] text-[#1d9bf0]"
                  alt="Image upload icon"
                />
                <span className="text text-sm text-[#252525]">Add emoji</span>
              </div>
            </Menu.Button>
            <Menu.Items className="absolute z-[999]">
              <Menu.Item>
                <Picker
                  onSelect={addEmoji}
                  style={{
                    position: "absolute",
                    top: "calc(100% + 12px)",
                    marginLeft: -40,
                    maxWidth: "320px",
                    borderRadius: "20px",
                  }}
                  theme="light"
                />
              </Menu.Item>
            </Menu.Items>
          </Menu>
        </div>

        <Button
          text="Post"
          loadingText="Posting"
          type="button"
          size="small"
          onClick={() => !!input && savePost()}
          loading={saving}
          disabled={saving || !input || onlySpaces(input)}
        />
      </div>
    </div>
  )
}

export default CreatePostInput;