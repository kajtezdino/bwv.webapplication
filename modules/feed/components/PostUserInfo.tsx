import { doc, onSnapshot } from "firebase/firestore";
import { useEffect, useState } from "react";
import Moment from "react-moment";
import { Avatar } from "../../../common/components";
import { db } from "../../../firebase";

const PostUserInfo: React.FC<{ userId: string, timestamp: any }> = ({ userId, timestamp }) => {
  const [user, setUser] = useState<any>({})

  useEffect(() => {
    onSnapshot(doc(db, "users", userId), (snapshot) => {
      setUser(snapshot.data());
    }) 
  }, [userId])

  return (
    <div className="flex gap-2 justify-start items-center pb-6">
      <Avatar user={user} size={56} />
      <div className="flex flex-col">
        <h4 className="text-base font-semibold text-[#252525]">{user?.firstName}{" "}{user?.lastName}</h4>
        <span className="text-sm text-bwv-accent"><Moment fromNow>{timestamp}</Moment></span>
      </div>
    </div>
  )
}

export default PostUserInfo;