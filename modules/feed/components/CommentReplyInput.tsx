import { Menu, Transition } from "@headlessui/react";
import { addDoc, collection, serverTimestamp } from "firebase/firestore";
import { useRouter } from "next/router";
import { Fragment, useState } from "react";
import Image from "next/image";
import { AutoTextArea, Avatar, Button } from "../../../common/components";
import { IUser } from "../../../common/models";
import { db } from "../../../firebase";
import { Picker } from "emoji-mart";
import { FirestoreUserService } from "../../../common/services";

const CommentReplyInput: React.FC<{user: IUser, postId: string, commentId: string, onReplyEnd: () => void}> = ({
  user,
  postId,
  commentId,
  onReplyEnd
}) => {
  const [reply, setReply] = useState<string>('')
  const [busy, setBusy] = useState<boolean>(false)
  const route = useRouter();
  
  const hanledOnChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    setReply(e.target.value)
  }

  const addEmoji = (e: any) => {
    const sym = e.unified.split("-");
    const codesArray: any = [];
    sym.forEach((el: any) => codesArray.push("0x" + el));
    const emoji = String.fromCodePoint(...codesArray);
    setReply(reply + emoji);
  };

  const addReply = async (e: any) => {
    setBusy(true)
    e.preventDefault();

    await FirestoreUserService.setUser(user)
    await addDoc(collection(db, "posts", postId, "comments", commentId, "replies"), {
      text: reply,
      userId: user.id,
      timestamp: serverTimestamp(),
    });

    setReply("")
    setBusy(false)
    onReplyEnd()
  }

  if (!db) return (<></>)

  return (
    <div className="relative pb-4 pl-14">
      <div className="flex gap-2 items-center pl-20px relative">
        <Avatar user={user} size={28}/>
        <AutoTextArea
          className="bg-bwv-white-dirty rounded-3xl resize-none outline-none pl-4 pr-11 py-2 text-sm text-[#222] placeholder-[#A0A0A0] tracking-wide w-full flex-1"
          value={reply}
          onChange={hanledOnChange}
          placeholder={`Leave a reply`}
        />

        <div className="absolute right-1 top-0">
          <Menu as="div" className="relative flex items-center">
            <Menu.Button>
              <div className="flex gap-2 cursor-pointer relative items-center icon">
                <Image
                  src="/icons/bwv-emoji-gray-icon.svg"
                  width={20}
                  height={20}
                  className="h-[22px] text-[#1d9bf0]"
                  alt="Image upload icon"
                />
              </div>
            </Menu.Button>
            <Menu.Items className="absolute z-40">
              <Menu.Item>
                <Picker
                  onSelect={addEmoji}
                  style={{
                    position: "absolute",
                    top: "calc(100% + 12px)",
                    marginLeft: -40,
                    maxWidth: "320px",
                    borderRadius: "20px",
                  }}
                  theme="light"
                />
              </Menu.Item>
            </Menu.Items>
          </Menu>
        </div>
      </div>
      <Transition appear show={!!reply} as={Fragment}>
        <div>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0 h-0"
            enterTo="opacity-100 h-14"
            leave="ease-in duration-100"
            leaveFrom="opacity-100 h-14"
            leaveTo="opacity-0 h-0"
          >
            <div className="text-right pt-4">
              <Button
                text="Reply"
                type="button"
                size="xs"
                onClick={addReply}
                disabled={busy}
                loading={busy}
                loadingText="Creating reply"
              />
            </div>
          </Transition.Child>
        </div>
      </Transition>
    </div>
  )
}

export default CommentReplyInput;