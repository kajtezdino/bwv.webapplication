import { doc, onSnapshot } from "firebase/firestore";
import { useEffect, useState } from "react";
import Moment from "react-moment";
import { Avatar } from "../../../common/components";
import { db } from "../../../firebase";

const CommentReply: React.FC<{ reply: any }> = ({ reply }) => {
  const [user, setUser] = useState<any>({})
  
  useEffect(() => {
    onSnapshot(doc(db, "users", reply.userId), (snapshot) => {
      setUser(snapshot.data());
    }) 
  }, [reply.userId])

  return (
    <div className="relative pb-4 pl-14">
      <div className="flex gap-2 items-start pl-20px relative">
        <div className="pt-1">
          <Avatar user={user} size={28}/>
        </div>
        <div className="bg-bwv-white-dirty rounded-3xl resize-none outline-none pl-4 pr-11 py-2 text-sm text-[#333] placeholder-[#A0A0A0] tracking-wide w-full flex-1">
          {reply.text}
        </div>
      </div>
      <span className="block text-sm text-bwv-gray pl-14 pt-1 -ml-1">
        {user.firstName} {user.lastName} · <Moment fromNow>{reply?.timestamp?.toDate()}</Moment>
      </span>
    </div>
  )
}

export default CommentReply;