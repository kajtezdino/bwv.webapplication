import Image from "next/image";
import { useRef } from "react";
import toast from "react-hot-toast";
import {
  FacebookShareButton,
  EmailShareButton,
  TwitterShareButton
} from 'react-share';
import { useRecoilValue } from "recoil";
import { CustomToast, DropdownMenu } from "../../../common/components";
import { ToastTypes } from "../../../common/enums";
import { userState } from "../../../store/atoms/userAtom";

const ShareMenu: React.FC<{ postId: string }> = ({ postId }) => {
  const fbRef: any = useRef();
  const mailRef: any = useRef();
  const twitterRef: any = useRef();
  const url = `${window.location.origin}/feed/${postId}`;
  const user: any = useRecoilValue(userState);
  
  const menuBtn = () => {
    return (
      <>
        <div className="flex items-center gap-1">
          <div className="icon" onClick={() => {}}>
            <Image
              src="/icons/bwv-share-icon.svg"
              width={21}
              height={18}
              alt="Share Icon"
            />
          </div>
          <span className="text-sm font-semibold">Share</span>
        </div>
        <div className="hidden">
          <FacebookShareButton
            ref={fbRef}
            url={url}
            quote={`BWV Feed Post by ${user?.firstName} ${user?.lastName}`}
            hashtag="#BusinessWideView"
          >
            <></>
          </FacebookShareButton>

          <EmailShareButton
            ref={mailRef}
            url={url}
            subject={`BWV Feed Post by ${user?.firstName} ${user?.lastName}`}
            body="Check this post on BusinessWideView web app. Join for free."
            separator={"\n\n"}
          >
            <></>
          </EmailShareButton>

          <TwitterShareButton
            ref={twitterRef}
            url={url}
            title={`BWV Feed Post by ${user?.firstName} ${user?.lastName}`}
            via="BusinessWideV"
            hashtags={['agriculture', 'ag', 'agapp', 'farmers', 'ranchers', 'farming', 'consumers', 'connection', 'innovation', 'business', 'canadian', 'canada', 'beef', 'cows', 'cowboy', 'grain', 'crop', 'app', 'webapp']}
          >
            <></>
          </TwitterShareButton>
        </div>
      </>
    )
  }

  const menuItems = [
    {
      name: "Email",
      onClick: () => {
        mailRef.current.click()
      },
      icon: {
        src: "/icons/bwv-mail-icon.svg",
        width: 19,
        height: 14,
        alt: "Copy link icon"
      }
    },
    {
      name: "Facebook",
      onClick: () => {
        fbRef.current.click()
      },
      icon: {
        src: "/icons/bwv-facebook-icon.svg",
        width: 12,
        height: 20,
        alt: "Copy link icon"
      }
    },
    {
      name: "Twitter",
      onClick: () => {
        twitterRef.current.click()
      },
      icon: {
        src: "/icons/bwv-twitter-icon.svg",
        width: 20,
        height: 20,
        alt: "Copy link icon"
      }
    },
    {
      name: "Copy link",
      onClick: () => {
        navigator.clipboard.writeText(url);
        toast((t) => (
          <CustomToast
            type={ToastTypes.SUCCESS}
            title="Copied"
            message="Link copied to the clipboard."
            t={t}
          />
        ))
      },
      icon: {
        src: "/icons/bwv-copy-icon.svg",
        width: 21,
        height: 20,
        alt: "Copy link icon"
      }
    }
  ]

  return (
    <DropdownMenu menuBtn={menuBtn} menuItems={menuItems} width={"w-44"} />
  )
}

export default ShareMenu;