import { useEffect, useRef, useState } from "react";
import Image from "next/image";
import { XIcon } from "@heroicons/react/solid";

const VideoUplaod: React.FC<{ onChange: (video: string | { file: File, objectUrl: string }) => void, onClose: () => void }> = ({
  onChange,
  onClose
}) => {
  const [video, setVideo] = useState<string | { file: File, objectUrl: string }>("");
  const filePickerRef = useRef(null);
  const videoInputRef = useRef(null);

  const addVideoFile = (e: any) => {
    setVideo({ file: e.target.files[0], objectUrl: URL.createObjectURL(e.target.files[0]) })
  }

  useEffect(() => {
    if (video) {
      onChange(video)
    }
  }, [onChange, video])

  return (
    <div className="relative w-full p-4 mb-7 border rounded-2xl bg-slate-50">
      <div
          className="absolute z-10 w-6 h-6 bg-[#15181c] hover:bg-[#272c26] bg-opaciti-75 rounded-full flex items-center justify-center top-1 right-1 cursor-pointer"
          onClick={onClose}
        >
          <XIcon className="text-white h-5" />
      </div>
      <div>
        <div
          className="flex flex-col items-center justify-center gap-3 py-7"
        >
          <Image
            src="/icons/bwv-circle-plus-green-icon.svg"
            width={24}
            height={24}
            alt="Image upload icon"
            className="cursor-pointer hover:opacity-70"
            onClick={() => (filePickerRef.current as any).click()}
          />
          <label htmlFor="video-file">Upload video from Computer</label>
          <input
            type="file"
            hidden
            name="video-file"
            accept="video/mp4"
            onChange={addVideoFile}
            ref={filePickerRef}
          />
        </div>
      </div>

      <div className="w-full">
        <label
          className="text-sm pb-2 inline-block"
          htmlFor="video-upload"
        >
          Or upload from a URL
        </label>
        <div className="relative w-full">
          <input
            className="px-4 py-[10px] pr-[60px] rounded-[15px] text-base border border-solid border-bwv-light-gray w-full placeholder:text-bwv-gray focus:border-bwv-accent focus:outline-0"
            type="text"
            name="video-upload"
            placeholder="https://www.youtube.com/"
            ref={videoInputRef}
          />
          <button
            className="absolute right-4 top-1/2 transform -translate-y-1/2 text-sm font-semibold text-[#A0A0A0]"
            onClick={() => setVideo((videoInputRef?.current as any).value)}
          >
            Upload
          </button>
        </div>
      </div>
    </div>
  )
}

export default VideoUplaod;