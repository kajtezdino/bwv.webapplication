import Image from "next/image";

const VideoUploadToggle: React.FC<{ disabled: boolean, onClick: () => void }> = ({
  disabled,
  onClick
}) => {
  return (
    <div
      className={`flex gap-2 cursor-pointer items-center ${disabled ? "opacity-50 cursor-default select-none pointer-events-none" : ""}`}
      onClick={onClick}
    >
      <Image
        src="/icons/bwv-video-upload-green-icon.svg"
        width={20}
        height={20}
        alt="Image upload icon"
      />
      <span className="text text-sm text-[#252525]">Add video</span>
    </div>
  )
}

export default VideoUploadToggle;