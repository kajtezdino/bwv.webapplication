import { useEffect, useState } from 'react';
import { ImageGalleryModal, ImagePreview } from '../../../common/components';

const PostImagesGrid: React.FC<{files: File[], onFileRemove?: (files: File[]) => void, previewOnly?: boolean}> = ({
  files,
  onFileRemove = () => null,
  previewOnly = false
}) => {
  const [selectedFiles, setSelectedFiles] = useState<any>(null)
  const [modalIsOpen, setModalIsOpen] = useState<boolean>(false)
  const [selectedImageIndex, steSelectedImageIndex] = useState<number>(0)

  useEffect(() => {
    if (files) {
      setSelectedFiles(files);
    }
  }, [files])

  const removeImage = (file: File) => {
    setSelectedFiles(selectedFiles.filter((f: File) => f.name !== file.name))
    onFileRemove(files.filter((f: File) => f.name !== file.name))
  }

  const openGalleryModal = (imageIndex: number) => {
    if (!previewOnly) return

    steSelectedImageIndex(imageIndex)
    setModalIsOpen(true)
  }

  if (!selectedFiles) return (<></>);

  const displayGrid = () => {
    switch(selectedFiles.length) {
      case 1:
      case 2:
        return (
          <div className={`flex gap-4 h-[320px]`}>
            {
              selectedFiles.map((file: File, index: number) => (
                <div className='w-full cursor-pointer hover:opacity-90' key={index} onClick={() => openGalleryModal(index)}>
                  <ImagePreview file={file} onImageRemove={removeImage} previewOnly={previewOnly} />
                </div>
              ))
            }
          </div>
        )
        break;
      case 3:
        return (
          <div className="flex gap-4 w-full h-[320px]">
            <div className="w-1/2 h-full">
              <div className='w-full h-full cursor-pointer hover:opacity-90' onClick={() => openGalleryModal(0)}>
                <ImagePreview file={selectedFiles[0]} onImageRemove={removeImage} previewOnly={previewOnly} />
              </div>
            </div>

            <div className="grid grid-flow-row grid-rows-2 gap-4 w-1/2 h-full">
              <div className='relative w-full cursor-pointer hover:opacity-90' onClick={() => openGalleryModal(1)}>
                <ImagePreview file={selectedFiles[1]} onImageRemove={removeImage} previewOnly={previewOnly} />
              </div>
              <div className='relative w-full cursor-pointer hover:opacity-90' onClick={() => openGalleryModal(2)}>
                <ImagePreview file={selectedFiles[2]} onImageRemove={removeImage} previewOnly={previewOnly} />
              </div>
            </div>
          </div>
        )
        break;
      case 4:
        return (
          <div className="flex gap-4 w-full h-[320px]">
            <div className="w-1/2 h-full">
              <div className='w-full h-full cursor-pointer hover:opacity-90' onClick={() => openGalleryModal(0)}>
                <ImagePreview file={selectedFiles[0]} onImageRemove={removeImage} previewOnly={previewOnly} />
              </div>
            </div>

            <div className="grid grid-flow-row grid-rows-2 gap-4 w-1/2 h-full">
              <div className='w-full cursor-pointer hover:opacity-90' onClick={() => openGalleryModal(1)}>
                <ImagePreview file={selectedFiles[1]} onImageRemove={removeImage} previewOnly={previewOnly} />
              </div>
              <div className='flex w-full gap-4'>
                <div className='w-full h-full cursor-pointer hover:opacity-90' onClick={() => openGalleryModal(2)}>
                  <ImagePreview file={selectedFiles[2]} onImageRemove={removeImage} previewOnly={previewOnly} />
                </div>
                <div className='w-full h-full cursor-pointer hover:opacity-90' onClick={() => openGalleryModal(3)}>
                  <ImagePreview file={selectedFiles[3]} onImageRemove={removeImage} previewOnly={previewOnly} />
                </div>
              </div>
            </div>
          </div>
        )
        break;
      default:
        return (
          <div className="flex gap-4 w-full h-[320px]">
            <div className="w-1/2 h-full">
              <div className='w-full h-full cursor-pointer hover:opacity-90' onClick={() => openGalleryModal(0)}>
                <ImagePreview file={selectedFiles[0]} onImageRemove={removeImage} previewOnly={previewOnly} />
              </div>
            </div>

            <div className="grid grid-flow-row grid-rows-2 gap-4 w-1/2 h-full">
              <div className='flex w-full gap-4'>
                <div className='w-full h-full cursor-pointer hover:opacity-90' onClick={() => openGalleryModal(1)}>
                  <ImagePreview file={selectedFiles[1]} onImageRemove={removeImage} previewOnly={previewOnly} />
                </div>
                <div className='w-full h-full cursor-pointer hover:opacity-90' onClick={() => openGalleryModal(2)}>
                  <ImagePreview file={selectedFiles[2]} onImageRemove={removeImage} previewOnly={previewOnly} />
                </div>
              </div>
              <div className='flex w-full gap-4'>
                <div className='w-full h-full cursor-pointer hover:opacity-90' onClick={() => openGalleryModal(3)}>
                  <ImagePreview file={selectedFiles[3]} onImageRemove={removeImage} previewOnly={previewOnly} />
                </div>
                <div className='w-full h-full cursor-pointer hover:opacity-90' onClick={() => openGalleryModal(4)}>
                  <ImagePreview file={selectedFiles[4]} onImageRemove={removeImage} previewOnly={previewOnly} />
                </div>
              </div>
            </div>
          </div>
        )
    }
    
  }

  return (
    <>
      {displayGrid()}

      <ImageGalleryModal
        isOpen={modalIsOpen}
        imageList={files}
        startIndex={selectedImageIndex}
        onClose={() => setModalIsOpen(false)}
      />
    </>
  )
}

export default PostImagesGrid;