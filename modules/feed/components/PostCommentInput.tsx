import { Menu, Transition } from "@headlessui/react";
import { addDoc, collection, serverTimestamp } from "firebase/firestore";
import { useRouter } from "next/router";
import { Fragment, useState } from "react";
import Image from "next/image";
import toast from "react-hot-toast";
import { AutoTextArea, Avatar, Button, CustomToast, NavLink } from "../../../common/components";
import { ToastTypes } from "../../../common/enums";
import { IUser } from "../../../common/models";
import { db } from "../../../firebase";
import { Picker } from "emoji-mart";
import { FirestoreUserService } from "../../../common/services";

const PostCommentInput: React.FC<{user: IUser, postId: string}> = ({ user, postId }) => {
  const [comment, setComment] = useState<string>('')
  const [busy, setBusy] = useState<boolean>(false)
  const route = useRouter();
  
  const hanledOnChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    setComment(e.target.value)
  }

  const addEmoji = (e: any) => {
    const sym = e.unified.split("-");
    const codesArray: any = [];
    sym.forEach((el: any) => codesArray.push("0x" + el));
    const emoji = String.fromCodePoint(...codesArray);
    setComment(comment + emoji);
  };

  const goToPostPage = () => {
    route.push(`/feed/${postId}`)
  }

  const addComment = async (e: any) => {
    setBusy(true)
    e.preventDefault();

    await FirestoreUserService.setUser(user);
    await addDoc(collection(db, "posts", postId, "comments"), {
      comment: comment,
      userId: user.id,
      timestamp: serverTimestamp(),
    });

    setBusy(false)
    setComment("")

    toast((t) => (
      <CustomToast
        type={ToastTypes.SUCCESS}
        title="Success"
        message={() => (
          <div>
            Comment created. <span className="underline cursor-pointer" onClick={goToPostPage}>Check all post comments.</span>
          </div>
        )}
        t={t}
      />
    ))
  }

  return (
    <>
      <div className="flex gap-4 items-center border-t border-bwv-white-dirty pt-6 mt-6 relative">
        <Avatar user={user} size={40}/>
        <AutoTextArea
          className="bg-bwv-white-dirty rounded-3xl resize-none outline-none pl-4 pr-11 py-3 text-base text-[#222] placeholder-[#A0A0A0] tracking-wide w-full flex-1"
          value={comment}
          onChange={hanledOnChange}
          placeholder={`Leave a comment`}
        />

        <div className="absolute right-1 top-[27px] pt-[2px]">
          <Menu as="div" className="relative flex items-center">
            <Menu.Button>
              <div className="flex gap-2 cursor-pointer relative items-center icon">
                <Image
                  src="/icons/bwv-emoji-gray-icon.svg"
                  width={20}
                  height={20}
                  className="h-[22px] text-[#1d9bf0]"
                  alt="Image upload icon"
                />
              </div>
            </Menu.Button>
            <Menu.Items className="absolute z-40">
              <Menu.Item>
                <Picker
                  onSelect={addEmoji}
                  style={{
                    position: "absolute",
                    top: "calc(100% + 12px)",
                    marginLeft: -40,
                    maxWidth: "320px",
                    borderRadius: "20px",
                  }}
                  theme="light"
                />
              </Menu.Item>
            </Menu.Items>
          </Menu>
        </div>
      </div>
      <Transition appear show={!!comment} as={Fragment}>
        <div>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0 h-0"
            enterTo="opacity-100 h-14"
            leave="ease-in duration-100"
            leaveFrom="opacity-100 h-14"
            leaveTo="opacity-0 h-0"
          >
            <div className="text-right pt-4">
              <Button
                text="Comment"
                type="button"
                size="small"
                onClick={addComment}
                disabled={busy}
                loading={busy}
                loadingText="Creating comment"
              />
            </div>
          </Transition.Child>
        </div>
      </Transition>
    </>
  )
}

export default PostCommentInput;