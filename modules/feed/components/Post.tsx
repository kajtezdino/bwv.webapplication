import { useEffect, useState } from "react";
import Image from "next/image";
import { LinkPreview } from '@dhaiwat10/react-link-preview';
import ReactPlayer from "react-player";
import { useRecoilValue } from "recoil";
import { userState } from "../../../store/atoms/userAtom";
import {
  collection,
  deleteDoc,
  doc,
  onSnapshot,
  orderBy,
  query,
  setDoc
} from "firebase/firestore";
import { db } from "../../../firebase";
import {
  PostCommentInput,
  PostUserInfo,
  ShareMenu
} from '.';
import { useRouter } from "next/router";
import { ImagesGrid } from "../../../common/components";

const Post: React.FC<{ id: string, post: any, isPage?: boolean }> = ({
  id,
  post
}) => {
  const router = useRouter()
  const [likes, setLikes] = useState([]);
  const [liked, setLiked] = useState(false);
  const [dislikes, setDislikes] = useState([]);
  const [disliked, setDisliked] = useState(false);
  const [comments, setComments] = useState<any>([])
  const user: any = useRecoilValue(userState);

  useEffect(
    () =>
      onSnapshot(
        query(
          collection(db, "posts", id, "comments"),
          orderBy("timestamp", "desc")
        ),
        (snapshot) => setComments(snapshot.docs)
      ),
    [id]
  );

  useEffect(
    () =>
      onSnapshot(collection(db, "posts", id, "likes"), (snapshot) =>
        setLikes(snapshot.docs as [])
      ),
    [id]
  );

  useEffect(
    () =>
      onSnapshot(collection(db, "posts", id, "dislikes"), (snapshot) =>
        setDislikes(snapshot.docs as [])
      ),
    [id]
  );

  useEffect(
    () =>
      setLiked(
        likes.findIndex((like: any) => like.id === user?.id) !== -1
      ),
    [likes, user?.id]
  );

  useEffect(
    () =>
      setDisliked(
        dislikes.findIndex((dislike: any) => dislike.id === user?.id) !== -1
      ),
    [dislikes, user?.id]
  );
  
  const likePost = async (toggleDislike?: boolean) => {
    if (disliked && toggleDislike) dislikePost();

    if (liked) {
      await deleteDoc(doc(db, 'posts', id, 'likes', user?.id))
    } else {
      await setDoc(doc(db, 'posts', id, 'likes', user?.id), {
        username: user.firstName
      })
    }
  }

  const dislikePost = async (toggleLike?: boolean) => {
    if (liked && toggleLike) likePost();

    if (disliked) {
      await deleteDoc(doc(db, 'posts', id, 'dislikes', user?.id))
    } else {
      await setDoc(doc(db, 'posts', id, 'dislikes', user?.id), {
        username: user.firstName
      })
    }
  }

  return (
    <div className="p-6 bg-white rounded-2xl mb-8 group-last:mb-0">
      <PostUserInfo userId={post?.userId} timestamp={post?.timestamp?.toDate()} />
      
      <p className="text-base font-normal text-[#252525] pb-6">{post?.text}</p>

      {
        post.previewLink && (
          <LinkPreview
            url={post.previewLink}
            width='100%'
            height='400px'
            showLoader
          />
        )
      }

      {
        post.images && (
          <ImagesGrid files={post.images} previewOnly/>
        )
      }

      {
        post.video && (
          <div className="relative rounded-2xl w-full pt-[56.25%] overflow-hidden">
            <ReactPlayer
              className="absolute top-0 left-0"
              width='100%'
              height='100%'
              controls
              url={post?.video}
            />
          </div>
        )
      }

      <div className="px-6 pt-6 flex justify-between items-center">
        <div className="flex gap-2 items-center">
          <div
            className="flex gap-1 items-center cursor-pointer"
            onClick={(e) => {
              e.stopPropagation();
              likePost(true);
            }}
          >
            <div className="icon">
              {
                liked ? (
                  <Image
                    src="/icons/bwv-like-green-icon.svg"
                    width={17}
                    height={18}
                    alt="Like Icon"
                  />
                ) : (
                  <Image
                    src="/icons/bwv-like-icon.svg"
                    width={17}
                    height={18}
                    alt="Liked Icon"
                  />
                )
              }
            </div>
            {
              likes.length ? (
                <span className={`text-sm font-semibold ${liked ? "text-bwv-accent" : "text-bwv-dark-gray" }`}>
                  {likes.length}
                </span>
              ) : null
            }
          </div>
          <div
            className="icon"
            onClick={(e) => {
              e.stopPropagation();
              dislikePost(true);
            }}
          >
            {
              disliked ? (
                <Image
                  src="/icons/bwv-dislike-black-icon.svg"
                  width={17}
                  height={18}
                  alt="Like Icon"
                />
              ) : (
                <Image
                  src="/icons/bwv-dislike-icon.svg"
                  width={17}
                  height={18}
                  alt="Like Icon"
                />
              )
            }
          </div>
        </div>

        <div className="flex items-center gap-1">
          <div className="icon" onClick={() => router.push(`/feed/${id}`)}>
            <Image
              src="/icons/bwv-comments-black-icon.svg"
              width={18}
              height={17}
              alt="Like Icon"
            />
          </div>
          <span className="text-sm font-semibold">{comments?.length} Comment{comments?.length === 1 ? "" : "s"}</span>
        </div>

        <ShareMenu postId={id}/>
      </div>

      <PostCommentInput user={user} postId={id} />
    </div>
  )
}

export default Post;