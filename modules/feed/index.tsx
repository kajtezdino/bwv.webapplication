import { useEffect, useState } from "react";
import { onSnapshot, collection, query, orderBy } from "@firebase/firestore";
import { db } from "../../firebase";
import { CreatePostInput } from "./components"
import Post from "./components/Post";

const FeedContent: React.FC = () => {
  const [posts, setPosts] = useState([]);

  useEffect(() => {
    onSnapshot(
      query(collection(db, "posts"), orderBy("timestamp", "desc")),
      (snapshot) => {
        setPosts(snapshot.docs as any);
      }
    )
  }, [db])
  
  return (
    <div className="flex flex-col gap-6 w-[700px] group">
      <CreatePostInput />

      {posts.map((post: any) => (
        <Post
          key={post.id}
          id={post.id}
          post={post.data()}
        />
      ))}
    </div>
  )
}

export default FeedContent