import { useEffect, useState } from "react";
import { IBuyAndSellListingsFilters, IBuyAndSellListingsResponse } from "../../common/models";
import { Listings, ListingsFilters, ListingsSortDropdown } from "./components";
import { useRecoilValue, useSetRecoilState } from "recoil";
import adListingsState from "../../store/atoms/adListingsAtom";
import BuyAndSellApiService from "./api.service";
import adListingsFiltersState from "../../store/atoms/adListingsFiltersAtom";
import toast from "react-hot-toast";
import { Button, CustomToast } from "../../common/components";
import { ToastTypes } from "../../common/enums";
import AdGroupFilter from "./components/ListingsFilters/AdGroupFilter";
import { useRouter } from 'next/router';

const BuyAndSellContent: React.FC = () => {
  const router = useRouter();
  const filters: IBuyAndSellListingsFilters = useRecoilValue(adListingsFiltersState);
  const listings: IBuyAndSellListingsResponse = useRecoilValue(adListingsState);
  const setListings = useSetRecoilState(adListingsState);
  const [busy, setBusy] = useState<boolean>(false)

  const fetchListings = async (filters: IBuyAndSellListingsFilters) => {
    try {
      setBusy(true)
      setListings(await BuyAndSellApiService.fetchListings(filters));
    } catch (error: any) {
      toast((t) => (
        <CustomToast
          type={ToastTypes.ERROR}
          title="Error"
          message={error.response.data.title}
          t={t}
        />
      ))
    } finally {
      setBusy(false)
    }
  }

  useEffect(() => {
    fetchListings(filters)
  }, [filters])

  return (
    <div className="w-[838px] m-auto max-w-full">
      <div className="flex justify-between items-center pb-8">
        <AdGroupFilter />
        <Button
          text="+ Add listing"
          onClick={() => router.push("/buy-and-sell/add-new-listing")}
          size="small"
        />
      </div>

      <ListingsFilters />
      
      <div className="flex justify-between py-7">
        <ListingsSortDropdown />
        <span className="text-sm text-bwv-accent font-semibold">{ listings.totalNumber } Ad{listings.totalNumber > 1 ? 's' : ''} found</span>
      </div>
      <Listings list={listings.ads} loading={busy} />
    </div>
  )
};

export default BuyAndSellContent;