import { yupResolver } from "@hookform/resolvers/yup";
import { useEffect, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { useRecoilValue, useSetRecoilState } from "recoil";
import {
  AutoTextArea,
  Button,
  ComboboxInput,
  Input,
  CustomToast,
  LocationInput
} from "../../common/components";
import { ToastTypes } from "../../common/enums";
import {
  IAddNewListing,
  IAddNewListingForm,
  ICategoryAndSubcategory,
  ISubcategory,
  ITypesGroupsAndConditions,
  IUserProfile
} from "../../common/models";
import { AddNewListingSchema } from "../../common/schemas";
import BuyAndSellApiService from "./api.service";
import { userState } from '../../store/atoms/userAtom'
import toast from "react-hot-toast";
import CurrencyFormat from "react-currency-format";
import CoverSelection from "./components/CoverSelection";
import FilesSelection from "./components/FileSelection";
import Image from "next/image";
import { typesGroupsAndConditions } from "../../store/atoms/typesGroupsConditionsAtom";

const AddNewListingContent: React.FC = () => {
  const [categoriesAndSubcategories, setCategoriesAndSubcategories] = useState<ICategoryAndSubcategory[]>([]);
  const [subcategories, setSubcategories] = useState<ISubcategory[]>([]);
  const [busy, setBusy] = useState<boolean>(false);
  const user = useRecoilValue<IUserProfile>(userState)
  const setTypesGroupsAndConditions = useSetRecoilState(typesGroupsAndConditions);
  const typesGroupsAndConditionsValues: ITypesGroupsAndConditions = useRecoilValue(typesGroupsAndConditions)
  const { handleSubmit, control, formState: { isValid } } = useForm<IAddNewListingForm>({
    mode: 'onChange',
    reValidateMode: 'onChange',
    resolver: yupResolver(AddNewListingSchema)
  });
  const [selectedCover, setSelectedCover] = useState<File | null>(null)
  const [selectedFiles, setSelectedFiles] = useState<File[] | null>(null)

  const onSubmit = async (data: IAddNewListing) => {
    setBusy(true);
    try {
      await BuyAndSellApiService.createAd(data)
      toast((t) => (
        <CustomToast
          type={ToastTypes.SUCCESS}
          title="Success"
          message="You have successfully created Ad!!"
          t={t}
        />
      ))
    } catch (error: any) {
      toast((t) => (
        <CustomToast
          type={ToastTypes.ERROR}
          title="Error"
          message={error.response}
          t={t}
        />
      ))
    } finally {
      setBusy(false)
    }
  }

  const handleOnSubmit = (data: IAddNewListingForm) => {
    onSubmit({
      title: data.title,
      location: data.location.location,
      longitude: data.location.longitude,
      latitude: data.location.latitude,
      adConditionID: data.adConditionID,
      price: Number(data.price),
      coverImageUrl: selectedCover,
      adImages: selectedFiles,
      description: data.description,
      adTypeID: data.adTypeID,
      adGroupID: data.adGroupID,
      categoryID: data.categoryID,
      subcategoryID: data.subcategoryID,
      createdBy: user.id,
    });
  }

  const fetchCategoriesAndSubcategories = async () => {
    try {
      const categoriesAndSubcategories = await BuyAndSellApiService.getCategoriesAndSubcategories()
      setCategoriesAndSubcategories(categoriesAndSubcategories)
    } catch (error: any) {
      toast((t) => (
        <CustomToast
          type={ToastTypes.ERROR}
          title="Error"
          message={error.response}
          t={t}
        />
      ))
    }
  }

  const fetchTypesGroupsAndConditions = async () => {
    try {
      const response = await BuyAndSellApiService.getTypesGroupsAndConditions()
      setTypesGroupsAndConditions(response)
    } catch (error: any) {
      toast((t) => (
        <CustomToast
          type={ToastTypes.ERROR}
          title="Error"
          message={error.response}
          t={t}
        />
      ))
    }
  }

  const handleImagesChange = (e: File[] | null) => {
    setSelectedFiles(e);
  }

  const handleCoverChange = (e: File | null) => {
    setSelectedCover(e);
  }

  useEffect(() => {
    fetchCategoriesAndSubcategories()
    fetchTypesGroupsAndConditions()
  }, [])

  return (
    <div className="w-[876px] bg-white rounded-2xl p-8">
      <CoverSelection coverFile={selectedCover} onChange={handleCoverChange} />
      <form className="mt-6" onSubmit={handleSubmit(handleOnSubmit)}>
        <p className="text-2xl border-b pt-8">Info</p>
        <div className="pt-8 gap-8 flex flex-row justify-between">
          <div className="w-1/2">
            <Controller
              control={control}
              name="title"
              render={({ formState, field: { onBlur, onChange } }) => (
                <Input
                  label="Title"
                  name="title"
                  placeholder="Put a title"
                  onBlur={onBlur}
                  onChange={onChange}
                  formState={formState}
                  required
                />
              )}
            />
          </div>
          <div className="w-1/2">
            <Controller
              control={control}
              name="adTypeID"
              render={({ formState, field: { onBlur, onChange } }) => (
                <ComboboxInput
                  label="Ad type"
                  placeholder="Choose a type"
                  name="adType"
                  options={typesGroupsAndConditionsValues.adTypes}
                  valueKey="id"
                  labelKey="name"
                  formState={formState}
                  onBlur={onBlur}
                  onChange={onChange}
                  defaultValue={null}
                  required
                />
              )}
            />
          </div>
        </div>
        <div className="flex flex-row justify-between gap-8 mb-4">
          <div className="w-1/2">
            <Controller
              control={control}
              name="location"
              render={({ field: { onBlur, onChange } }) => (
                <LocationInput
                  onBlur={onBlur}
                  onChange={onChange}
                  placeholder="Select location"
                  defaultValue={null}
                  label="Location"
                  alt="Location Icon"
                  withCoordinates
                />
              )}
            />
          </div>
          <div className="w-1/2">
            <Controller
              control={control}
              name="categoryID"
              render={({ formState, field: { onChange, onBlur, } }) => (
                <ComboboxInput
                  label="Product Category"
                  placeholder="Choose a category"
                  options={categoriesAndSubcategories}
                  formState={formState}
                  defaultValue={null}
                  labelKey="name"
                  onChange={(value: any) => {
                    onChange(value.categoryID);
                    setSubcategories(value.subcategories ?? []);
                  }}
                  onBlur={onBlur}
                  name="category"
                  required
                />
              )}
            />
          </div>
        </div>
        <div className="flex flex-row justify-between gap-8">
          <div className="w-1/2">
            <Controller
              control={control}
              name="adConditionID"
              render={({ formState, field }) => (
                <ComboboxInput
                  label="condition"
                  placeholder="Choose a condition"
                  options={typesGroupsAndConditionsValues.adConditions}
                  valueKey="id"
                  labelKey="name"
                  formState={formState}
                  defaultValue={null}
                  required
                  {...field} />
              )}
            />
          </div>
          <div className="w-1/2">
            <Controller
              control={control}
              name="subcategoryID"
              render={({ formState, field }) => (
                <ComboboxInput
                  label="Sub Category"
                  placeholder="Choose a subcategory"
                  options={subcategories}
                  valueKey="subcategoryID"
                  labelKey="name"
                  formState={formState}
                  defaultValue={''}
                  required
                  {...field} />
              )}
            />
          </div>
        </div>
        <div className="flex flex-row justify-between mt-4 gap-8">
          <div className="w-1/2">
            <div className="flex items-start">
              <p className="uppercase font-semibold text-sm text-bwv-label inline-block">Price</p>
              <Image src="/icons/bwv-required-icon.svg" width={7} height={7} alt="required icon" />
              <span className="text-bwv-error text-xs font-bold"> (when user select for sale)</span>
            </div>
            <Controller
              control={control}
              name="price"
              render={({ field: { onChange, ...rest } }) => (
                <CurrencyFormat
                  className="input-small mt-2 mb-4 pl-4"
                  displayType={'input'}
                  placeholder="Put a price"
                  thousandSeparator={true}
                  prefix={'$'}
                  onValueChange={(values) => {
                    const { value } = values;
                    onChange(value)
                  }}
                  defaultValue={0}
                  {...rest}
                />
              )}
            />
          </div>
          <div className="w-1/2">
            <Controller
              control={control}
              name="adGroupID"
              render={({ formState, field }) => (
                <ComboboxInput
                  label="Item for sale / Looking for"
                  placeholder="Choose a group"
                  options={typesGroupsAndConditionsValues.adGroups}
                  valueKey="id"
                  labelKey="name"
                  formState={formState}
                  defaultValue={null}
                  required
                  {...field} />
              )}
            />
          </div>
        </div>
        <div>
          <Controller
            control={control}
            name="description"
            render={({ formState, field: { onChange } }) => (
              <AutoTextArea
                className="w-full input-small min-h-[100px]"
                onChange={onChange}
                placeholder="Put a description"
                label="description"
                required
              />
            )}
          />
        </div>
        <div>
          <p className="uppercase my-8 font-semibold text-sm text-bwv-label inline-block">Photos</p>
          <FilesSelection files={selectedFiles} onChange={handleImagesChange} />
        </div>
        <div className="flex justify-between mb-24">
          <div className="py-2 rounded-2xl px-6 bg-bwv-white-dirty w-[560px]">
            <p>Maximum number of uploaded pictures is 5!</p>
          </div>
          <div>
            <Button type="submit" text="Publish" size="small" disabled={!isValid} loading={busy} loadingText="Creating Ad" />
          </div>
        </div>
      </form>
    </div>
  )
}

export default AddNewListingContent;