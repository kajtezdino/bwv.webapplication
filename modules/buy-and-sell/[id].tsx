import CurrencyFormat from "react-currency-format";
import { ImagesGrid } from "../../common/components";
import { ChevronLeftIcon } from "@heroicons/react/solid";
import { useRouter } from "next/router";
import {
  CoverPhoto,
  SellerInfo,
  ContactForm,
  Map
} from "./components/[id]";

const BuyAndSellAdContent: React.FC<{ ad: any }> = ({ ad }) => {
  const router = useRouter();

  return (
    <>
      <div
        className="w-[876px] max-w-full m-auto flex gap-4 items-center text-bwv-dark-gray transition-all hover:text-bwv-dark cursor-pointer mb-4"
        onClick={() => router.push('/buy-and-sell')}
      >
        <ChevronLeftIcon
          height={32}
        />
        <span>Back to listings</span>
      </div>
      <div className="w-[876px] m-auto max-w-full bg-white rounded-2xl overflow-hidden">
        {/* Cover photo */}
        <CoverPhoto src={ad.coverImageUrl} />
        {/* Title and price */}
        <div className="p-6">
          <div className="flex justify-between items-center mb-14">
            <h1 className="t text-3xl font-medium">{ ad?.title }</h1>
            <h2 className="t text-3xl text-bwv-accent font-semibold">
              <CurrencyFormat value={ad?.price} displayType={'text'} thousandSeparator={true} prefix={'$'} />
            </h2>
          </div>
          {/* Desctiption */}
          <div>
            <h3 className="block text-2xl font-medium border-b pb-1 mb-6">
              Description
            </h3>
            <p>{ ad?.description }</p>
          </div>
          {/* Photos and location */}
          <div className="pt-7 mb-14">
            <h3 className="block text-2xl font-medium border-b pb-1 mb-6">
              Photos & Location
            </h3>
            <div className="flex justify-between gap-6 items-center h-[200px]">
              {
                ad?.adImages?.length > 0
                && <div className="w-1/2 h-full">
                  <ImagesGrid
                    files={ad?.adImages}
                    previewOnly
                    height="h-full"
                  />
                </div>
              }
              <Map ad={ad} />
            </div> 
          </div>
          {/* Contact form */}
          <div>
            <h3 className="block text-2xl font-medium border-b pb-1 mb-6">
              Seller Info & Contact
            </h3>
            <div className="w-full flex justify-between items-center gap-16">
              <SellerInfo userId={ad?.createdBy} />
              <ContactForm />
            </div>
          </div>
        </div>
      </div>
    </>
  )
};

export default BuyAndSellAdContent;