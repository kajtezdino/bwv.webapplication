import { ChangeEvent, useState } from "react";
import { AutoTextArea, Button } from "../../../../common/components";

const ContactForm: React.FC = () => {
  const [message, setMessage] = useState<string>('')
  const [busy, setBusy] = useState<boolean>(false)

  const sendMessage = () => {
    setBusy(true)
  }

  return (
    <div className="flex-1">
      <h3 className="text-lg font-semibold">Contact form</h3>
      <AutoTextArea
        className="bg-bwv-white-dirty rounded-xl border-white border focus-visible:border-bwv-accent focus:outline-none py-2 px-3 min-h-[180px] w-full mt-4 mb-3"
        placeholder="Write a message to the seller"
        onChange={(e: ChangeEvent<HTMLTextAreaElement>) => setMessage(e.target.value)}
        disabled={busy}
      />
      <Button
        text="Send"
        size="small"
        type="button"
        onClick={sendMessage}
        disabled={!message || busy}
        loading={busy}
        loadingText="Sending"
      />
    </div>
  )
};

export default ContactForm;