import { useEffect, useState } from 'react';
import { Avatar } from '../../../../common/components';
import { IUser } from '../../../../common/models';
import { UserApiService } from '../../../../common/services';
import Image from 'next/image';

const SellerInfo: React.FC<{ userId: string }> = ({ userId }) => {
  const [seller, setSeller] = useState<IUser>();
  
  useEffect(() => {
    getSellerInfo()
  }, [])

  const getSellerInfo = async () => {
    try {
      setSeller(await UserApiService.getUserProfile(userId))
    } catch (error: any) {
      console.error(error)
    }
  }

  return (
    <div className="flex flex-row gap-4 justify-start items-center">
      { seller ? <Avatar user={seller as IUser} size={105} /> : null}
      <div>
        <h3 className="t text-lg font-semibold pb-3">{seller?.firstName + " " + seller?.lastName }</h3>
        <span className="flex gap-2 text-xs text-bwv-label font-medium justify-between items-center pb-2">
          <Image
            src="/icons/bwv-location-icon.svg"
            width={10}
            height={15}
            alt="Close modal icon"
          />
          { seller?.location }
        </span>

        {
          seller?.phoneNumber ? (
            <span className="flex gap-2 text-xs text-bwv-label font-medium justify-between items-center">
              <Image
                src="/icons/bwv-phone-grey-icon.svg"
                width={14}
                height={14}
                alt="Close modal icon"
              />
              { seller?.phoneNumber }
            </span>
          ) : null
        }
      </div>
    </div>
  )
};

export default SellerInfo;