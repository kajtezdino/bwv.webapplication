import CoverPhoto from './CoverPhoto';
import Map from './Map';
import ContactForm from './ContactForm';
import SellerInfo from './SellerInfo';

export {
  CoverPhoto,
  Map,
  ContactForm,
  SellerInfo
}