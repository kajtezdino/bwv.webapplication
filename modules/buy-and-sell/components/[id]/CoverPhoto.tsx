const CoverPhoto: React.FC<{ src: string }> = ({ src }) => {
  return (
    <div className="w-full bg-slate-200 h-[300px]">
      <img
        src={src}
        className="w-full h-full object-cover"
      />
    </div>
  )
};

export default CoverPhoto;