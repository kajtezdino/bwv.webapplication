import { Loader } from '@googlemaps/js-api-loader';
import { useEffect, useRef } from "react";

const Map: React.FC<{ ad: any }> = ({ ad }) => {
  const googlemap = useRef(null);

  useEffect(() => {
    if (!ad?.latitude) return

    const loader = new Loader({
      apiKey: "AIzaSyBSvYmKPMI5qLRJxahnaTBCiAs6RUhzOm8",
      version: 'weekly',
    });

    loader.load().then(() => {
      const google = window.google;
      const map = new google.maps.Map(googlemap.current as any, {
        center: { lat: ad?.latitude, lng: ad?.longitude },
        zoom: 8,
        disableDefaultUI: true
      });
      new google.maps.Marker({
        position: new google.maps.LatLng(ad?.latitude, ad?.longitude),
        map: map
      });
    });
  });

  return (
    <>
      {
        ad?.latitude ? <div className="w-1/2 h-full rounded-2xl overflow-hidden">
          <div className="w-full h-full" id="map" ref={googlemap} />
        </div> : null
      }
    </>
  )
};

export default Map;