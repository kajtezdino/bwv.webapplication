import { IBuyAndSellListing } from "../../../common/models";
import { Listing } from "./";

const Listings: React.FC<{ list: IBuyAndSellListing[], loading: boolean }> = ({
  list,
  loading = false
}) => {  
  return (
    <div className="grid gap-4 grid-cols-4 justify-start">
      {
        loading ? (
          [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15].map(i => 
            <div key={i} className="relative flex flex-col rounded-2xl bg-gray-100 overflow-hidden h-[295px]">
               <div className="flex justify-between items-start w-full h-[100px] bg-gray-200 p-4 relative"></div>
            </div>
          )
        ) : (
          list.map((ad: IBuyAndSellListing) => <Listing key={ad.adID} listing={ad}/>)
        )
      }
    </div>
  )
}

export default Listings;