import { useEffect, useState } from "react";
import Image from "next/image";
import { DropdownMenu } from "../../../common/components";
import { AdListingsSortOptions } from "../../../common/enums";
import { useRecoilValue, useSetRecoilState } from "recoil";
import adListingsFiltersState from "../../../store/atoms/adListingsFiltersAtom";

const ListingsSortDropdown: React.FC = () => {
  const filters: any = useRecoilValue(adListingsFiltersState);
  const setFilter = useSetRecoilState(adListingsFiltersState);
  const [selectedSort, setSelectedSort] = useState<string>("Newest to Oldest");

  const applySort = (value: {name: string, value: number}) => {
    setSelectedSort(value.name)
    setFilter({
      ...filters,
      sortOrder: value.value
    })
  }

  const menuBtn = () => {
    return (
      <div className="flex items-center gap-2">
        <Image
          src="/icons/bwv-arrow-green-icon.svg"
          width={16}
          height={8}
          alt="Arrow green"
        />
        <span className="text-sm text-bwv-accent font-semibold">Sort by: {selectedSort}</span>
      </div>
    )
  }

  const menuOptions = [
    {name: "Newest to Oldest", value: AdListingsSortOptions.NEWEST_TO_OLDEST},
    {name: "Oldest to Newest", value: AdListingsSortOptions.OLDEST_TO_NEWEST},
    {name: "Price Low to High", value: AdListingsSortOptions.PRICE_LOW_TO_HIGH},
    {name: "Price High to Low", value: AdListingsSortOptions.PRICE_HIGH_TO_LOW}
  ]

  const menuItems = menuOptions.map(option => {
    return {
      name: option.name,
      onClick: () => {
        applySort(option)
      }
    }
  })

  return (
    <DropdownMenu menuBtn={menuBtn} menuItems={menuItems} width={"auto"} position="left" />
  )
};

export default ListingsSortDropdown;