import { ImageBrowse, ImagePreview } from "../../../common/components";
import { ICoverSelection } from "../../../common/models";

const CoverSelection: React.FC<ICoverSelection> = ({ coverFile, onChange }) => {
  return (
    <div className="rounded-2xl border-2 h-80 relative ">
      {coverFile ? (
        <ImagePreview
          file={coverFile}
          onImageRemove={() => onChange(null)}
          previewOnly={false}
        />
      ) : (
        <div className="bg-[url('/images/add-new-listing-placeholder.png')] bg-contain w-full h-full bg-center rounded-2xl"></div>
      )}
      <ImageBrowse
        disabled={false}
        files={coverFile ? [coverFile as File] : []}
        onChange={(e: any) => onChange(e[0])}
        label="Edit cover photo"
        maxLength={1}
        width={20}
        height={20}
        classes="border px-3 py-[6px] rounded-xl absolute bottom-5 right-6 bg-white"
        icon="/icons/bwv-image-green-icon.svg"
      />
    </div>
  )
}

export default CoverSelection;