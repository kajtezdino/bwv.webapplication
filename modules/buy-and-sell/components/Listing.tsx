import { Tag } from "../../../common/components";
import { IBuyAndSellListing } from "../../../common/models";
import Image from "next/image";
import CurrencyFormat from 'react-currency-format';
import { useRouter } from 'next/router';
import ShareMenu from "./ShareMenu";

const Listing: React.FC<{ listing: IBuyAndSellListing }> = ({
  listing
}) => {
  const router = useRouter();

  return (
    <div
      className="relative flex flex-col rounded-2xl bg-white overflow-hidden cursor-pointer hover:opacity-90"
      onClick={() => router.push(`/buy-and-sell/${listing.adID}`)}
    >
      <div className="flex justify-between items-start w-full h-[100px] bg-slate-400 p-2 relative">
        <div className="absolute top-0 left-0 -z-0 w-full h-full">
          <div className="absolute top-0 left-0 z-1 w-full h-full bg-black/20" />
          <img
            src={listing.coverImageUrl}
            className="object-cover w-full h-full"
          />
        </div>
        
        <div className="icon">
          <Image
            src="/icons/bwv-favourite-white-outline-icon.svg"
            width={16}
            height={16}
            alt="Favourite Icon"
          />
        </div>
        
        <ShareMenu adId={listing.adID} adTitle={listing.title} />
      </div>

      <div className="flex flex-col gap-2 p-4 pb-14">
        <div className="flex justify-between items-center">
          <Tag label={listing.subcategory} />
          <Tag label={listing.adGroup === "Buy" ? "Looking for" : "For sale"} color={listing.adGroup === "Buy" ? "bwv-accent" : "bwv-orange"} type="solid" />
        </div>

        <h2 className="text-base font-semibold text-bwx-dark">
          { listing.title }
        </h2>

        <p className="text-sm font-light text-bwx-dark">
          { listing.description }
        </p>

        <div className="absolute bottom-4 left-4 right-4 flex justify-between items-center">
          <div className="flex gap-1 items-center">
            <Image
              src="/icons/bwv-location-icon.svg"
              width={10}
              height={14}
              alt="Location Icon"
            />
            <span className="text-[10px] text-[#929292] font-medium">{ listing.location }</span>
          </div>

          <h5 className="text-base font-medium">
            {
              listing?.price && <CurrencyFormat value={listing?.price} displayType={'text'} thousandSeparator={true} prefix={'$'} />
            }
          </h5>
        </div>
      </div>
    </div>
  )
};

export default Listing;