import Image from "next/image";
import { useRef } from "react";
import toast from "react-hot-toast";
import {
  FacebookShareButton,
  EmailShareButton,
  TwitterShareButton
} from 'react-share';
import { CustomToast, DropdownMenu } from "../../../common/components";
import { ToastTypes } from "../../../common/enums";

const ShareMenu: React.FC<{ adId: string, adTitle: string }> = ({ adId, adTitle }) => {
  const fbRef: any = useRef();
  const mailRef: any = useRef();
  const twitterRef: any = useRef();
  const url = `${window.location.origin}/buy-and-sell/${adId}`;
  
  const menuBtn = () => {
    return (
      <>
        <div className="icon">
          <Image
            src="/icons/bwv-share-white-icon.svg"
            width={17}
            height={15}
            alt="Share Icon"
          />
        </div>
        <div className="hidden">
          <FacebookShareButton
            onClick={(e: any) => e.stopPropagation()}
            ref={fbRef}
            url={url}
            quote={`BWV Listing: ${adTitle}`}
            hashtag="#BusinessWideView"
          >
            <></>
          </FacebookShareButton>

          <EmailShareButton
            onClick={(e: any) => e.stopPropagation()}
            ref={mailRef}
            url={url}
            subject={`BWV Listing: ${adTitle}`}
            body="Check this listing on BusinessWideView web app. Join for free."
            separator={"\n\n"}
          >
            <></>
          </EmailShareButton>

          <TwitterShareButton
            onClick={(e: any) => e.stopPropagation()}
            ref={twitterRef}
            url={url}
            title={`BWV Listing: ${adTitle}`}
            via="BusinessWideV"
            hashtags={['agriculture', 'ag', 'agapp', 'farmers', 'ranchers', 'farming', 'consumers', 'connection', 'innovation', 'business', 'canadian', 'canada', 'beef', 'cows', 'cowboy', 'grain', 'crop', 'app', 'webapp']}
          >
            <></>
          </TwitterShareButton>
        </div>
      </>
    )
  }

  const menuItems = [
    {
      name: "Email",
      onClick: () => {
        mailRef.current.click()
      },
      icon: {
        src: "/icons/bwv-mail-icon.svg",
        width: 19,
        height: 14,
        alt: "Copy link icon"
      }
    },
    {
      name: "Facebook",
      onClick: () => {
        fbRef.current.click()
      },
      icon: {
        src: "/icons/bwv-facebook-icon.svg",
        width: 12,
        height: 20,
        alt: "Copy link icon"
      }
    },
    {
      name: "Twitter",
      onClick: () => {
        twitterRef.current.click()
      },
      icon: {
        src: "/icons/bwv-twitter-icon.svg",
        width: 20,
        height: 20,
        alt: "Copy link icon"
      }
    },
    {
      name: "Copy link",
      onClick: () => {
        navigator.clipboard.writeText(url);
        toast((t) => (
          <CustomToast
            type={ToastTypes.SUCCESS}
            title="Copied"
            message="Link copied to the clipboard."
            t={t}
          />
        ))
      },
      icon: {
        src: "/icons/bwv-copy-icon.svg",
        width: 21,
        height: 20,
        alt: "Copy link icon"
      }
    }
  ]

  return (
    <DropdownMenu menuBtn={menuBtn} menuItems={menuItems} width={"w-44"} />
  )
}

export default ShareMenu;