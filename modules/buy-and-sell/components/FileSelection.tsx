import { ImageBrowse, ImagePreview } from "../../../common/components";
import { IFilesSelection } from "../../../common/models";

const FilesSelection: React.FC<IFilesSelection> = ({ files, onChange }) => {
  return (
    <div className="h-[200px]">
      <div className="flex flex-row gap-4">
        {
          files?.map((file: File, index: number) => (
            <div className='w-[128px] h-[128px] cursor-pointer hover:opacity-90' key={index}>
              <ImagePreview file={file} onImageRemove={() => onChange(files.filter((f: File) => f.name !== file.name) ?? null)} previewOnly={false} />
            </div>
          ))
        }
        <ImageBrowse
          disabled={false}
          files={files}
          onChange={onChange}
          maxLength={5}
          classes='w-[128px] h-[128px] border-2 border-bwv-accent rounded-2xl flex justify-center'
          label=""
          width={40}
          height={40}
          icon="/icons/bwv-add-icon.png"
        />
      </div>
    </div>
  )
}

export default FilesSelection;