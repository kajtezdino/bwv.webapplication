import { Input } from "../../../../common/components";
import debounce from 'lodash/debounce';
import { useRecoilValue, useSetRecoilState } from "recoil";
import adListingsFiltersState from "../../../../store/atoms/adListingsFiltersAtom";

const Search: React.FC = () => {
  const filters: any = useRecoilValue(adListingsFiltersState);
  const setFilter = useSetRecoilState(adListingsFiltersState);
  
  const search = (e: string) => {
    if (e.length > 1) {
      setFilter({...filters, searchString: e})
    } else {
      setFilter({...filters, searchString: null})
    }
  } 

  return (
    <>
      <Input
        iconUrl="/icons/bwv-search-icon.svg"
        width={20}
        height={20}
        name="search"
        type="text"
        placeholder="Search products..."
        size="small"
        classes="content-center !mb-0"
        onBlur={() => null}
        onChange={debounce(search, 750)}
        formState={{ errors: { 'search-box': '' } } as any}
        hideErrorEl={true}
      />
    </>
  )
};

export default Search;