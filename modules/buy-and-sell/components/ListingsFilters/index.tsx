import CategoryFilter from "./CategoryFilter";
import Search from "./Search";

const ListingsFilters: React.FC = () => {
  return (
    <div className="py-8 px-6 bg-white rounded-xl">
      <CategoryFilter />
      <Search />
    </div>
  )
};

export default ListingsFilters;