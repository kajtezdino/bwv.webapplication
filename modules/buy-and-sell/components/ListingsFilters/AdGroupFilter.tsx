import { useEffect, useState } from "react";
import { useRecoilValue, useSetRecoilState } from "recoil";
import Image from "next/image";
import adListingsFiltersState from "../../../../store/atoms/adListingsFiltersAtom";

const AdGroupFilter: React.FC = () => {
  const filters: any = useRecoilValue(adListingsFiltersState);
  const setFilter = useSetRecoilState(adListingsFiltersState);
  const [buy, setBuy] = useState<boolean>(true)
  const [sell, setSell] = useState<boolean>(true)

  const handleSellClick = () => {
    if (!buy && sell) return
    setSell(!sell)
  }

  const handleBuyClick = () => {
    if (!sell && buy) return
    setBuy(!buy)
  }

  useEffect(() => {
    setFilter({
      ...filters,
      buy: buy,
      sell: sell
    })
  }, [sell, buy])
  
  return (
    <div className="flex gap-10">
      <span
        className={`flex gap-3 items-center text-sm font-semibold cursor-pointer ${sell ? 'text-bwv-accent' : 'text-bwv-label'} select-none`}
        onClick={handleSellClick}
      >
        <Image
          src={sell ? "/icons/bwv-sell-green-icon.svg" : "/icons/bwv-sell-gray-icon.svg"}
          width={21}
          height={20}
          alt="Sell icon"
        />
        For sale
      </span>
      <span
        className={`flex gap-3 items-center text-sm font-semibold cursor-pointer ${buy ? 'text-bwv-accent' : 'text-bwv-label'} select-none`}
        onClick={handleBuyClick}
      >
        <Image
          src={buy ? "/icons/bwv-buy-green-icon.svg" : "/icons/bwv-buy-gray-icon.svg"}
          width={20}
          height={20}
          alt="Buy icon"
        />
        Looking for
      </span>
    </div>
  )
};

export default AdGroupFilter;