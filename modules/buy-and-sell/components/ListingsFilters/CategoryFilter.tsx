import { useEffect, useState } from "react";
import toast from "react-hot-toast";
import { useRecoilValue, useSetRecoilState } from "recoil";
import { CustomToast } from "../../../../common/components";
import { ToastTypes } from "../../../../common/enums";
import { ICategoryAndSubcategory, ISubcategory } from "../../../../common/models";
import adListingsFiltersState from "../../../../store/atoms/adListingsFiltersAtom";
import BuyAndSellApiService from "../../api.service";

const CategoryFilter: React.FC = () => {
  const [categories, setCategories] = useState<ICategoryAndSubcategory[]>();
  const [subcategories, setSubcategories] = useState<ISubcategory[]>();
  const [allSubcategories, setAllSubcategories] = useState<ISubcategory[]>();
  const [selectedCategory, setSelectedCategory] = useState<ICategoryAndSubcategory>({ categoryID: null, name: 'All' });
  const [selectedSubcategories, setSelectedSubcategories] = useState<string[]>([]);
  const filters: any = useRecoilValue(adListingsFiltersState);
  const setFilter = useSetRecoilState(adListingsFiltersState);

  useEffect(() => {
    fetchCategoriesAndSubcategories()
    fetchAllSubcategories()
  }, [])

  const handleCategoryClicked = (category: ICategoryAndSubcategory) => {
    let params = {...filters};

    if (!category.categoryID) {
      delete params.categoryId
    }

    setSelectedCategory(category)
    setSelectedSubcategories([])
    setSubcategories(category.subcategories)
    setFilter({
      ...params,
      ...(category.categoryID && {categoryId: category.categoryID}),
      subcategories: []
    })
  }

  const handleSubcategoryClicked = (subcategoryId: string) => {
    let subcategories = selectedSubcategories;
    let updatedSubcategories: string[] = []

    if (subcategories.includes(subcategoryId)) {
      updatedSubcategories = subcategories.filter((subCategory: string) => subCategory !== subcategoryId);
      setSelectedSubcategories(updatedSubcategories)
    } else {
      updatedSubcategories = [...selectedSubcategories, subcategoryId]
      setSelectedSubcategories(updatedSubcategories)
    }

    setFilter({ ...filters, subcategories: updatedSubcategories})
  }

  const clearSubcategoriesFilter = () => {
    setSelectedSubcategories([])
    setFilter({ ...filters, subcategories: []})
  }

  const fetchAllSubcategories = async () => {
    try {
      setAllSubcategories(await BuyAndSellApiService.getSubcategories())
    } catch (error: any) {
      toast((t) => (
        <CustomToast
          type={ToastTypes.ERROR}
          title="Error"
          message={error.response.data.title}
          t={t}
        />
      ))
    }
  }

  const fetchCategoriesAndSubcategories = async () => {
    try {
      setCategories([
        {
          categoryID: null,
          name: 'All'
        },
        ... await BuyAndSellApiService.getCategoriesAndSubcategories()
      ])
    } catch (error: any) {
      toast((t) => (
        <CustomToast
          type={ToastTypes.ERROR}
          title="Error"
          message={error.response.data.title}
          t={t}
        />
      ))
    }
  }
  
  return (
    <>
      <div className="flex gap-8 border-b border-[#D4D4D4]">
        {categories?.map((category, i: number) => (
          <span
            className={`
              ${category.categoryID === selectedCategory.categoryID ? 'text-bwv-accent border-b-2 border-bwv-accent' : ''}
              text-sm font-semibold cursor-pointer hover:text-bwv-accent px-4 pb-2 transition-colors select-none
            `}
            key={i}
            onClick={() => handleCategoryClicked(category)}
          >
            {category.name}
          </span>
        ))}
      </div>

      <div className="flex justify-between">
        <div className="flex gap-2 py-[10px]">
          {(selectedCategory.categoryID ? subcategories : allSubcategories)?.map((subcategory: ISubcategory, i: number) => (
            <span
              key={i}
              className={`
              text-[#252525] text-[10px] font-semibold px-[10px] py-[5px] border border-[#5c5c5c] rounded-[8px] cursor-pointer select-none transition-all
              hover:bg-bwv-light-gray
                ${selectedSubcategories.includes(subcategory.subcategoryID) ? '!bg-bwv-accent !text-white !border-bwv-accent' : ''}
              `}
              onClick={() => handleSubcategoryClicked(subcategory.subcategoryID)}
            >
              {subcategory.name}
            </span>
          ))}
        </div>

        <div className="py-[10px]">
          <span
            className={`
            text-[#252525] text-[10px] font-semibold px-[10px] py-[5px] border border-[#5c5c5c] rounded-[8px] cursor-pointer select-none transition-all
            hover:bg-bwv-light-gray
            `}
            onClick={clearSubcategoriesFilter}
          >
            Clear
          </span>
        </div>
      </div>
    </>
  )
};

export default CategoryFilter;