
import {
  IAddNewListing,
  IBuyAndSellListingsResponse,
  ICategoryAndSubcategory,
  ITypesGroupsAndConditions,
  IBuyAndSellListingsFilters,
  ISubcategory
} from "../../common/models";
import qs from "qs";
import { http } from "../../common/utils";

class BuyAndSellApiService {
  fetchListings = (filters: IBuyAndSellListingsFilters): Promise<IBuyAndSellListingsResponse> =>
    http<IBuyAndSellListingsResponse>({
      url: '/buy-sell-api/Ad/GetAds',
      method: 'get',
      queryParams: {
        ...filters as any,
      },
      paramsSerializer: (params: any) => {
        return qs.stringify(params)
      }
    })

  getListingDetails = (id: string, ...args: any): Promise<any> =>
    http<any>({
      url: `${args.length ? args[0] : ''}/buy-sell-api/Ad/AdDetails/${id}`,
      method: 'get',
    });

  getCategoriesAndSubcategories = (): Promise<ICategoryAndSubcategory[]> => {
    return http<ICategoryAndSubcategory[]>({
      url: `/buy-sell-api/Ad/GetCategoriesAndSubcategories`,
      method: 'get',
    });
  };

  createAd = (params: IAddNewListing): Promise<IAddNewListing> | null => {
    const formData = new FormData();

    for (const param in params) {
      if (Array.isArray((params as any)[param])) {
        for (const value of (params as any)[param]) {
          formData.append(param, value)
        }
      } else {
        formData.append(param, (params as { [key: string]: any })[param])
      }
    }

    return http<IAddNewListing>({
      url: `/buy-sell-api/Ad/CreateAd`,
      method: 'post',
      headers: {
        'content-type': 'multipart/form-data'
      },
      body: formData
    })
  };

  getTypesGroupsAndConditions = (): Promise<ITypesGroupsAndConditions> =>
    http<ITypesGroupsAndConditions>({
      url: `/buy-sell-api/Ad/GetTypesGroupsAndConditions`,
      method: 'get',
    })


  getSubcategories = (): Promise<ISubcategory[]> =>
    http<ISubcategory[]>({
      url: `/buy-sell-api/Subcategory/GetSubcategories`,
      method: 'get',
    })
}

export default new BuyAndSellApiService();