import { yupResolver } from '@hookform/resolvers/yup';
import Image from 'next/image'
import router from 'next/router';
import { useEffect, useState } from 'react';
import { Controller, useForm } from 'react-hook-form'
import toast from 'react-hot-toast';
import {
  AutoTextArea,
  Button,
  CustomToast,
  Input,
} from '../../../common/components'
import RadioButton from '../../../common/components/RadioButton';
import { ToastTypes } from '../../../common/enums';
import { IUnsubscribeForm, IUnsubscribeReason } from '../../../common/models';
import { NewsletterUnsubscribeShema } from '../../../common/schemas';
import NewsletterApiService from '../api.service'

const NewsletterUnsubscribeContent: React.FC = () => {
  const [isActive, setActive] = useState(false);
  const [unsubscribeReasons, setUnsubscribeReasons] = useState<IUnsubscribeReason[]>([])
  const showTextAreaID = "415c4aae-12d4-4861-af40-ec38b64d9c3f"
  const { handleSubmit, control, formState: { errors, isValid } } = useForm<IUnsubscribeForm>({
    mode: 'onChange',
    reValidateMode: 'onChange',
    resolver: yupResolver(NewsletterUnsubscribeShema)
  });

  const getUnsubscribeReasons = async () => {
    try {
      setUnsubscribeReasons(await NewsletterApiService.fetchUnsubscribeReasons())
    } catch (error: any) {
      toast((t) => (
        <CustomToast
          type={ToastTypes.ERROR}
          title="Error"
          message={error.response.data.title}
          t={t}
        />
      ))
    }
  }

  useEffect(() => {
    getUnsubscribeReasons()
  }, [])

  const goBack = () => {
    router.back()
  }

  const toggleClass = (id: string) => {
    if (id === showTextAreaID) {
      setActive(true);
    } else {
      setActive(false)
    }
  };

  const unsubscribedReasonsExpression =
    unsubscribeReasons.map(item => (
      <Controller
        key={item.id}
        control={control}
        name="predefinedReasons"
        render={({ formState, field: { onChange } }) =>
          <RadioButton
            id={item.id}
            name="reasons"
            onChange={() => {
              onChange(item.id)
              toggleClass(item.id)
            }}
            formState={formState}
            label={item.name}
          />
        }
      />
    ));

  const onSubmit = async (data: IUnsubscribeForm) => {
    try {
      const unsubscribeData = {
        emailAddress: data.emailAddress,
        predefinedReasons: [String(data.predefinedReasons)],
        unsubscribeReason: data.unsubscribeReason
      }
      await NewsletterApiService.submitUnsubscribeForm(unsubscribeData)
      goBack()
      toast((t) => (
        <CustomToast
          type={ToastTypes.SUCCESS}
          title="Success"
          message={() => (
            <div className="flex">
              <p className="mr-1 text-base font-medium">
                You have successfully unsubscribed from the newsletter.
              </p>
            </div>
          )}
          t={t}
        />
      ))
    } catch (e: any) {
      toast((t) => (
        <CustomToast
          type={ToastTypes.ERROR}
          title="Error"
          message={`${e.response.data.title}`}
          t={t}
        />
      )
      )
    }
  }

  return (
    <div className="mt-[140px]">
      <div className="text-center">
        <Image src={"/images/newsletter-unsubscribe.png"} width={160} height={160} alt={'unsubscribe-image'} />
        <p className="pt-6 text-2xl font-semibold">Unsubscribe</p>
        <p className="pt-5 font-medium text-base text-bwv-dark-gray">
          You will be unsubscribed from our newsletter.<br />
          Please take a moment to tell us the reason why you unsubscribed:
        </p>
      </div>
      <form onSubmit={handleSubmit(onSubmit)} className="bg-white flex-grow flex justify-center flex-col gap-6 w-[876px] mt-6 mb-[84px]">
        <div className="flex justify-center">
          <Controller
            control={control}
            name="emailAddress"
            render={({ formState, field: { onBlur, onChange } }) => (
              <Input
                classes="w-[390px]"
                iconUrl="/icons/bwv-email-icon.svg"
                width={22}
                height={22}
                name="firstName"
                placeholder="Confirm your email"
                onBlur={onBlur}
                onChange={onChange}
                formState={formState}
                size="small"
              />
            )}
          />
        </div>
        <div className="flex justify-center">
          <div className="flex flex-col">
            {unsubscribedReasonsExpression}
          </div>
        </div>
        <div className='flex justify-center'>
          <Controller
            control={control}
            name="unsubscribeReason"
            render={({ formState, field: { onChange } }) => (
              <AutoTextArea
                placeholder="Write your reason here"
                onChange={onChange}
                className={!isActive ? "hidden " : "bg-bwv-white-dirty rounded-xl focus:border-[1px] focus-visible:border-bwv-accent focus:outline-none py-2 px-3 min-h-[108px] w-[390px]"}
              />
            )}
          />
        </div>
        <div className="flex justify-center gap-4">
          <div className="w-[390px] flex justify-start">
            <Button
              text="Submit"
              type="submit"
              size="small"
              style="outline"
              disabled={!isValid}
            />
          </div>
        </div>
      </form>
    </div>
  )
}

export default NewsletterUnsubscribeContent