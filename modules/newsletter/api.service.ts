import { AxiosResponse } from 'axios';
import { IActivityFeedTableParams, IUnsubscribeForm, IUnsubscribeReason } from '../../common/models';
import { http } from '../../common/utils/http';

class NewsletterApiService {

  fetchUnsubscribeReasons = (): Promise<IUnsubscribeReason[]> => {
    return http<IUnsubscribeReason[]>({
      url: "api/email/Newsletter/GetReasonsForUnsubscibe",
      method: 'get',
    });
  }

  submitUnsubscribeForm = (params: IUnsubscribeForm): Promise<AxiosResponse> => {
    return http<AxiosResponse>({
      url: "api/email/Newsletter/Unsubscribe",
      method: 'post',
      body: { ...params }
    });
  }
}

export default new NewsletterApiService();