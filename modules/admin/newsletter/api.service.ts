import { http } from "../../../common/utils/http";
import { AxiosResponse } from "axios";
import {
  CreateNewsletterRequestParams,
  IEmailTemplate,
  INewsletter,
  IActivityFeedResponse,
  IActivityFeed
} from "../../../common/models"

class NewsletterApiService {

  createNewsletter = (params: CreateNewsletterRequestParams): Promise<INewsletter> =>
    http<INewsletter>({
      url: `/email-api/Newsletter/CreateNewsletter`,
      method: 'post',
      body: { ...params }
    })

  fetchNewsletters = (): Promise<INewsletter[]> =>
    http<INewsletter[]>({
      url: `/email-api/Newsletter/GetNewsletters`,
      method: 'get'
    })


  fetchEmailTemplates = (): Promise<IEmailTemplate[]> =>
    http<IEmailTemplate[]>({
      url: `/email-api/Newsletter/GetDynamicTemplates`,
      method: 'get'
    })

  fetchNewslettersActivityFeed = (params: IActivityFeed): Promise<IActivityFeedResponse> =>
    http<IActivityFeedResponse>({
      url: `/email-api/Newsletter/EmailActivityFeedForAllNewsletters`,
      method: 'post',
      body: { ...params }
    })

  deleteNewsletter = (id: string): Promise<INewsletter> =>
    http<INewsletter>({
      url: `/email-api/Newsletter/${id}`,
      method: 'delete'
    })

  updateNewsletter = (id: string, params: CreateNewsletterRequestParams): Promise<AxiosResponse> =>
    http<AxiosResponse>({
      url: `/email-api/Newsletter/UpdateNewsletter/?newsletterID=${id}`,
      method: 'put',
      body: { ...params }
    })

  activateNewsletter = (id: string): Promise<INewsletter> =>
    http<INewsletter>({
      url: `/email-api/Newsletter/SendNewsletter/${id}`,
      method: 'post'
    })

  retryNewsletter = (id: string): Promise<AxiosResponse> =>
    http<AxiosResponse>({
      url: `/email-api/Newsletter/Retry/${id}`,
      method: 'get'
    })
}

export default new NewsletterApiService();