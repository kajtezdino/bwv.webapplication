import Image from "next/image"
import ActivityFeedTableFilters from "./ActivityFeedTableFilters"

const ActivityFeedToolbar: React.FC<{ table: any }> = ({ table, }) => {

  const filterUsers = (value: number | string) => {
    table.setFilter('mailStatus', value)
  }
  return (
    <div className='flex flex-row items-center w-full'>
      {/* Filters */}
      <div className="flex justify-end w-1/2 gap-4">
        <ActivityFeedTableFilters onChange={filterUsers} />
      </div>

      <div className="flex justify-end w-1/2 items-center gap-8">
        {/* Pagination */}
        <div className="pagination h-20 flex justify-end items-center gap-6">
          <div className="text-sm text-[#D4D4D4] leading-[22px] font-normal">
            {table.state.pageIndex + 1} of {table.pageOptions.length}
          </div>
          <div className="inline-flex gap-8">
            <button onClick={() => table.previousPage()} disabled={!table.canPreviousPage}>
              <Image
                src="/icons/bwv-arrow-light-icon.svg"
                width={12}
                height={22}
                alt="Arrow"
              />
            </button>
            <button onClick={() => table.nextPage()} disabled={!table.canNextPage}>
              <Image
                className="rotate-180"
                src="/icons/bwv-arrow-light-icon.svg"
                width={12}
                height={22}
                alt="Arrow"
              />
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ActivityFeedToolbar