import { yupResolver } from "@hookform/resolvers/yup";
import Image from "next/image";
import { Controller, useForm } from "react-hook-form";
import { Input } from "../../../../common/components";
import { ISearchNewsletters } from "../../../../common/models";
import { SearchNewsletterSchema } from "../../../../common/schemas";

const NewsletterTableToolbar: React.FC<{ table: any }> = ({ table }) => {

  const { handleSubmit, control, formState: { errors, isValid } } = useForm<ISearchNewsletters>({
    mode: 'onChange',
    reValidateMode: 'onChange',
    resolver: yupResolver(SearchNewsletterSchema)
  });

  return (
    <div className="flex justify-between mb-4 pl-6 pr-8">
      <form className="relative min-w-[278px]">
        <Controller
          control={control}
          name="searchNewsletter"
          render={({ formState, field: { onBlur, onChange } }) => (
            <Input
              name="searchNewsletter"
              placeholder="Newsletter Name"
              onBlur={onBlur}
              onChange={onChange}
              formState={formState}
            />
          )}
        />
      </form>

      <div className="pagination flex justify-end items-center gap-6">
        <div className="text-sm text-[#D4D4D4] leading-[22px] font-normal">
          {table.state.pageIndex + 1} of {table.pageOptions.length}
        </div>
        <div className="inline-flex gap-8">
          <button onClick={() => table.previousPage()} disabled={!table.canPreviousPage}>
            <Image
              src="/icons/bwv-arrow-light-icon.svg"
              width={12}
              height={22}
              alt="Arrow"
            />
          </button>
          <button onClick={() => table.nextPage()} disabled={!table.canNextPage}>
            <Image
              className="rotate-180"
              src="/icons/bwv-arrow-light-icon.svg"
              width={12}
              height={22}
              alt="Arrow"
            />
          </button>
        </div>
      </div>
    </div>
  )
}

export default NewsletterTableToolbar;