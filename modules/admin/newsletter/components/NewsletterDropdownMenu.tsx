import Image from "next/image";
import { Button, ComboboxInput, CustomToast, DropdownMenu, Input, Modal } from "../../../../common/components";
import NewsletterApiService from "../api.service";
import { useState } from "react";
import toast from "react-hot-toast";
import { ToastTypes } from "../../../../common/enums";
import { useRecoilState, useRecoilValue } from "recoil";
import { Controller, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { INewsletter, ICreateNewsletter } from "../../../../common/models";
import { CreateNewsletterSchema } from "../../../../common/schemas";
import newslettersState from "../../../../store/atoms/newslettersAtom";
import emailTemplatesState from "../../../../store/atoms/emailTemplatesAtom";
import { useRouter } from "next/router";

const NewsletterDropdownMenu: React.FC<{ data: INewsletter }> = ({ data }) => {
  const router = useRouter();
  const [openModal, setOpenModal] = useState(false);
  const [busy, setBusy] = useState(false);
  const [newsletters, setNewsletters] = useRecoilState(newslettersState);
  const templates = useRecoilValue(emailTemplatesState);

  const { handleSubmit, control, formState: { errors, isValid } } = useForm<ICreateNewsletter>({
    mode: 'onChange',
    reValidateMode: 'onSubmit',
    resolver: yupResolver(CreateNewsletterSchema)
  });

  const activateNewsletter = async () => {
    try {
      const response = await NewsletterApiService.activateNewsletter(data.newsletterID);
      setNewsletters(newsletters.map((newsletter) => {
        const activatedNewsletter = { ...newsletter }
        if (activatedNewsletter.newsletterID === data.newsletterID) {
          activatedNewsletter.status = response.status; 
        }
        return activatedNewsletter;
      }))

      toast((t) => (
        <CustomToast
          type={ToastTypes.SUCCESS}
          title="Success"
          message="You have successfully activate the newsletter!"
          t={t}
        />
      ))
    } catch (err: any) {
      toast((t) => (
        <CustomToast
          type={ToastTypes.ERROR}
          title="Error"
          message={err.response.data}
          t={t}
        />
      ))
    }
  }

  const deleteNewsletter = async () => {
    try {
      await NewsletterApiService.deleteNewsletter(data.newsletterID);
      toast((t) => (
        <CustomToast
          type={ToastTypes.SUCCESS}
          title="Success"
          message="You have successfully deleted the newsletter!"
          t={t}
        />
      ))
      setNewsletters(await NewsletterApiService.fetchNewsletters())
    } catch (err: any) {
      toast((t) => (
        <CustomToast
          type={ToastTypes.ERROR}
          title="Error"
          message={err.response.data}
          t={t}
        />
      ))
    }
  }

  const updateNewsletter = async (props: ICreateNewsletter) => {
    setBusy(true);
    try {
      await NewsletterApiService.updateNewsletter(data.newsletterID, props);
      setNewsletters(newsletters.map((newsletter) => {
        const updatedNewsletter = { ...newsletter }
        if (updatedNewsletter.newsletterID === data.newsletterID) {
          updatedNewsletter.name = data.name.substring(0, 20);
          updatedNewsletter.subject = data.subject.substring(0, 20);
          updatedNewsletter.emailTemplateID = data.emailTemplateID;
        }
        return updatedNewsletter;
      }))
      setNewsletters(await NewsletterApiService.fetchNewsletters())
      toast((t) => (
        <CustomToast
          type={ToastTypes.SUCCESS}
          title="Success"
          message="You have successfully edited the newsletter!"
          t={t}
        />
      ))
      setOpenModal(false);
    } catch (err: any) {
      toast((t) => (
        <CustomToast
          type={ToastTypes.ERROR}
          title="Error"
          message={err.response.data.title}
          t={t}
        />
      )
      )
    } finally {
      setBusy(false)
    }
  }

  const retryNewsletter = async () => {
    try {
      await NewsletterApiService.retryNewsletter(data.newsletterID);
      toast((t) => (
        <CustomToast
          type={ToastTypes.SUCCESS}
          title="Success"
          message="You have successfully retried the newsletter!"
          t={t}
        />
      ))
    } catch (err: any) {
      toast((t) => (
        <CustomToast
          type={ToastTypes.ERROR}
          title="Error"
          message={err.response.data}
          t={t}
        />
      ))
    }
  }

  const redirectToActivityFeed = () => {
    router.push(`/admin/newsletter/activity-feed?newsletterId=${data.newsletterID}`);
  }

  const menuBtn = () => {
    return (
      <div className="w-8 h-7 flex justify-end">
        <Image
          src="/icons/bwv-options-icon.svg"
          width={4}
          height={17}
          alt="Options Icon"
        />
      </div>
    )
  }

  const menuItems = [
    {
      name: "Activate Newsletter",
      onClick: () => activateNewsletter(),
      disabled: data.status === "Active"
    },
    {
      name: "Delete Newsletter",
      onClick: () => deleteNewsletter(),
      disabled: data.status === "Active"
    },
    {
      name: "Edit Newsletter",
      onClick: () => setOpenModal(true),
      disabled: data.status === "Active"
    },
    {
      name: "Activity Feed",
      onClick: () => redirectToActivityFeed(),
      disabled: data.status !== "Active"
    }
  ]

  const modalContent = () => {
    const options = templates && templates.map(template => template.name);

    return (
      <form onSubmit={handleSubmit(updateNewsletter)}>
        <Controller
          control={control}
          name="name"
          render={({ formState, field: { onBlur, onChange } }) => (
            <Input
              name="name"
              placeholder="Newsletter Name"
              onBlur={onBlur}
              onChange={onChange}
              formState={formState}
              size="small"
              defaultValue={data.name}
              maxLength={20}
            />
          )}
        />
        <Controller
          control={control}
          name="subject"
          render={({ formState, field: { onBlur, onChange } }) => (
            <Input
              name="subject"
              placeholder="Subject"
              onBlur={onBlur}
              onChange={onChange}
              formState={formState}
              size="small"
              defaultValue={data.subject}
              maxLength={20}
            />
          )}
        />
        <Controller
          control={control}
          name="emailTemplateID"
          render={({ formState, field: { onBlur, onChange } }) => (
            <div className="w-1/2">
              <ComboboxInput
                src="/icons/bwv-newsletter-icon.svg"
                alt="Newsletter Icon"
                width={20}
                height={20}
                name="emailTemplate"
                placeholder="Email Template"
                onBlur={onBlur}
                onChange={onChange}
                formState={formState}
                label="Email Templates"
                options={options}
                defaultValue={data.emailTemplateID}
              />
            </div>
          )}
        />
      </form>
    )
  }

  return (
    <div>
      <DropdownMenu menuBtn={menuBtn} menuItems={menuItems} width={"w-44"} />
      <Modal
        title={"Edit Newsletter"}
        isOpen={openModal}
        content={modalContent}
        onClose={() => setOpenModal(false)}
        proceedBtn={{
          name: "Save",
          onClick: handleSubmit(updateNewsletter)
        }}
        proceedBtnDisabled={!isValid || busy}
        proceedBtnLoading={busy}
        proceedBtnLoadingText="Saving..."
        cancelBtn={{
          name: "Cancel",
          onClick: () => setOpenModal(false)
        }}
      />
    </div>
  )
}

export default NewsletterDropdownMenu;