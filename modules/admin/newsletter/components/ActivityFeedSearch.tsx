import {
  Button,
  ComboboxInput,
  CustomToast,
  RetryRefreshButton,
  Datepicker
} from '../../../../common/components';
import { Controller, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { NewsletterActivityFeedShema } from '../../../../common/schemas';
import { IActivityFeed, INewsletter, ITableSearch } from '../../../../common/models';
import { useEffect, useState } from 'react';
import NewsletterApiService from '../api.service'
import toast from 'react-hot-toast';
import { ToastTypes } from '../../../../common/enums';
import { useRouter } from 'next/router';

const ActivityFeedSearchbar: React.FC<{ showRetry: boolean, onSubmit: (data: IActivityFeed) => void, onRefresh: () => void }> = ({ showRetry, onSubmit, onRefresh }) => {
  const router = useRouter();
  const { newsletterId } = router.query;
  const [selectedDefaultObject, setSelectedDefaultObject] = useState<INewsletter>()
  const [newsletterNames, setNewsletterNames] = useState<INewsletter[]>([])
  const { handleSubmit, control, reset, formState: { errors, isValid }, setValue, getValues } = useForm<ITableSearch>({
    mode: 'onChange',
    reValidateMode: 'onChange',
    resolver: yupResolver(NewsletterActivityFeedShema)
  });

  const handleOnSubmit = (data: ITableSearch) => {
    onSubmit({
      newsletterId: data.searchQuery ?? null,
      from: data.dateFrom?.toISOString() ?? null,
      to: data.dateTo?.toISOString() ?? null
    });
  }

  const handleRetry = async () => {
    const newsletterName = getValues('searchQuery');
    try {
      await NewsletterApiService.retryNewsletter(newsletterName);
      toast((t) => (
        <CustomToast
          type={ToastTypes.SUCCESS}
          title="Success"
          message="You have successfully retried the newsletter!!"
          t={t}
        />
      ))
    } catch (err: any) {
      toast((t) => (
        <CustomToast
          type={ToastTypes.ERROR}
          title="Error"
          message="{err.response.data}"
          t={t}
        />
      ))
    }
  }

  const handleClear = () => {
    reset()
    onRefresh()
  }

  useEffect(() => {
    const setDefaultObject = (newsletterID: string | string[]) => {
      const result = newsletterNames.filter(option => option.newsletterID === newsletterID)[0]
      setSelectedDefaultObject(result)
    }

    if (newsletterId) {
      setDefaultObject(newsletterId)
      const { name } = newsletterNames.find(({ newsletterID }) => newsletterID === newsletterId) ?? {};

      if (name) {
        setValue('searchQuery', name);
      }
    }
  }, [newsletterNames, newsletterId, setValue]);

  useEffect(() => {
    const fetch = async () => {
      try {
        const newsletterNames = await NewsletterApiService.fetchNewsletters();
        setNewsletterNames(newsletterNames);
      } catch (error: any) {
        toast((t) => (
          <CustomToast
            type={ToastTypes.ERROR}
            title="Error"
            message={error.response.data.title}
            t={t}
          />
        ))
      }
    }
    fetch();
  }, []);

  return (
    <div className="flex flex-col w-full items-center border-b border-[#f1f1f1] px-8 pb-2">

      {/*Search */}
      <div className='w-full'>
        <form onSubmit={handleSubmit(handleOnSubmit)}>
          <div className="flex justify-between items-center gap-5">
            <div className="flex align-middle gap-5">
              <div className="w-[250px] align-midle">
                <label className="flex text-bwv-gray">Newsletter name</label>
                <Controller
                  control={control}
                  name="searchQuery"
                  render={({ formState, field }) => (
                    <ComboboxInput
                      defaultValue={selectedDefaultObject?.newsletterID || ''}
                      placeholder='Chosse newsletter'
                      options={newsletterNames}
                      valueKey='newsletterID'
                      labelKey='name'
                      formState={formState}
                      {...field}
                    />
                  )}
                />
              </div>
              <div>
                <div className='flex gap-5'>
                  <div className='flex flex-col'>
                    <label className="flex text-bwv-gray">Pick date from</label>
                    <Controller
                      control={control}
                      name="dateFrom"
                      render={({ formState, field: { value, ...rest } }) => (
                        <Datepicker
                          selected={value ? value : null}
                          startDate={value}
                          isClearable={false}
                          className="border rounded-2xl flex flex-row input-small"
                          {...rest}
                        />
                      )}
                    />
                  </div>
                  <div>
                    <label className="flex text-bwv-gray">Pick date to</label>
                    <Controller
                      control={control}
                      name="dateTo"
                      render={({ formState, field: { value, ...rest } }) => (
                        <Datepicker
                          selected={value ? value : null}
                          startDate={value}
                          isClearable={false}
                          className="border rounded-2xl flex flex-row input-small"
                          {...rest}
                        />
                      )}
                    />
                  </div>
                </div>
              </div>
              <div className="flex items-center pt-5">
                <Button type="submit" text={"Search"} size='small' style="outline" color='primary' disabled={!isValid} />
              </div>
              <div className="flex items-center pt-5" >
                <Button onClick={handleClear}
                  type="reset" text={"Clear"} size='small' style="outline" color='error' />
              </div>
            </div>
            <div className='flex gap-7 items-center pt-5'>
              {showRetry && (
                <div>
                  <RetryRefreshButton onClick={(handleRetry)} type={'retry'} />
                </div>
              )}
              <div>
                <RetryRefreshButton onClick={onRefresh} type={'refresh'} />
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  )
}

export default ActivityFeedSearchbar;