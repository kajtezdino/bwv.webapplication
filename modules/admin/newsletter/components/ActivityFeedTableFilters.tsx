import { useMemo, useState } from 'react';
import { StatusIndicator } from '../../../../common/components';

const UserManagementStatusFilters: React.FC<{ onChange: (value: string) => void }> = ({
  onChange
}) => {
  const [activeFilter, setActiveFilter] = useState<string>()
  const filters = useMemo(() => {
    return [
      {
        label: "All Newsletter",
        value: "",
        color:""
      },
      {
        label: "Sent",
        color: "bg-bwv-accent",
        value: "Sent"
      },
      {
        label: "Failed",
        color: "bg-bwv-error",
        value: "Failed"
      }
    ]
  }, [])

  const onFilterChange = (value: string) => {
    onChange(value)
    setActiveFilter(value)
  }

  return (
    <div className="flex flex-row gap-5">
      {filters.map(filter => (
        <div key={filter.value} onClick={() => onFilterChange(filter.value)}>
          {
            !filter.value
              ? <div className={`
              status-indicator hover:bg-bwv-accent-dark hover:!text-white cursor-pointer
              ${activeFilter === filter.value ? "bg-bwv-accent-dark !text-white" : ""}
            `}>
                <span className="inline-block text-base leading-5 font-normal">{filter.label}</span>
              </div>
              : <StatusIndicator
                label={filter.label}
                color={filter.color}
                classes={`hover:bg-bwv-accent-dark hover:!text-white cursor-pointer transition-all ${activeFilter === filter.value ? "bg-bwv-accent-dark !text-white" : ""}`} />
          }
        </div>
      ))}
    </div>
  )
}

export default UserManagementStatusFilters