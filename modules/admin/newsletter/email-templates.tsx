import { NewsletterSubNavigation, CustomToast, Table, StatusIndicator } from "../../../common/components";
import NewsletterApiService from "./api.service";
import { IEmailTemplate, IStatus } from "../../../common/models";
import { useRecoilState } from "recoil";
import emailTemplatesState from "../../../store/atoms/emailTemplatesAtom";
import toast from "react-hot-toast";
import { ToastTypes } from "../../../common/enums";
import { useEffect, useMemo } from "react"
import Image from "next/image";
import Moment from "react-moment";

const EmailTemplatesContent: React.FC = () => {
  const [templates, setTemplates] = useRecoilState(emailTemplatesState);

  const statusToDataMap: { [key: string]: IStatus } = {
    "Active": {
      label: "Active",
      color: "bg-bwv-accent"
    },
    "Created": {
      label: "Created",
      color: "bg-bwv-orange"
    }
  }

  const fetchEmailTemplates = async () => {
    try {
      const response = await NewsletterApiService.fetchEmailTemplates();
      const templatesArr: IEmailTemplate[] = response.map((template) => {
        return {
          thumbnailUrl: template.thumbnailUrl,
          name: template.name,
          status: template.status,
          creationDate: template.creationDate,
          templateId: template.templateId
        }
      })
      setTemplates(templatesArr);
    } catch (error: any) {
      toast((t) => (
        <CustomToast
          type={ToastTypes.ERROR}
          title="Error"
          message={error.response.data.title}
          t={t}
        />
      ))
    }
  }

  useEffect(() => {
    fetchEmailTemplates()
  }, [])

  const columns = useMemo(
    () => [
      {
        Header: "Email Template",
        accessor: "name",
        cellClass: "!text-bwv-dark font-medium",
        Cell: ({ row }: { row: { original: IEmailTemplate } }) => (
          <div className="flex items-center">
            <div className="rounded-[20px] w-[94px] h-[94px] overflow-hidden relative mr-3 border-[1px] border-[#c4c4c4]">
              <Image src={`https:${row.original.thumbnailUrl}`} layout="fill" alt="Template Thumbnail" />
            </div>
            {row.original.name}
          </div>
        ),
      },
      {
        Header: "Status",
        accessor: "status",
        Cell: ({ row }: { row: { original: IEmailTemplate } }) => (
          <StatusIndicator
            label={statusToDataMap[row.original.status].label}
            color={statusToDataMap[row.original.status].color} />
        )
      },
      {
        Header: "Created",
        accessor: "creationDate",
        cellClass: "!text-bwv-dark font-medium",
        Cell: ({ row }: { row: { original: IEmailTemplate } }) => (
          <Moment format="DD/MM/YY">{row.original.creationDate}</Moment>
        )
      },
      {
        Header: "Template ID",
        accessor: "templateId",
        cellClass: "!text-bwv-dark font-medium"
      }
    ],
    []
  )

  return (
    <div className="flex w-full my-11 mx-8">
      <div className="w-full bg-white rounded-3xl">
        <div className="flex justify-between items-center pt-8 pb-5 px-14">
          <h2 className="text-[28px] text-bwv-dark font-semibold">Email Templates</h2>
        </div>
        <NewsletterSubNavigation />
        <div className="flex flex-col px-6">
          <Table columns={columns} data={templates} checkbox={false} />
        </div>
      </div>
    </div>
  )
}

export default EmailTemplatesContent;