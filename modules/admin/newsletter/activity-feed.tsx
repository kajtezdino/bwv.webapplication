import { useEffect, useMemo, useState } from "react";
import {
  IActivityFeedResponse,
  IActivityFeedTableParams,
  IStatus,
  IActivityFeed
} from "../../../common/models";
import {
  CustomToast,
  NewsletterSubNavigation,
  StatusIndicator, 
  Table
} from "../../../common/components";
import Moment from "react-moment";
import NewsletterApiService from './api.service'
import { ToastTypes } from "../../../common/enums";
import toast from "react-hot-toast";
import { useRouter } from "next/router";
import ActivityFeedToolbar from "./components/ActivityFeedToolbar";
import ActivityFeedSearchbar from "./components/ActivityFeedSearch";

const statusToDataMap: { [key: string]: IStatus } = {
  "Sent": {
    label: "Sent",
    color: "bg-bwv-accent"
  },
  "Failed": {
    label: "Failed",
    color: "bg-bwv-error"
  }
}

const ActivityFeedContent: React.FC = () => {
  const router = useRouter();
  const [newsletterData, setNewsletterData] = useState<IActivityFeedResponse | null>(null);

  const handleSubmit = async (data: IActivityFeed) => {
    try {
      const newsletterData = await NewsletterApiService.fetchNewslettersActivityFeed(data);
      setNewsletterData(newsletterData);
    } catch (error: any) {
      toast((t) => (
        <CustomToast
          type={ToastTypes.ERROR}
          title="Error"
          message={error.response}
          t={t}
        />
      ))
    }
  }

  useEffect(() => {
    const fetch = async () => {
      try {
        const { newsletterId } = router.query;
        const newsletterData = await NewsletterApiService.fetchNewslettersActivityFeed({ newsletterId: (newsletterId as string) || null, from: null, to: null });
        setNewsletterData(newsletterData);
      } catch (error: any) {
        toast((t) => (
          <CustomToast
            type={ToastTypes.ERROR}
            title="Error"
            message={error.response}
            t={t}
          />
        ))
      }
    }
    fetch();
  }, [router.query]);

  const { activityFeeds, totalNumberOfRecipients, totalNumberOfSubscribedUsers } = newsletterData ?? { activityFeeds: [] };

  const showRetry = totalNumberOfRecipients !== totalNumberOfSubscribedUsers;

  const handleRefresh = async () => {
    const newsletterData = await NewsletterApiService.fetchNewslettersActivityFeed({ newsletterId: null, from: null, to: null });
    setNewsletterData(newsletterData);
    router.replace('/admin/newsletter/activity-feed', undefined, { shallow: true });
  }

  const columns = useMemo(
    () => [
      {
        id: "toolbar",
        className: "w-full flex flex-row",
        Header: (table: any) => {
          return (
            <ActivityFeedToolbar table={table} />
          )
        },
        columns: [
          {
            Header: "TRANSACTION ID:",
            accessor: "newsletterID",
            className: "min-w-[160px]",
            cellClass: "font-semibold",
            Cell: ({ row }: { row: { original: IActivityFeedTableParams } }) => (row.original.newsletterNotificationID)
          },
          {
            Header: "RECIPIENT",
            accessor: "regularMarketTime",
            cellClass: "font-medium",
            Cell: ({ row }: { row: { original: IActivityFeedTableParams } }) => (row.original.recipient)
          },
          {
            Header: "NEWSLETTER NAME",
            accessor: "newsletterName",
            cellClass: "font-medium",
            Cell: ({ row }: { row: { original: IActivityFeedTableParams } }) => (row.original.newsletterName)
          },
          {
            Header: "STATUS",
            accessor: "mailStatus",
            cellClass: "font-medium",
            Cell: ({ row }: { row: { original: IActivityFeedTableParams } }) => (
              <StatusIndicator color={statusToDataMap[row.original.mailStatus].color} label={statusToDataMap[row.original.mailStatus].label} />
            )
          },
          {
            Header: "CREATED",
            accessor: "creationDate",
            cellClass: "font-medium",
            Cell: ({ row }: { row: { original: IActivityFeedTableParams } }) => (
              <Moment format="MMM DD YYYY">{row.original.notificationDate}</Moment>
            )
          },

        ]
      }
    ],
    [showRetry]
  )

  return (
    <div className="flex w-full my-11 mx-8">
      <div className="w-full bg-white rounded-3xl">
        <div className="flex justify-between items-center pt-8 pb-5 px-14">
          <h2 className="text-[28px] text-bwv-dark font-semibold">Activity feed</h2>
        </div>
        <NewsletterSubNavigation />
        <div className="flex flex-col px-6">
          <ActivityFeedSearchbar showRetry={showRetry} onSubmit={handleSubmit} onRefresh={handleRefresh} />
        </div>
        <div className="flex flex-col px-6">
          <Table columns={columns} data={activityFeeds} checkbox={false} />
        </div>
      </div>
    </div>
  )
}

export default ActivityFeedContent