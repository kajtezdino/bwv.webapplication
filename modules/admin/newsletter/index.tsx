import {
  Button,
  Modal,
  Table,
  Input,
  ComboboxInput,
  NewsletterSubNavigation,
  CustomToast,
  StatusIndicator
} from "../../../common/components";
import { useMemo, useState, useEffect } from "react";
import { NewsletterTableToolbar, NewsletterDropdownMenu } from "./components";
import NewsletterApiService from "./api.service";
import {
  ICreateNewsletter,
  IEmailTemplate,
  INewsletter,
  IStatus
} from "../../../common/models";
import { Controller, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { CreateNewsletterSchema } from "../../../common/schemas";
import toast from "react-hot-toast";
import { ToastTypes } from "../../../common/enums";
import { useRecoilState } from "recoil";
import newslettersState from "../../../store/atoms/newslettersAtom";
import Moment from "react-moment";
import emailTemplatesState from "../../../store/atoms/emailTemplatesAtom";

const NewsletterContent: React.FC = () => {
  const [modalOpen, setModalOpen] = useState(false);
  const [newsletters, setNewsletters] = useRecoilState(newslettersState);
  const [templates, setTemplates] = useRecoilState(emailTemplatesState);
  const [busy, setBusy] = useState(false);

  const statusToDataMap: { [key: string]: IStatus } = {
    "Active": {
      label: "Active",
      color: "bg-bwv-accent"
    },
    "Created": {
      label: "Created",
      color: "bg-bwv-orange"
    }
  }

  const { handleSubmit, control, formState: { errors, isValid } } = useForm<ICreateNewsletter>({
    mode: 'onChange',
    reValidateMode: 'onSubmit',
    resolver: yupResolver(CreateNewsletterSchema)
  });

  const fetchEmailTemplates = async () => {
    try {
      const response = await NewsletterApiService.fetchEmailTemplates();
      const templatesArr: IEmailTemplate[] = response.map((template) => {
        return {
          thumbnailUrl: template.thumbnailUrl,
          name: template.name,
          status: template.status,
          creationDate: template.creationDate,
          templateId: template.templateId
        }
      })
      setTemplates(templatesArr);
    } catch (error: any) {
      toast((t) => (
        <CustomToast
          type={ToastTypes.ERROR}
          title="Error"
          message={error.response.data.title}
          t={t}
        />
      ))
    }
  }

  const fetchNewsletters = async () => {
    try {
      const response = await NewsletterApiService.fetchNewsletters();
      const newslettersArr: INewsletter[] = response.map((newsletter) => {
        return {
          name: newsletter.name.substring(0, 20),
          creationDate: newsletter.creationDate,
          emailTemplateID: newsletter.emailTemplateID,
          subject: newsletter.subject.substring(0, 20),
          status: newsletter.status,
          newsletterID: newsletter.newsletterID
        }
      })
      setNewsletters(newslettersArr);
    } catch (error: any) {
      toast((t) => (
        <CustomToast
          type={ToastTypes.ERROR}
          title="Error"
          message={error.response.data.title}
          t={t}
        />
      ))
    }
  }

  const createNewsletter = async (data: ICreateNewsletter) => {
    setBusy(true);
    try {
      const response = await NewsletterApiService.createNewsletter({
        ...data
      })
      setNewsletters([...newsletters, response]);

      toast((t) => (
        <CustomToast
          type={ToastTypes.SUCCESS}
          title="Success"
          message="You have successfully created the newsletter!"
          t={t}
        />
      ))
      setModalOpen(false);
    } catch (error: any) {
      toast((t) => (
        <CustomToast
          type={ToastTypes.ERROR}
          title="Error"
          message={error.response.data.title}
          t={t}
        />
      ))
    } finally {
      setBusy(false)
    }
  }

  const modalContent = () => {

    return (
      <form onSubmit={handleSubmit(createNewsletter)}>
        <Controller
          control={control}
          name="name"
          render={({ formState, field: { onBlur, onChange } }) => (
            <Input
              name="name"
              placeholder="Newsletter Name"
              onBlur={onBlur}
              onChange={onChange}
              formState={formState}
              maxLength={20}
            />
          )}
        />
        <Controller
          control={control}
          name="subject"
          render={({ formState, field: { onBlur, onChange } }) => (
            <Input
              name="subject"
              placeholder="Subject"
              onBlur={onBlur}
              onChange={onChange}
              formState={formState}
              maxLength={20}
            />
          )}
        />
        <Controller
          control={control}
          name="emailTemplateID"
          render={({ formState, field: { onBlur, onChange } }) => (
            <div className="w-1/2">
              <ComboboxInput
                src="/icons/bwv-newsletter-icon.svg"
                alt="Newsletter Icon"
                width={20}
                height={20}
                name="emailTemplate"
                placeholder="Email Template"
                onBlur={onBlur}
                onChange={onChange}
                formState={formState}
                labelKey="name"
                valueKey="templateId"
                options={templates}
                defaultValue={templates[0]}
              />
            </div>
          )}
        />
      </form>
    )
  }

  const columns = useMemo(
    () => [
      {
        id: "toolbar",
        Header: (table: any) => {
          return (
            <NewsletterTableToolbar table={table} />
          )
        },
        columns: [
          {
            Header: "Newsletter Name",
            accessor: "name",
            cellClass: "!text-bwv-dark font-medium",
          },
          {
            Header: "Subject",
            accessor: "subject",
            cellClass: "!text-bwv-dark font-medium",
          },
          {
            Header: "Status",
            accessor: "status",
            Cell: ({ row }: { row: { original: INewsletter } }) => (
              <StatusIndicator
                label={statusToDataMap[row.original.status].label}
                color={statusToDataMap[row.original.status].color} />
            )
          },
          {
            Header: "Creation Date",
            accessor: "creationDate",
            cellClass: "!text-bwv-dark font-medium",
            Cell: ({ row }: { row: { original: INewsletter } }) => (
              <Moment format="DD/MM/YY">{row.original.creationDate}</Moment>
            )
          },
          {
            Header: "Email Template",
            accessor: "emailTemplateID",
            cellClass: "!text-bwv-dark font-medium"
          },
          {
            Header: () => null,
            id: "info",
            Cell: ({ row }: { row: { original: INewsletter } }) => (
              <div className="flex justify-end cursor-pointer text-bwv-dark">
                <NewsletterDropdownMenu data={row.original} />
              </div>
            )
          }

        ]
      }
    ],
    []
  )

  useEffect(() => {
    fetchEmailTemplates();
    fetchNewsletters();
  }, [])

  return (
    <div className="flex w-full my-11 mx-8">
      <div className="w-full bg-white rounded-3xl">
        <div className="flex justify-between items-center pt-8 pb-5 px-14">
          <h2 className="text-[28px] text-bwv-dark font-semibold">
            Newsletter
          </h2>
          <div className="w-fit">
            <Button text={"Create a Newsletter"} size={"small"} onClick={() => setModalOpen(true)} />
          </div>
          <Modal
            title={"Newsletter"}
            isOpen={modalOpen}
            onClose={() => setModalOpen(false)}
            content={modalContent}
            proceedBtn={{
              name: "Create a Newsletter",
              onClick: handleSubmit(createNewsletter)
            }}
            proceedBtnDisabled={!isValid || busy}
            proceedBtnLoading={busy}
            proceedBtnLoadingText="Creating..."
            cancelBtn={{
              name: "Cancel",
              onClick: () => setModalOpen(false)
            }}
          />
        </div>
        <NewsletterSubNavigation />
        <div className="flex flex-col px-6">
          <Table columns={columns} data={newsletters} checkbox={false} />
        </div>
      </div>
    </div>
  )
}

export default NewsletterContent;