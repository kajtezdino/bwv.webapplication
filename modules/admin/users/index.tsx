import { useRouter } from "next/router";
import { useCallback, useEffect, useMemo } from "react";
import { useRecoilState } from "recoil";
import { Avatar, StatusIndicator, Table } from "../../../common/components";
import { IUser, IStatus } from "../../../common/models";
import { usersState } from "../../../store/atoms/usersAtom";
import UserManagementApiService from "./api.service"
import { UserManagementTableToolbar } from "./components";
import Moment from "react-moment";
import Image from 'next/image';

const AdminUsersContent: React.FC = () => {
  const router = useRouter();
  const [users, setUsers] = useRecoilState(usersState);

  useEffect(() => {
    UserManagementApiService.fetchUsers()
      .then((res: IUser[]) => setUsers(res))
  }, [setUsers])

  const openUserInfoPage = useCallback((user: IUser) => {
    router.push(`users/${user.id}`)
  }, [router])

  const statusToDataMap: {[key: number]: IStatus} = {
    1: {
      label: "Active",
      color: "bg-bwv-accent"
    },
    2: {
      label: "Ads approval pending",
      color: "bg-bwv-orange"
    },
    3: {
      label: "Suspended",
      color: "bg-bwv-error"
    }
  }

  const columns = useMemo(
    () => [
      {
        id: 'toolbar',
        className: 'border-b border-[#f1f1f1] !py-0 !px-0',
        Header: (table: any) => {
          return (
            <UserManagementTableToolbar table={table} />
          )
        },
        columns: [
          {
            Header: () => null,
            id: 'avatar',
            Cell: ({ row }: { row: { original: IUser } }) => (
              <Avatar size={36} user={row.original} />
            ),
            className: "!w-[0.0000000001%] pr-6"
          },
          {
            Header: 'Name',
            className: "!text-[#252525] font-semibold",
            Cell: ({ row }: { row: { original: IUser } }) => (
              <>{row.original.firstName} {row.original.lastName}</>
            ),
          },
          {
            Header: 'Email',
            accessor: 'email',
          },
          {
            Header: 'Location',
            accessor: 'location',
          },
          {
            Header: 'Status',
            accessor: 'accountStatus',
            id: 'accountStatus',
            Cell: ({ row }: { row: { original: IUser } }) => (
              <StatusIndicator
                label={statusToDataMap[row.original.accountStatus].label}
                color={statusToDataMap[row.original.accountStatus].color} />
            )
          },
          {
            Header: 'Created',
            accessor: 'creationDate',
            Cell: ({ row }: { row: { original: IUser } }) => (
              <Moment format="MMM DD YYYY">{row.original.creationDate}</Moment>
            )
          },
          {
            Header: () => null,
            id: 'info',
            Cell: ({ row }) => (
              <div className="w-[21px] h-[21px] cursor-pointer opacity-75 hover:opacity-100" onClick={() => openUserInfoPage(row.original)}>
                <Image
                  src="/icons/bwv-info-icon.svg"
                  width={21}
                  height={21}
                  alt="Info icon"
                />
              </div>
            ),
            className: "!w-[0.0000000001%] pl-6"
          }
        ]
      }
    ],
    [openUserInfoPage]
  )

  return (
    <div className="flex w-full min-h-[calc(100vh-130px)]">
      <div className="w-full bg-white rounded-3xl">
        {
          users
            ? <Table columns={columns} data={users} />
            : null
        }
      </div>
    </div>
  )
}

export default AdminUsersContent