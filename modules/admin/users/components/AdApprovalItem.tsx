import Image from 'next/image'
import Moment from 'react-moment'
import { TagType } from '../../../../common/models'
import { Tag } from '../../../../common/components'

interface IAd {
  imageSrc: string
  date: string
  description:string
  title: string
  type: TagType
}

const AdApprovalItem: React.FC<IAd> = ({
  type,
  imageSrc,
  date,
  title,
  description
}) => {
  return (
    <div className="flex flex-col rounded-[15px] p-5 bg-bwv-white-dirty gap-2">
      <Image
        src={imageSrc}
        alt="image"
        width={212}
        height={114}
        className="rounded-[10px] w-full h-auto object-cover"
      />

      <div>
        {/* TODO Will be exchanged with real data */}
        {/* <Tag type={type} /> */}
      </div>

      <div className="mt-4 flex flex-row text-[10px] font-bold">
        <p className="text-bwv-label">POSTED:</p>{" "}
        <Moment className="text-bwv-accent pl-1" format='DD MMM'>{date}</Moment>
      </div>

      <div>
        <h3 className="font-semibold leading-[20px] text-bwv-dark">
          {title}
        </h3>
        <p className="mt-2 text-xs text-bwv-dark-light">
          {description}
        </p>
      </div>

      <div className="pt-5 flex flex-row items-center justify-center gap-4">
        <button
          className="w-1/2 px-3 py-2 rounded-xl border border-bwv-accent text-sm font-semibold text-bwv-accent"
          onClick={() => {}}
        >
          Approve
        </button>
        <button
          className="w-1/2 px-3 py-2 rounded-xl border border-bwv-error text-sm font-semibold text-bwv-error"
          onClick={() => {}}
        >
          Reject
        </button>
      </div>
    </div>
  )
}

export default AdApprovalItem;
