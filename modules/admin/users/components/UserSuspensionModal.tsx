import { useEffect, useState } from "react"
import { useRecoilState, useRecoilValue } from "recoil"
import UserManagementApiService from '../api.service'
import { IUser } from "../../../../common/models"
import { userState } from "../../../../store/atoms/userAtom"
import { usersState } from "../../../../store/atoms/usersAtom"
import { CustomToast, Modal } from "../../../../common/components"
import toast from "react-hot-toast"
import { ToastTypes, UserAccountStatus } from "../../../../common/enums"
import { useSWRConfig } from "swr"

const UserSuspensionModal: React.FC<{ id: string, user: IUser, isOpened: boolean, onClose: () => void }> = ({
  id,
  user,
  isOpened,
  onClose
}) => {
  const adminUser: IUser = useRecoilValue(userState)
  const [users, setUsers] = useRecoilState(usersState)
  const [busy, setBusy] = useState<boolean>(false)
  const [openModal, setOpenModal] = useState(false)
  const [inputValue, setInputValue] = useState('')
  const { mutate } = useSWRConfig()

  useEffect(() => {
    setOpenModal(isOpened)
  }, [isOpened])

  const closeModal = () => {
    onClose()
  }

  const modalContent = () => {
    return (
      <>
        <p className="text-2xl font-semibold text-center">
          {user?.accountStatus !== UserAccountStatus.SUSPENDED ? "Do you want to suspend " : "Do you want to activate "}
          {user?.firstName} {user?.lastName}?
        </p>
        <form>
          <textarea
            placeholder={user?.accountStatus !== UserAccountStatus.SUSPENDED ? "Suspension details" : "Activation details"}
            id={user?.accountStatus !== UserAccountStatus.SUSPENDED ? "activationDetails" : "suspensionDetails"}
            className="bg-bwv-white-dirty rounded-xl p-5 w-full min-h-[300px] text-bwv-dark focus:outline-none resize-none"
            onChange={(e) => setInputValue(e.target.value)} />
        </form>
      </>
    )
  }

  const suspendActivateUser = async () => {
    setBusy(true)
    try {
      const params = {
        id: id,
        accountStatus: user.accountStatus !== UserAccountStatus.SUSPENDED ? UserAccountStatus.SUSPENDED : UserAccountStatus.ACTIVE,
        suspensionReason: inputValue,
        suspendedBy: adminUser.id,
      }
      await UserManagementApiService.suspendActivateUser(params)
      await mutate({url: '/apis/Profile/GetProfile', id: id})

      setUsers(users.map((user) => {
        const newUser = { ...user }
        if (newUser.id === id) {
          newUser.accountStatus = params.accountStatus;
        }
        return newUser
      }))
      closeModal()

      toast((t) => (
        <CustomToast
          type={ToastTypes.SUCCESS}
          title="Success"
          message={`${user?.firstName} ${user?.lastName} ${user.accountStatus === UserAccountStatus.SUSPENDED ? 'activated' : 'suspended'}`}
          t={t}
        />
      ))
    } catch (error: any) {
      console.error(error)
      toast((t) => (
        <CustomToast
          type={ToastTypes.ERROR}
          title="Error"
          message={`${error.response}`}
          t={t}
        />
      ))
    } finally {
      setBusy(false)
    }
  }
  return (
    <Modal
      title={user?.accountStatus !== UserAccountStatus.SUSPENDED ? "Suspension details" : "Activation details"}
      isOpen={openModal}
      onClose={closeModal}
      content={modalContent()}
      proceedBtnDisabled={!inputValue || busy}
      proceedBtnLoading={busy}
      proceedBtnLoadingText={user?.accountStatus !== UserAccountStatus.SUSPENDED ? "Suspending User" : "Activating User"}
      proceedBtn={{
        name: user?.accountStatus !== UserAccountStatus.SUSPENDED ? "Suspend" : "Activate",
        onClick: () => suspendActivateUser()
      }}
      cancelBtn={{
        name: "Cancel",
        onClick: () => closeModal()
      }}
    />
  )
}

export default UserSuspensionModal; 