import { useMemo, useState } from 'react';
import { StatusIndicator } from '../../../../common/components';

interface IStatusDataMap {
  label: string,
  color: string
}
const UserManagementStatusFilters: React.FC<{ onChange: (value: number) => void }> = ({
  onChange
}) => {
  const [activeFilter, setActiveFilter] = useState<number>(0)

  const filters = useMemo(() => {
    return [
      {
        label: 'All users',
        value: 0
      },
      { value: 1 },
      { value: 2 },
      { value: 3 }
    ]
  }, [])

  const statusToDataMap: {[key: number]: IStatusDataMap} = {
    1: {
      label: "Active",
      color: "bg-bwv-accent"
    },
    2: {
      label: "Ads approval pending",
      color: "bg-bwv-orange"
    },
    3: {
      label: "Suspended",
      color: "bg-bwv-error"
    }
  }

  const onFilterChange = (value: number) => {
    onChange(value)
    setActiveFilter(value)
  }

  return (
    <>
      {filters.map(filter => (
        <div key={filter.value} onClick={() => onFilterChange(filter.value)}>
          {
            filter.label
              ? <div className={`
              status-indicator hover:bg-bwv-accent-dark hover:!text-white cursor-pointer
              ${activeFilter === filter.value ? "bg-bwv-accent-dark !text-white" : ""}
            `}>
                <span className="inline-block text-base leading-5 font-normal">All users</span>
              </div>
              : <StatusIndicator
                label={statusToDataMap[filter.value].label}
                color={statusToDataMap[filter.value].color}
                classes={`hover:bg-bwv-accent-dark hover:!text-white cursor-pointer transition-all ${activeFilter === filter.value ? "bg-bwv-accent-dark !text-white" : ""}`}
              /> 
          } 
        </div>
      ))}
    </>
  )
}

export default UserManagementStatusFilters