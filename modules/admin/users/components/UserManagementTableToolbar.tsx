import Image from 'next/image';
import { useRouter } from 'next/router';
import { UserManagementStatusFilters } from '.'

const UserManagementTableToolbar: React.FC<{ table: any }> = ({ table }) => {
  const router = useRouter();
  const filterUsers = (value: number) => {
    table.setFilter('accountStatus', value)
  }

  return (
    <div className="flex w-full justify-between items-center border-b border-[#f1f1f1] px-8">
      {/* Actions */}
      <div>
        <Image
          src="/icons/bwv-options-icon.svg"
          width={4}
          height={17}
          alt="Options Icon"
        />
      </div>
      {/* Filters */}
      <div className="flex gap-4">
        <UserManagementStatusFilters onChange={filterUsers}/>
      </div>
      
      <div className="flex items-center gap-8">
        <button
        onClick={() => router.push('users/new')}
          className="font-semibold text-base leading-[20px] rounded-2xl px-[15px] py-[10px] bg-bwv-accent text-white hover:bg-bwv-accent-dark"
        >
          + Add User
        </button>
        {/* Pagination */}
        <div className="pagination h-20 flex justify-end items-center gap-6">
          <div className="text-sm text-[#D4D4D4] leading-[22px] font-normal">
            {table.state.pageIndex + 1} of {table.pageOptions.length}
          </div>
          <div className="inline-flex gap-8">
            <button onClick={() => table.previousPage()} disabled={!table.canPreviousPage}>
              <Image
                src="/icons/bwv-arrow-light-icon.svg"
                width={12}
                height={22}
                alt="Arrow"
              />
            </button>
            <button onClick={() => table.nextPage()} disabled={!table.canNextPage}>
              <Image
                className="rotate-180"
                src="/icons/bwv-arrow-light-icon.svg"
                width={12}
                height={22}
                alt="Arrow"
              />
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default UserManagementTableToolbar;