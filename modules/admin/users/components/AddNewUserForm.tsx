import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { yupResolver } from "@hookform/resolvers/yup";
import { Controller, useForm } from "react-hook-form";
import {
  Button,
  ComboboxInput,
  CustomToast,
  Input,
  SelectInput,
  PasswordStrengthBarIndicator,
  PasswordStrengthRulesPopover
} from "../../../../common/components";
import { IAddNewUser } from "../../../../common/models";
import { AddNewUserSchema, PasswordResetSchema } from "../../../../common/schemas";
import { AddNewUserImage } from ".";
import UserManagementApiService from "../api.service";
import AuthApiService from "../../../auth/api.service";
import toast from "react-hot-toast";
import zxcvbn from "zxcvbn";
import { ToastTypes } from "../../../../common/enums";

const AddNewUserForm: React.FC = () => {
  const router = useRouter();
  const [image, setImage] = useState<File | null>(null);
  const [password, setPassword] = useState('')
  const [testResult, setTestResult] = useState<any>(null)

  useEffect(() => {
    setTestResult(zxcvbn(password))
  }, [password])

  const { handleSubmit, control, formState: { errors, isValid } } = useForm<IAddNewUser>({
    mode: 'onChange',
    reValidateMode: 'onChange',
    resolver: yupResolver(AddNewUserSchema)
  });

  const goToUsersList = () => {
    router.push('/admin/users')
  }

  const onSubmit = async (data: IAddNewUser) => {
    try {
      const response = await UserManagementApiService.createProfile({
        ...data,
        ...(image && { imageUrl: image })
      })
      await AuthApiService.sendConfirmationMail(response.userId)
      goToUsersList()
      toast((t) => (
        <CustomToast
          type={ToastTypes.SUCCESS}
          title="Success"
          message="You have successfully created a new user. Yay!"
          t={t}
        />
      ))
    } catch (e) {
      const error = String(e)
      const modifiedError = error.split(':')[1]

      toast((t) => (
        <CustomToast
          type={ToastTypes.ERROR}
          title="Error"
          message={`${modifiedError}`}
          t={t}
        />
      )
      )
    }

  }

  return (
    <>
      <AddNewUserImage onChange={setImage} />

      <form
        className="pt-9"
        onSubmit={handleSubmit(onSubmit)}
      >
        <div className="grid grid-cols-2 gap-4">
          <Controller
            control={control}
            name="firstName"
            render={({ formState, field: { onBlur, onChange } }) => (
              <Input
                iconUrl="/icons/bwv-wheat-black-icon.svg"
                width={22}
                height={22}
                name="firstName"
                placeholder="User"
                onBlur={onBlur}
                onChange={onChange}
                formState={formState}
                size="small"
                label="First name"
              />
            )}
          />

          <Controller
            control={control}
            name="email"
            render={({ formState, field: { onBlur, onChange } }) => (
              <Input
                iconUrl="/icons/bwv-email-black-icon.svg"
                width={18}
                height={18}
                name="email"
                type="email"
                placeholder="Your Email"
                onBlur={onBlur}
                onChange={onChange}
                formState={formState}
                size="small"
                label="Email"
              />
            )}
          />

          <Controller
            control={control}
            name="lastName"
            render={({ formState, field: { onBlur, onChange } }) => (
              <Input
                iconUrl="/icons/bwv-wheat-black-icon.svg"
                width={22}
                height={22}
                name="lastName"
                placeholder="Last name"
                onBlur={onBlur}
                onChange={onChange}
                formState={formState}
                size="small"
                label="Last name"
              />
            )}
          />

          <Controller
            control={control}
            name="location"
            render={({ formState, field: { onBlur, onChange } }) => (
              <ComboboxInput
                src="/icons/bwv-location-black-icon.svg"
                alt="Location Icon"
                name="location"
                placeholder="Location"
                onBlur={onBlur}
                onChange={onChange}
                formState={formState}
                label="Location"
                options={[
                  'Toronto',
                  'Calgary',
                  'Montreal',
                  'Victoria',
                  'Regina',
                  'Vancouver'
                ]}
                defaultValue={'Toronto'}
              />
            )}
          />

          <Controller
            control={control}
            name="password"
            render={({ formState, field: { onBlur, onChange } }) => (
              <Input
                iconUrl="/icons/bwv-lock-black-icon.svg"
                width={16}
                height={20}
                name="password"
                placeholder="New Password"
                onBlur={() => {
                  if (testResult?.score < 4) {
                    toast((t) => (
                      <CustomToast
                        type={ToastTypes.WARNING}
                        title="Warning"
                        message={() => (
                          <PasswordStrengthRulesPopover />
                        )}
                        t={t}
                      />
                    ))
                  }
                }}
                onChange={(e: string) => {
                  setPassword(e)
                  onChange(e)
                }}
                formState={formState}
                type="password"
                size="small"
                label="New password"
              />
            )}
          />

          <Controller
            control={control}
            name="gender"
            render={({ formState, field: { onBlur, onChange } }) => (
              <SelectInput
                name="gender"
                placeholder="Gender"
                options={[
                  { id: 1, name: 'Male' },
                  { id: 2, name: 'Female' },
                  { id: 3, name: 'Other' },
                ]}
                valueKey="id"
                labelKey="name"
                onBlur={onBlur}
                onChange={onChange}
                formState={formState}
                label="Gender"
                defaultValue={1}
              />
            )}
          />

          <Controller
            control={control}
            name="confirmPassword"
            render={({ formState, field: { onBlur, onChange } }) => (
              <Input
                iconUrl="/icons/bwv-lock-black-icon.svg"
                width={16}
                height={20}
                name="confirmPassword"
                placeholder="Confirm Password"
                onBlur={onBlur}
                onChange={onChange}
                formState={formState}
                type="password"
                size="small"
                label="Confirm password"
              />
            )}
          />

          <Controller
            control={control}
            name="phoneNumber"
            render={({ formState, field: { onBlur, onChange } }) => (
              <Input
                iconUrl="/icons/bwv-phone-black-icon.svg"
                width={18}
                height={18}
                name="phoneNumber"
                placeholder="Contact phone"
                onBlur={onBlur}
                onChange={onChange}
                formState={formState}
                size="small"
                label="Contact phone"
              />
            )}
          />

          <PasswordStrengthBarIndicator passwordLength={password} testResultScore={testResult?.score} />

          <Controller
            control={control}
            name="role"
            render={({ formState, field: { onBlur, onChange } }) => (
              <SelectInput
                name="role"
                placeholder="Role"
                options={['SubscribedUser', 'Administrator']}
                onBlur={onBlur}
                onChange={onChange}
                formState={formState}
                label="Role"
                defaultValue="SubscribedUser"
              />
            )}
          />
        </div>

        <div className="w-full flex justify-end gap-4 pt-4">
          <Button
            text="Cancel"
            size="small"
            color="secondary"
            type="button"
            onClick={goToUsersList}
          />
          <Button
            text="Add"
            type="submit"
            size="small"
            disabled={!isValid || testResult?.score < 4}
          />
        </div>
      </form>
    </>
  )
}

export default AddNewUserForm;