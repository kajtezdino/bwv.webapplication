import { AddNewUserForm } from "./components";

const AdminUsersNewContent: React.FC = () => {
  return (
    <div className='w-[876px] max-w-full p-8 rounded-[20px] bg-white'>
      <h1 className="text-3xl font-semibold pb-6">Add new user</h1>

      <AddNewUserForm />
    </div>
  )
}

export default AdminUsersNewContent;