import { http } from '../../../common/utils/http';
import { AxiosResponse } from 'axios';
import {
  IUser,
  CreateProfileRequestParams,
  SuspendActivateProfileRequestParams
} from '../../../common/models';

class UserManagementApiService {
  fetchUsers = (): Promise<IUser[]> =>
    http<IUser[]>({
      url: `/apis/Profile/GetProfiles`,
      method: 'get',
    })

  getUser = (id: string): Promise<IUser> =>
    http<IUser>({
      url: `/apis/Profile/GetProfile/${id}`,
      method: 'get'
    })

  suspendActivateUser = (params: SuspendActivateProfileRequestParams): Promise<AxiosResponse> => {
    return http<AxiosResponse>({
      url: `/apis/Profile`,
      method: 'post',
      body: { ...params }
    })
  }

  createProfile = (params: CreateProfileRequestParams): Promise<{ userId: string }> => {
    const formData = new FormData();
    
    for (const param in params) {
      formData.append(param, (params as { [key: string]: any })[param])
    }

    return http<{ userId: string }>({
      url: `/apis/Profile/CreateProfile`,
      method: 'post',
      headers: {
        'content-type': 'multipart/form-data'
      },
      body: formData
    })
  }
}

export default new UserManagementApiService();