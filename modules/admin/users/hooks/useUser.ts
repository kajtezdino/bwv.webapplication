import useSWR from "swr";
import UserManagementApiService from "../api.service";

const useUser = (id: string) => {
  const fetcher = ({ id }: {id: string}) => UserManagementApiService.getUser(id)
  return useSWR({url: '/apis/Profile/GetProfile', id: id}, fetcher)
}

export default useUser;