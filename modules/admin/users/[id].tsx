import { ChevronLeftIcon } from '@heroicons/react/solid'
import { useRouter } from 'next/router'
import { Avatar } from '../../../common/components'
import useUser from './hooks/useUser'
import { AdsResponse, IUser } from '../../../common/models'
import { UserSuspensionModal } from './components'
import { useEffect, useState } from 'react'
import { UserAccountStatus } from '../../../common/enums'
import AdsTable from "../ads/components/AdsTable";
import adsHistoryService from "../ads/ads-history.service";
import { useRecoilState } from "recoil";
import { adsHistoryState } from "../../../store/atoms/adsHistoryAtom";
import { adsHistoryRequestState } from "../../../store/atoms/adsHistoryRequestAtom";

export const AdminUsersIdContent: React.FC = () => {
  const router = useRouter()
  const { id } = router.query
  const { data } = useUser(id as string)
  const [openModal, setOpenModal] = useState(false)
  const [, setAdsHistory] = useRecoilState(adsHistoryState)
  const [adRequestParams] = useRecoilState(adsHistoryRequestState)

  useEffect(() => {
    adsHistoryService.fetchAdsHistory(adRequestParams)
      .then((res: AdsResponse)=> {
        setAdsHistory(res)
      })
  }, [adRequestParams])

  return (
    <div className="w-[876px] max-w-full p-8 rounded-[20px] bg-white">
      <div className="flex gap-4 items-center mb-6">
        <ChevronLeftIcon
          height={32}
          className="cursor-pointer text-bwv-dark-gray transition-all hover:text-bwv-dark"
          onClick={() => router.push('/admin/users')}
        />
        <div className="h-[32px] w-[1px] bg-bwv-dark-gray"/>
        <h2 className="text-3xl font-semibold">Profile Info</h2>
      </div>
      <div className="flex flex-col items-center justify-between">
        {data ? (
          <>
            <Avatar user={data} size={140}/>
            <h3 className="mt-4 text-2xl">
              {data?.firstName + ' ' + data?.lastName}
            </h3>
          </>
        ) : (
          <>
            <div className="h-[140px] w-[140px] rounded-full bg-slate-300"/>
            <h3 className="mt-4 text-2xl">Loading user</h3>
          </>
        )}
      </div>

      <div className="pt-6">
        <div className="flex items-center justify-between border-b border-bwv-light-gray pb-4">
          <h3 className="text-2xl">Basic Info</h3>
          {
            data &&
                        <button
                          className={`rounded-2xl px-[15px] py-[10px] text-base font-semibold text-white transition-all  
              ${data?.accountStatus !== UserAccountStatus.SUSPENDED ? "bg-bwv-error hover:bg-bwv-error-dark" : "bg-bwv-accent hover:bg-bwv-accent-dark"}`}
                          onClick={() => {
                            setOpenModal(true)
                          }}
                        >
                          {data?.accountStatus !== UserAccountStatus.SUSPENDED ? "Suspend" : "Activate"}
                        </button>
          }

          <UserSuspensionModal
            id={id as string}
            user={data as IUser}
            isOpened={openModal}
            onClose={() => setOpenModal(false)}
          />
        </div>

        <div className="inline-grid grid-cols-2 gap-6 pt-7">
          <h4 className="text-sm font-semibold uppercase text-bwv-gray">
                        Name & Surname
          </h4>
          <p className="text-base">{data?.firstName + ' ' + data?.lastName}</p>

          <h4 className="text-sm font-semibold uppercase text-bwv-gray">
                        Email
          </h4>
          <p className="text-base">{data?.email}</p>

          <h4 className="text-sm font-semibold uppercase text-bwv-gray">
                        Location
          </h4>
          <p className="text-base">{data?.location}</p>
        </div>
      </div>

      <AdsTable userId={id as string} />
    </div>
  )
}

export default AdminUsersIdContent
