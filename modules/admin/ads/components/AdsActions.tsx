import Image from "next/image";
import {
  AutoTextArea,
  CustomToast,
  DropdownMenu,
  Modal
} from "../../../../common/components";
import { useState } from "react";
import toast from "react-hot-toast";
import { ToastTypes } from "../../../../common/enums";
import { IAd } from "../../../../common/models";
import AdsHistoryService from "../ads-history.service";
import { AdStatus } from "../../../../common/enums";
import { useRecoilState } from "recoil";
import { adsHistoryRequestState } from "../../../../store/atoms/adsHistoryRequestAtom";

const AdsActionsDropdownMenu: React.FC<{ data: IAd }> = ({ data  }) => {
  const [openModal, setOpenModal] = useState(false);
  const [busy, setBusy] = useState(false);
  const [modalOptions, setModalOptions] = useState({
    title: '',
    subTitle: '',
    placeholder: ''
  })
  const [adAction, setAdAction] = useState('')

  const [description, setDescription] = useState<string>('')
  const [adRequestParams, setAdRequestParams] = useRecoilState(adsHistoryRequestState)

  const handleOnChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    setDescription(e.target.value)
  }

  const rejectAd = async () => {
    setModalOptions(() => ({
      title: 'Reject Details',
      subTitle: 'Do you want to reject selected ad?',
      placeholder: 'Write your rejection details here...'
    }))
    setAdAction('REJECTED')
    setOpenModal(true)
  }
  const closeAd = async () => {
    setModalOptions(() => ({
      title: 'Close Details',
      subTitle: 'Do you want to close selected ad?',
      placeholder: 'Write your closing details here...'
    }))
    setAdAction('CLOSED')
    setOpenModal(true)
  }

  const updateAd = async (action?: string) => {
    setBusy(true);
    try {
      await AdsHistoryService.updateAd({
        adID: data.adID,
        adStatus: Number(AdStatus[(action ? action : adAction as any)]),
        reason: description
      });
      toast((t) => (
        <CustomToast
          type={ToastTypes.SUCCESS}
          title="Success"
          message="Ad has been successfully approved!"
          t={t}
        />
      ))
      setAdRequestParams({ ...adRequestParams })
      setOpenModal(false);
    } catch (err: any) {
      toast((t) => (
        <CustomToast
          type={ToastTypes.ERROR}
          title="Error"
          message={err.response}
          t={t}
        />
      )
      )
    } finally {
      setBusy(false)
    }
  }

  const menuBtn = () => {
    return (
      <div className="w-8 h-7 flex justify-end">
        <Image
          src="/icons/bwv-options-icon.svg"
          width={4}
          height={17}
          alt="Options Icon"
        />
      </div>
    )
  }

  const menuItems = [
    {
      name: "Approve",
      onClick: () => updateAd('APPROVED')
    },
    {
      name: "Reject",
      onClick: () => rejectAd()
    },
    {
      name: "Close",
      onClick: () => closeAd()
    }
  ]

  const modalContent = () => {
    return (
      <div>
        <div className="subtitle text-center my-4">
          <h1 className="font-bold">{modalOptions.subTitle}</h1>
        </div>
        <AutoTextArea
          className="bg-bwv-white-dirty rounded-xl focus:border-[1px] focus-visible:border-bwv-accent focus:outline-none py-2 px-3 min-h-[216px] w-full"
          placeholder={modalOptions.placeholder}
          onChange={handleOnChange}
        />
      </div>
    )
  }

  return (
    <div>
      <DropdownMenu menuBtn={menuBtn} menuItems={menuItems} width={"w-44"} />
      <Modal
        title={modalOptions.title}
        isOpen={openModal}
        content={modalContent}
        onClose={() => setOpenModal(false)}
        proceedBtn={{
          name: "Save",
          onClick: () => updateAd()
        }}
        cancelBtn={{
          name: "Cancel"
        }}
        proceedBtnLoading={busy}
        proceedBtnLoadingText="Saving..."
      />
    </div>
  )
}

export default AdsActionsDropdownMenu;