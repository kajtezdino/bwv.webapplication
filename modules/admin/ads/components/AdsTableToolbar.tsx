import Image from 'next/image';
import { useRecoilState, useRecoilValue } from "recoil";
import { adsHistoryState } from "../../../../store/atoms/adsHistoryAtom";
import { adsHistoryRequestState } from "../../../../store/atoms/adsHistoryRequestAtom";
import AdsManagementStatusFilter from "./AdsManagementStatusFilter";

const AdsTableToolbar: React.FC<{showFilters: boolean}> = ({ showFilters }) => {
  const adsHistory = useRecoilValue(adsHistoryState)
  const [adRequestParams, setAdRequestParams] = useRecoilState(adsHistoryRequestState)

  const goToPreviousPage = () => {
    setAdRequestParams({
      ...adRequestParams,
      pageNumber: adsHistory.paging.currentPage - 1
    })
  }

  const goToNextPagePage = () => {
    setAdRequestParams({
      ...adRequestParams,
      pageNumber: adsHistory.paging.currentPage + 1
    })
  }

  const filterAds = (value: number) => {
    setAdRequestParams({
      ...adRequestParams,
      adStatus: value
    })
  }

  return (
    <div className="flex w-full justify-between items-center border-b border-[#f1f1f1] px-8">
      {/* Actions */}
      <div className="flex items-center justify-between border-bwv-light-gray pb-4">
        {!showFilters && <h3 className="text-2xl">Ads</h3>}
      </div>
      {/* Filters */}
      {showFilters && (<div className="flex gap-4">
        <AdsManagementStatusFilter onChange={filterAds}/>
      </div>)}

      <div className="flex items-center gap-8">
        {/* Pagination */}
        <div className="pagination h-20 flex justify-end items-center gap-6">
          <div className="text-sm text-[#D4D4D4] leading-[22px] font-normal">
            {adsHistory.paging.currentPage} of {adsHistory.paging.totalPages}
          </div>
          <div className="inline-flex gap-8">
            <button onClick={goToPreviousPage} disabled={!adsHistory.paging.hasPrevious}>
              <Image
                src="/icons/bwv-arrow-light-icon.svg"
                width={12}
                height={22}
                alt="Arrow"
              />
            </button>
            <button onClick={goToNextPagePage} disabled={!adsHistory.paging.hasNext}>
              <Image
                className="rotate-180"
                src="/icons/bwv-arrow-light-icon.svg"
                width={12}
                height={22}
                alt="Arrow"
              />
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default AdsTableToolbar;