import { useMemo, useState } from 'react';
import { StatusIndicator } from '../../../../common/components';
import { IStatus } from "../../../../common/models";

const AdsManagementStatusFilter: React.FC<{ onChange: (value: number) => void }> = ({
  onChange
}) => {
  const [activeFilter, setActiveFilter] = useState<number>(0)

  const filters = useMemo(() => {
    return [
      {
        label: 'All Ads',
        value: 0
      },
      { value: 1 },
      { value: 2 },
      { value: 3 },
      { value: 4 }
    ]
  }, [])

  const statusToDataMap: { [key: string]: IStatus } = {
    1: {
      label: "Pending",
      color: "bg-bwv-orange"
    },
    2: {
      label: "Active",
      color: "bg-bwv-accent"
    },
    3: {
      label: "Rejected",
      color: "bg-bwv-error"
    },
    4: {
      label: "Closed",
      color: "bg-bwv-gray"
    },
  }

  const onFilterChange = (value: number) => {
    onChange(value)
    setActiveFilter(value)
  }

  return (
    <>
      {filters.map(filter => (
        <div key={filter.value} onClick={() => onFilterChange(filter.value)}>
          {
            filter.label
              ? <div className={`
              status-indicator hover:bg-bwv-accent-dark hover:!text-white cursor-pointer
              ${activeFilter === filter.value ? "bg-bwv-accent-dark !text-white" : ""}
            `}>
                <span className="inline-block text-base leading-5 font-normal">All ads</span>
              </div>
              : <StatusIndicator
                label={statusToDataMap[filter.value].label}
                color={statusToDataMap[filter.value].color}
                classes={`hover:bg-bwv-accent-dark hover:!text-white cursor-pointer transition-all ${activeFilter === filter.value ? "bg-bwv-accent-dark !text-white" : ""}`}
              />
          }
        </div>
      ))}
    </>
  )
}

export default AdsManagementStatusFilter