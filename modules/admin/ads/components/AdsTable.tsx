import { useEffect, useMemo } from "react";
import { useRecoilState } from "recoil";
import { adsHistoryState } from "../../../../store/atoms/adsHistoryAtom";
import { IAd } from "../../../../common/models";
import { adsHistoryRequestState } from "../../../../store/atoms/adsHistoryRequestAtom";
import { AdsActionsDropdownMenu, AdsTableToolbar } from "./index";
import Image from "next/image";
import { StatusIndicator, Table, Tag } from "../../../../common/components";
import Moment from "react-moment";
import { statusToDataMap } from "../../../../common/utils/status-to-data-map";
import { removeColumn } from "../../../../common/utils/remove-column";

export const AdsTable: React.FC<{userId?: string}> = ({ userId }) => {
  const [adsHistory] = useRecoilState(adsHistoryState)
  const [adRequestParams, setAdRequestParams] = useRecoilState(adsHistoryRequestState)

  const tableColumns = [
    {
      Header: () => null,
      id: 'avatar',
      Cell: ({ row }: { row: { original: IAd } }) => (
        <div className="flex content-center">
          {row.original?.coverImageUrl ?
            <Image
              className="rounded-full object-cover w-full h-full"
              src={row.original?.coverImageUrl}
              alt={"Image"}
              width={36}
              height={36}
            /> : null}
        </div>
      ),
    },
    {
      Header: 'ADS NAME',
      className: 'max-w-[50px] truncate',
      accessor: 'title',
    },
    {
      Header: 'Ads Type',
      accessor: 'adType',
      Cell: ({ row }: { row: { original: IAd } }) => (
        <Tag label={row.original.adType} />
      )
    },
    {
      Header: 'CATEGORY',
      className: 'max-w-[50px] truncate',
      accessor: 'category',
    },
    {
      Header: 'Date Added',
      accessor: 'creationDate',
      Cell: ({ row }: { row: { original: IAd } }) => (
        <Moment format="MMM DD YYYY">{row.original.creationDate}</Moment>
      )
    },
    {
      Header: 'Status',
      accessor: 'adStatus',
      className: 'max-w-[50px]',
      Cell: ({ row }: { row: { original: IAd } }) => (
        <StatusIndicator
          label={statusToDataMap[row.original.adStatus].label}
          color={statusToDataMap[row.original.adStatus].color}/>
      )
    },
    {
      Header: 'AD GROUP',
      className: 'max-w-[50px] truncate',
      accessor: 'adGroup',
    },
    {
      Header: () => null,
      id: "actions",
      className: 'max-w-[20px]',
      Cell: ({ row }: { row: { original: IAd } }) => (
        <div className="flex justify-end cursor-pointer text-bwv-dark">
          <AdsActionsDropdownMenu data={row.original} />
        </div>
      )
    }
  ]
  if (userId) {
    removeColumn(tableColumns, 'adGroup')
    removeColumn(tableColumns, 'category')
  }
  useEffect(() => {
    setAdRequestParams({
      ...adRequestParams,
      userId:(userId as string)
    })
  }, [])

  const columns = useMemo(
    () => [
      {
        id: 'toolbar',
        className: 'border-b border-[#f1f1f1] !py-0 !px-0',
        Header: () => {
          return (
            <AdsTableToolbar showFilters={!userId} />
          )
        },
        columns:  tableColumns
      }
    ],
    []
  )

  return (
    <div className="pt-8">
      <div className="flex w-full min-h-[calc(100vh-130px)]">
        <div className="w-full bg-white rounded-3xl">
          {
            adsHistory.ads
              ? <Table columns={columns} data={adsHistory.ads} checkbox={false}/>
              : null
          }
        </div>
      </div>
    </div>
  )
}

export default AdsTable
