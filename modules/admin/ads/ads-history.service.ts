import { http } from "../../../common/utils";
import { AdRequestParams, AdsResponse, UpdateAdRequestParams } from "../../../common/models";

class AdsHistoryApiService {

  fetchAdsHistory = async (params?: AdRequestParams): Promise<AdsResponse> =>
    http<AdsResponse>({
      url: `/buy-sell-api/Ad/AdHistory`,
      method: 'get',
      queryParams: {
        ...params,
      },
    })
  fetchAds = async (params?: AdRequestParams): Promise<AdsResponse> =>
    http<AdsResponse>({
      url: `/buy-sell-api/Ad/AdsManagement`,
      method: 'get',
      queryParams: {
        ...params,
      },
    })
  updateAd = async (params: UpdateAdRequestParams): Promise<void> =>
    http<void>({
      url: `/buy-sell-api/Ad/UpdateAdStatus`,
      method: 'patch',
      body: params
    })
}

export default new AdsHistoryApiService();