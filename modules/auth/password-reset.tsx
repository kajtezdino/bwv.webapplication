import { useRouter } from "next/router";
import { EmailInputView, NewPasswordView } from "./components";

const AuthPasswordResetContent: React.FC = () => {
  const router = useRouter()
  const { token, email } = router.query

  return (
    <>
      { token ? <NewPasswordView token={token as string} email={email as string} /> : <EmailInputView /> }
    </>
  )
}

export default AuthPasswordResetContent;