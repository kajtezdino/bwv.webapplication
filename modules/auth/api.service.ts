import { AxiosResponse } from 'axios';
import { http } from '../../common/utils/http';
import {
  IUserRegisterRequestParams,
  IUserRegisterResponseParams,
  IUserResetPasswordParams,
  IUserLoginRequestParams,
  IUserLoginResponseParams
} from '../../common/models';

class AuthApiService {
  login = (params: IUserLoginRequestParams): Promise<IUserLoginResponseParams> =>
    http<IUserLoginResponseParams>({
      url: params.url + '/apis/Auth/Login',
      method: 'post',
      body: {
        email: params.email,
        password: params.password,
        rememberMe: !!params.rememberMe
      },
      timeout: 6000000
    })
  
  register = (params: IUserRegisterRequestParams): Promise<IUserRegisterResponseParams> => 
    http<IUserRegisterResponseParams>({
      url: '/apis/Auth/Register',
      method: 'post',
      body: {...params}
    })

  confirmEmail = (params: {email: string, token: string}): Promise<AxiosResponse> =>
    http<AxiosResponse>({
      url: '/apis/Auth/ConfirmEmail',
      method: 'get',
      queryParams: {...params}
    })

  forgotPassword = (email: string): Promise<{message: string}> =>
    http<{message: string}>({
      url: '/apis/Auth/ForgotPassword',
      method: 'post',
      body: {
        email: email
      }
    })

  resetPassword = (params: IUserResetPasswordParams): Promise<AxiosResponse> =>
    http<AxiosResponse>({
      url: '/apis/Auth/ResetPassword',
      method: 'post',
      body: {...params}
    })

  sendConfirmationMail = (userId: string): Promise<AxiosResponse> =>
    http<AxiosResponse>({
      url: '/apis/Auth/SendConfirmationUrl',
      method: 'post',
      body: {
        userId
      }
    })
}

export default new AuthApiService();