import Link from "next/link";
import { useRouter } from "next/router";
import { SignInForm } from "./components";

const AuthSignInContent: React.FC = () => {
  const router = useRouter()
  const { token, email } = router.query
  
  return (
    <section className="w-[572px] max-w-full px-4">
      <h1 className="font-semibold text-2xl pb-5 text-bwv-dark">Sign in</h1>
      <p className="font-medium text-base leading-[20px] text-bwv-dark-gray pb-8">Welcome back, you&apos;ve been missed</p>
      
      <SignInForm token={token as string} email={email as string} />
      
      <p className="pt-4 leading-[20px] font-medium text-bwv-gray">
        Don&apos;t have an account yet?{" "}
        <Link href="/auth/sign-up">
          <a className="text-bwv-accent hover:text-bwv-accent-dark transition-all">Sign up</a>
        </Link>
      </p>
    </section>
  )
}

export default AuthSignInContent;