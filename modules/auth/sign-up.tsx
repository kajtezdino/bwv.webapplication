import Link from "next/link";
import { SignUpForm } from "./components";
import AuthApiService from "./api.service";
import { useState } from "react";

const AuthSignUpContent: React.FC = () => {
  const [userEmail, setUserEmail] = useState<string>('');
  const [showVerifyEmailStep, setShowVerifyEmailStep] = useState<boolean>(false);

  const onSubmit = async (data: { userId: string, email: string }): Promise<void> => {
    try {
      const response = await AuthApiService.sendConfirmationMail(data.userId)
      setUserEmail(data.email);
      setShowVerifyEmailStep(true);
    } catch(e) {
      console.error(e);
    }
  }

  const showView = () => {
    if (showVerifyEmailStep) {
      return (
        <section className="w-[572px] max-w-full px-4">
          <h1 className="font-semibold text-2xl pb-5 text-[#252525]">Verify your email</h1>
          <p className="font-medium text-base leading-[20px] text-[#8E98A9] pb-8">
            We&apos;ve sent a link to your email adress: <span className="text-bwv-accent">{userEmail}</span>
          </p>

          <Link href="/auth/sign-in">
            <a
              className="bg-bwv-accent inline-block w-full pb-[20px] pt-[20px] text-center leading-[20px] rounded-[15px] text-white font-semibold hover:bg-bwv-accent-dark transition-all"
            >
              Skip now
            </a>
          </Link>

          <p className="pt-4 leading-[20px] font-medium text-[#C1C7D0]">
            Didn&apos;t receive an email?{" "}
            <a className="text-bwv-accent cursor-pointer hover:text-bwv-accent-dark">Resend</a>
          </p>
        </section>
      )
    }

    return (
      <section className="w-[572px] max-w-full px-4">
        <h1 className="font-semibold text-2xl pb-5 text-[#252525]">Getting started</h1>
        <p className="font-medium text-base leading-[20px] text-[#8E98A9] pb-8">Create an account to continue!</p>
        
        <SignUpForm onSubmit={onSubmit} />

        <p className="pt-4 leading-[20px] font-medium text-[#C1C7D0]">
          Already have an account?{" "}
          <Link href="/auth/sign-in">
            <a className="text-bwv-accent hover:text-bwv-accent-dark transition-all">Sign in</a>
          </Link>
        </p>
      </section>
    )
  }
    
  return (
    <>
      {showView()}
    </>
  )
};

export default AuthSignUpContent;