import { useEffect, useState } from 'react'
import { useForm, Controller } from 'react-hook-form'
import { ISignUpFormInput } from '../../../common/models'
import { SignUpSchema } from '../../../common/schemas'
import {
  Checkbox,
  Input,
  TermsAndConditions,
  Button,
  CustomToast,
  PasswordStrengthBarIndicator,
  PasswordStrengthRulesPopover
} from '../../../common/components'
import { yupResolver } from '@hookform/resolvers/yup'
import zxcvbn from 'zxcvbn'
import AuthApiService from '../api.service'
import { ToastTypes, UserRoles } from '../../../common/enums'
import toast from 'react-hot-toast'

const SignUpForm: React.FC<{ onSubmit: (data: any) => void }> = ({
  onSubmit,
}) => {
  const [openTerms, setOpenTerms] = useState(false)
  const [busy, setBusy] = useState<boolean>(false)
  const [password, setPassword] = useState('')
  const [testResult, setTestResult] = useState<any>(null)

  useEffect(() => {
    setTestResult(zxcvbn(password))
  }, [password])

  const {
    handleSubmit,
    control,
    formState: { errors, isValid },
  } = useForm<ISignUpFormInput>({
    mode: 'onChange',
    reValidateMode: 'onSubmit',
    resolver: yupResolver(SignUpSchema),
  })

  const submit = async (data: any) => {
    try {
      setBusy(true)
      const apiParams = {
        role: UserRoles.SUBSCRIBER,
        ...data,
      }
      const response = await AuthApiService.register(apiParams)
      setBusy(false)
      onSubmit({ email: data.email, ...response })
    } catch (e: any) {
      setBusy(false)
    }
  }
  
  return (
    <form onSubmit={handleSubmit(submit)}>
      <Controller
        control={control}
        name="email"
        render={({ formState, field: { onBlur, onChange } }) => (
          <Input
            iconUrl="/icons/bwv-email-icon.svg"
            width={18}
            height={18}
            name="email"
            type="email"
            placeholder="Your Email"
            onBlur={onBlur}
            onChange={onChange}
            formState={formState}
          />
        )}
      />

      <div className="flex gap-4">
        <Controller
          control={control}
          name="firstName"
          render={({ formState, field: { onBlur, onChange } }) => (
            <Input
              iconUrl="/icons/bwv-wheat-icon.svg"
              width={22}
              height={22}
              name="firstName"
              placeholder="Your Name"
              onBlur={onBlur}
              onChange={onChange}
              formState={formState}
            />
          )}
        />

        <Controller
          control={control}
          name="lastName"
          render={({ formState, field: { onBlur, onChange } }) => (
            <Input
              iconUrl="/icons/bwv-wheat-icon.svg"
              width={22}
              height={22}
              name="lastName"
              placeholder="Lastname"
              onBlur={onBlur}
              onChange={onChange}
              formState={formState}
            />
          )}
        />
      </div>

      <Controller
        control={control}
        name="password"
        render={({ formState, field: { onBlur, onChange } }) => (
          <Input
            iconUrl="/icons/bwv-password-icon.svg"
            width={15}
            height={18}
            name="password"
            placeholder="Password"
            onBlur={()=>{
              if (testResult?.score < 4){
                toast((t) => (
                  <CustomToast
                    type={ToastTypes.WARNING}
                    title="Warning"
                    message={() => (
                      <PasswordStrengthRulesPopover />
                    )}
                    t={t}
                  />
                ))
              }
            }
          }
            onChange={(e: string) => {
              setPassword(e)
              onChange(e)
            }}
            formState={formState}
            type="password"
          />
        )}
      />

      <Controller
        control={control}
        name="confirmPassword"
        render={({ formState, field: { onBlur, onChange } }) => (
          <Input
            iconUrl="/icons/bwv-password-icon.svg"
            width={15}
            height={18}
            name="confirmPassword"
            placeholder="Confirm Password"
            onBlur={onBlur}
            onChange={onChange}
            formState={formState}
            type="password"
          />
        )}
      />

      <PasswordStrengthBarIndicator passwordLength={password} testResultScore={testResult?.score} />
      
      <div className="flex w-full justify-between pb-[26px] align-middle">
        <Controller
          control={control}
          name="acceptTermsAndConditions"
          render={({ field: { onChange } }) => (
            <>
              <Checkbox
                name="acceptTermsAndConditions"
                onChange={onChange}
                placeholder="Agree to the"
              />
              <button
                className=" mr-10 text-bwv-accent hover:text-bwv-accent-dark "
                type="button"
                onClick={() => {
                  setOpenTerms(true)
                }}
              >
                Terms And Conditions
              </button>
            </>
          )}
        />
        <TermsAndConditions
          isOpen={openTerms}
          onClose={() => {
            setOpenTerms(false)
          }}
        />

        <Controller
          control={control}
          name="subscribeOnNewsletter"
          render={({ field: { onChange } }) => (
            <Checkbox
              name="subscribeOnNewsletter"
              onChange={onChange}
              placeholder="Sign up for newsletter"
            />
          )}
        />
      </div>

      <Button
        text="Sign up"
        loadingText="Signing up"
        type="submit"
        disabled={!isValid || busy || testResult?.score < 4}
        loading={busy}
      />
    </form>
  )
}

export default SignUpForm
