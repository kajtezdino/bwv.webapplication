import { useState } from "react";
import { useForm, Controller } from "react-hook-form";
import Link from "next/link";
import Image from 'next/image';
import { yupResolver } from "@hookform/resolvers/yup";
import { PasswordResetSchema } from "../../../common/schemas";
import { Input, Button } from "../../../common/components";
import AuthApiService from "../api.service";
import { CheckIcon } from "@heroicons/react/solid";

const EmailInputView: React.FC = () => {
  const [busy, setBusy] = useState<boolean>(false);
  const [message, setMessage] = useState<string | null>(null)
  const { handleSubmit, control, formState: { errors, isValid } } = useForm<{email: string}>({
    mode: 'onChange',
    reValidateMode: 'onChange',
    resolver: yupResolver(PasswordResetSchema)
  });

  const onSubmit = async (data: { email: string }) => {
    setBusy(true)
    try {
      await AuthApiService.forgotPassword(data.email)
      setMessage("A password reset request email has been sent to your account. Please use link provided to change the password.")
    } catch (e) {
      console.error(e)
    }
    setBusy(false)
  }
  
  return (
    <section className="w-[540px]">
      <h1 className="font-semibold text-2xl pb-5 text-bwv-dark">Forgot password?</h1>
      <p className="font-medium text-base leading-[20px] text-bwv-dark-gray pb-8">Enter your email to recieve a reset password email.</p>

      { message ? (
        <div className="flex p-4 bg-bwv-accent text-white rounded-lg align-top gap-4">
          <CheckIcon width={24} height={24} />
          <p>{message}</p>
        </div>
      ) : (
        <form onSubmit={handleSubmit(onSubmit)}>
          <Controller
            control={control}
            name="email"
            render={({ formState, field: { onBlur, onChange } }) => (
              <Input
                iconUrl="/icons/bwv-email-icon.svg"
                width={18}
                height={18}
                name="email"
                type="email"
                placeholder="Your Email"
                onBlur={onBlur}
                onChange={onChange}
                formState={formState}
              />
            )}
          />

          <Button
            text="Send verification email"
            loadingText="Sending mail"
            type="submit"
            disabled={!isValid || busy}
            loading={busy}
          />
        </form>
      )}

      <p className="pt-4 leading-[20px] font-medium text-bwv-gray">
        <Link href="/auth/sign-in">
          <a className="text-bwv-accent hover:text-bwv-accent-dark transition-all">
            <Image
              src="/icons/bwv-arrow-left-icon.svg"
              width={7}
              height={12}
              alt="Back to sign page icon"
            />
            <span className="pl-2">Back to Sign In</span>
          </a>
        </Link>
      </p>
    </section>
  )
}

export default EmailInputView;