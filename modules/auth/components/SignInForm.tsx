import { useEffect, useState } from "react";
import { useForm, Controller } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import { ISignInFormInput } from "../../../common/models";
import {
  Checkbox,
  Input,
  Button,
  CustomToast
} from '../../../common/components';
import Link from "next/link";
import { SignInSchema } from "../../../common/schemas";
import { signIn, useSession } from "next-auth/react";
import { useRouter } from 'next/router';
import AuthApiService from "../api.service";
import toast from "react-hot-toast";
import { ToastTypes, UserRoles } from "../../../common/enums";

const SignInForm: React.FC<{ token?: string, email?: string }> = ({
  token,
  email
}) => {
  const {data: session} = useSession()
  const router = useRouter();
  const [busy, setBusy] = useState<boolean>(false);
  const { handleSubmit, control, formState: { errors, isValid } } = useForm<ISignInFormInput>({
    mode: 'onChange',
    reValidateMode: 'onChange',
    defaultValues: {
      email: email
    },
    resolver: yupResolver(SignInSchema)
  });

  useEffect(() => {
    const userSession: any = session
    if (userSession) {
      router.push(userSession?.user?.roles === UserRoles.ADMIN ? '/admin/users' : '/')
    }
  }, [router, session])

  const onSubmit = async (data: any) => {
    setBusy(true);
  
    token && email && await AuthApiService.confirmEmail({email: email, token: token})
    const res: any = await signIn('credentials',
      {
        email: data.email,
        password: data.password,
        rememberMe: data.rememberMe,
        callbackUrl: `${window.location.origin}`, 
        redirect: false,
      }
    );

    if (res.error) {
      setBusy(false);

      const error = res.error.split(':')[1]
      
      toast((t) => (
        <CustomToast
          type={ToastTypes.ERROR}
          title="Error"
          message={error}
          t={t}
        />
      ))
    }
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Controller
        control={control}
        name="email"
        render={({ formState, field: { onBlur, onChange } }) => (
          <Input
            iconUrl="/icons/bwv-email-icon.svg"
            width={18}
            height={18}
            name="email"
            type="email"
            placeholder="Your Email"
            onBlur={onBlur}
            onChange={onChange}
            formState={formState}
            defaultValue={email ? email : null}
            readOnly={!!email}
          />
        )}
      />

      <Controller
        control={control}
        name="password"
        render={({ formState, field: { onBlur, onChange } }) => (
          <Input
            iconUrl="/icons/bwv-password-icon.svg"
            width={15}
            height={18}
            name="password"
            placeholder="Password"
            onBlur={onBlur}
            onChange={onChange}
            formState={formState}
            type="password"
          />
        )}
      />

      <div className="flex w-full justify-between align-middle pb-[26px] pt-[26px]">
        <Controller
          control={control}
          name="rememberMe"
          render={({ field: { onChange } }) => (
            <Checkbox
              name="rememberMe"
              onChange={onChange}
              placeholder="Remember me"
            />
          )}
        />
      
        <Link href="/auth/password-reset">
          <a className="font-semibold leading-5 text-bwv-accent hover:text-bwv-accent-dark transition-all">Forgot password?</a>
        </Link>
      </div>

      <Button
        text="Sign in"
        loadingText="Signing in"
        type="submit"
        disabled={!isValid || busy}
        loading={busy}
      />
    </form>
  )
}

export default SignInForm;