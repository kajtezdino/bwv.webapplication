import { useEffect, useState } from "react"
import { Controller, useForm } from "react-hook-form";
import Image from 'next/image';
import { yupResolver } from "@hookform/resolvers/yup";
import { NewPasswordSchema } from "../../../common/schemas";
import { 
  Input, 
  Button, 
  CustomToast, 
  PasswordStrengthBarIndicator, 
  PasswordStrengthRulesPopover 
} from "../../../common/components";
import zxcvbn from 'zxcvbn';
import Link from "next/link";
import AuthApiService from "../api.service";
import { CheckIcon } from "@heroicons/react/solid";
import toast from "react-hot-toast";
import { ToastTypes } from "../../../common/enums";

interface IResetPasswordForm {
  password: string
  confirmPassword: string
}

interface INewPasswordViewParams {
  token: string
  email: string
}

const NewPasswordView: React.FC<INewPasswordViewParams> = ({
  token,
  email
}) => {
  const [busy, setBusy] = useState<boolean>(false);
  const [password, setPassword] = useState('')
  const [testResult, setTestResult] = useState<any>(null)
  const [message, setMessage] = useState<string | null>(null)

  useEffect(() => {
    setTestResult(zxcvbn(password))
  }, [password])

  const { handleSubmit, control, formState: { errors, isValid } } = useForm<IResetPasswordForm>({
    mode: 'onChange',
    reValidateMode: 'onChange',
    resolver: yupResolver(NewPasswordSchema)
  });

  const onSubmit = async (data: IResetPasswordForm) => {
    setBusy(true)
    try {
      await AuthApiService.resetPassword({
        password: data.password,
        confirmPassword: data.confirmPassword,
        email: email,
        code: token
      })
      setMessage("You have successfully changed your password. Click on 'Back to Sign In' to login into app.")
    } catch (e) {
      console.error(e)
    }
    setBusy(false)
  }

  return (
    <section className="w-[540px]">
      <h1 className="font-semibold text-2xl pb-5 text-bwv-dark">Set new password</h1>
      <p className="font-medium text-base leading-[20px] text-bwv-dark-gray pb-8">Enter new password</p>

      { message ? (
        <div className="flex p-4 bg-bwv-accent text-white rounded-lg align-top gap-4">
          <CheckIcon width={24} height={24} />
          <p>{message}</p>
        </div>
      ) : (
        <form onSubmit={handleSubmit(onSubmit)}>
          <Controller
            control={control}
            name="password"
            render={({ formState, field: { onBlur, onChange } }) => (
              <Input
                iconUrl="/icons/bwv-password-icon.svg"
                width={15}
                height={18}
                name="password"
                placeholder="Password"
                onBlur={()=>{
                  if (testResult?.score < 4){
                    toast((t) => (
                      <CustomToast
                        type={ToastTypes.WARNING}
                        title="Warning"
                        message={() => (
                          <PasswordStrengthRulesPopover />
                        )}
                        t={t}
                      />
                    ))
                  }
                }}
                onChange={(e: string) => { setPassword(e); onChange(e) }}
                formState={formState}
                type="password"
              />
            )}
          />

          <Controller
            control={control}
            name="confirmPassword"
            render={({ formState, field: { onBlur, onChange } }) => (
              <Input
                iconUrl="/icons/bwv-password-icon.svg"
                width={15}
                height={18}
                name="confirmPassword"
                placeholder="Confirm Password"
                onBlur={onBlur}
                onChange={onChange}
                formState={formState}
                type="password"
              />
            )}
          />

          <PasswordStrengthBarIndicator passwordLength={password} testResultScore={testResult?.score} />

          <Button
            text="Save new password"
            loadingText="Saving password"
            type="submit"
            disabled={!isValid || busy || testResult?.score < 4}
            loading={busy}
          />
        </form>
      )}
      <p className="pt-4 leading-[20px] font-medium text-bwv-gray">
        <Link href="/auth/sign-in">
          <a className="text-bwv-accent hover:text-bwv-accent-dark transition-all">
            <Image
              src="/icons/bwv-arrow-left-icon.svg"
              width={7}
              height={12}
              alt="Back to sign page icon"
            />
            <span className="pl-2">Back to Sign In</span>
          </a>
        </Link>
      </p>
    </section>
  )
}

export default NewPasswordView;