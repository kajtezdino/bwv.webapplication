module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './common/components/**/*.{js,ts,jsx,tsx}',
    './common/layouts/**/*.{js,ts,jsx,tsx}',
    './modules/**/*.{js,ts,jsx,tsx}',
    './widgets/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {
      colors: {
        'bwv-accent': '#87CA47',
        'bwv-accent-light': '#f2f9ea',
        'bwv-accent-dark': '#66a12d',
        'bwv-error': '#FF5B36',
        'bwv-light-gray': '#F2F2F2',
        'bwv-gray': '#C1C7D0',
        'bwv-dark-gray': '#8E98A9',
        'bwv-dark': '#252525',
        'bwv-dark-light': '#25252580',
        'bwv-error-dark': '#cd3613',
        'bwv-label': '#B8B8B8',
        'bwv-white-dirty': '#F8F8F8',
        'bwv-orange': '#FFB703',
        'bwv-orange-light': '#FFB70333',
        'bwv-accent-medium':'#87CA4733',
        'bwv-red':'#ED1C24',
        'bwv-orange-dark':'#F15A29',
        'bwv-yellow':'#FFF200'
      },
      boxShadow: {
        error: '0px 4px 16px 4px rgba(255, 91, 54, .05)',
      }
    }
  }
}
