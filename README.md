![BWV Logo](https://bwv-webapplication-git-master-kajtezdino.vercel.app/_next/image?url=%2Fimages%2Flogo.png&w=640&q=75 "Logo")
# BWV Web Application 

## Tec stack
The project uses [Tailwind CSS](https://tailwindcss.com/) for the styling. State management is done using [Recoil](https://recoiljs.org/).
[Jest](https://jestjs.io/) is used for unit testing. HTTP helper util uses Axios. [NextAuth.js](https://next-auth.js.org/) controlls authentication and user client sessions. It will be used also for Google and Facebook sign-ins. 

## Project structure
- **pages**: pages files that wrap content
- **pages/api/[...nextauth].js**: authentication providers
- **modules**: structure copies the structure from pages folder. This is where content for pages is created. every module should have its own *api.service*
- **common**: this is where shared components, enums, hocs, models, schemas, utils and layouts are created. Every folder has its own *index.ts* file that exports every item from that folder.
- **widgets**: all widget files
- **store**: atoms, selectors
- **swr-state**: reusable useSWR files
- **public**: icons and images
- **styles**: global css file for custom css classes

## Project setup
All packages are added with [yarn](https://yarnpkg.com/).
To start working on the project first install dependancies and than start the development server.

```bash
# Install dependancies
yarn

# Start dev server
yarn dev
```

## Testing
Unit test files are located in *__tests__* folder. Structure inside that folder should be similar to one found in pages and modules.

```bash
# Run tests
yarn test
```

## Api calls
**http** helper wraps Axios. Every api call method shoud be defined inside modules api.service.

```ts
/** 
 * Example api service class.
 * 
 * @export
 * ApiService instance
 */
class ApiService {
  methodName = (params: RequestParamsModel): Promise<ResponseModel> => 
    http<ResponseModel>({
      url: 'url', // URL - check proxy settings in sub section
      method: 'get, post, put, patch, delete', // Any HTTP method
      body: params // If method is not get
    })
}

export default new ApiService();
```
### Proxy
To avoid CORS proxy settings are added to *next.config.js*.
API urls of the BWV microservices start with ``/apis/`` and to with ``/api/`` because next.js api routes are accessible trough urls that start with that.
```js
async rewrites() {
  return [
    {
      source: '/apis/:path*',
      destination: process.env.NEXT_PUBLIC_BASE_API_URL + '/api/:path*'
    }
  ]
}
```

## Authorization
### Session handling
NextAuth.js provides authentication solution for this project. Credentials provider is defined inside `pages/api/[...nextauth].js` file. It is a custom provider that handles basic email-password sign in.
JWT token from the API response is stored inside `session` object which is available troughout the application because `SessionProvider` wraps the app (check `_app.tsx`).

### Page protection
Some pages in this app are public. But, there are those that need to be protected behind user session and some have extra layer of protection using user roles to determine whether those are accessible to the logged-in user. *404 Page not found* message is be displayed if user tries to go to protected page and security conditions are not met.
Logic that controls and determines page security can be extended. For now, it only supports user session check and user roles match (check `Auth` function in `_app.tsx`).
Check the example below that shows how to setup page authorization.

```ts
/**
 * Admin dashboard page is protected behind user session and requires 
 * user to have Administrator role.
 * 
 * @page
 * pages/admin/[id].tsx
 */

AdminDashboard.auth = {
  roles: [UserRoles.ADMIN]
}
```

## Toaster notifications
Application uses [react-hot-toast](https://react-hot-toast.com/) library.
`Toaster component` is placed inside *_app.tsx*. This way we can create toast notification from any component in the application. Notifications use custom transitions done with [Transition](https://headlessui.dev/react/transition) component from `HeadlessUI`.

### Toast notification creation
Toast notifications are created by calling `toast((t) => (<CustomToast />)`.

```tsx
import toast from "react-hot-toast";
import { CustomToast } from "../../common/components";
import { ToastTypes } from "../../common/enums";

/**
 * CustomToast - React component
 * @prop @required type
 * @prop @required title
 * @prop @required message
 * @prop @required t
 */
toast((t) => (
  <CustomToast
    type={ToastTypes.INFO}
    title="Notification title"
    message="Notification message"
    t={t}
  />
)
```

Message can be also a custom component:
```tsx
toast((t) => (
  <CustomToast
    type={ToastTypes.INFO}
    title="Notification title"
    message={() => (
      <div>
        We&rsquo;ve updated a few things.
        {" "}
        <Example />
      </div>
    )}
    t={t}
  />
)
```

## DropdownMenu component
Application uses [@headlessui/react](https://headlessui.dev/react/menu) library.
`DropdownMenu component` is placed inside *common/components*. This way we can use dropdown from any component in the application. DropdownMenu component use custom transitions done with [Transition](https://headlessui.dev/react/transition) component from `HeadlessUI`.

### DropdownMenu creation
```tsx
import { DropdownMenu } from "../../common/components";

DropdownMenu is created by calling `<DropdownMenu ...{props} />`.

/**
 * DropdownMenu - React component
 * @prop @required menuBtn
 * @prop @required menuItems
 * @prop width
 */

```
menuBtn can be also a custom component:
```tsx
<DropdownMenu
  menuBtn={() => (
      <div>
        <Example />
      </div>
    )}
  menuItems={
    name: "Item name",
    onClick: () => {}
    disabled: false
  }
  width="w-32"
/>
```