import { http } from "../../common/utils/http";
import { IWeatherWidgetResponse } from './../../common/models'

class WeatherWidgetApiService {
  getWeather = (params: { lat: number, lon: number } | null): Promise<IWeatherWidgetResponse> =>
    http<IWeatherWidgetResponse>({
      url: `https://api.openweathermap.org/data/2.5/weather?lat=${params?.lat}&lon=${params?.lon}&units=metric&appid=3d6eef9e0d4e37d8f2b7735f2cce8cd0`,
      method: 'get'
    })
}

export default new WeatherWidgetApiService()