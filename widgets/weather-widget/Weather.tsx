import Image from 'next/image'
import { useEffect, useMemo, useState } from 'react'
import toast from 'react-hot-toast'
import Moment from 'react-moment'
import { CustomToast } from '../../common/components'
import { ToastTypes } from '../../common/enums'
import WeatherWidgetApiService from './api.service'
import { convertToFahrenheit } from './utils/convertToFahrenheit'

interface IWeatherType {
  location: string
  description: string
  temp: number
  sunrise: number
  sunset: number
}

interface IUrlParams {
  lat: number
  lon: number
}

type TempType = 'celsius' | 'fahrenheit'

interface IMetric{
  metric: TempType
  symbol:string
}[]

const WeatherWidget: React.FC = () => {
  const [weather, setWeather] = useState<IWeatherType | null>(null)
  const [icon, setIcon] = useState<string>('bwv-sunny-icon')
  const [unit, setUnit] = useState<TempType>('celsius')

  const metrics: IMetric[] =[
    {metric:'celsius', symbol:'°C'},
    {metric:'fahrenheit', symbol:'°f'}
  ]
  
  const fetchWeather = async (params: IUrlParams) => {
    try {
      const response = await WeatherWidgetApiService.getWeather(params)

      setWeather({
        location: response.name,
        description: response.weather[0].description,
        temp: response.main.temp,
        sunrise: response.sys.sunrise,
        sunset: response.sys.sunset,
      })
      setIcon(response.weather[0].icon)
    } catch (error) {
      toast((t) => (
        <CustomToast
          type={ToastTypes.WARNING}
          title="Warning"
          message="There is a problem with the Weather Widget. Weather is not updated!"
          t={t}
        />
      ))
    }
    
  }
 

  useEffect(() => {
    navigator.geolocation.getCurrentPosition((position) => {
    const lat = position.coords.latitude
    const lon = position.coords.longitude

      fetchWeather({lat, lon})
    })
  }, [])

  const weatherByUnit = useMemo(() => {
    if (!weather?.temp) {
      return 0
    }

    if (unit === 'fahrenheit') {
      return convertToFahrenheit(weather?.temp)
    }

    return weather?.temp
  }, [unit, weather?.temp])

  return (
    <div className="mb-[24px] flex flex-col">
      <p className="mb-[8px] text-xs font-semibold uppercase text-[#A0A0A0]">
        Weather
      </p>
      <div className="flex flex-col rounded-[10px] bg-[#f8f8f8] p-[12px]">
        {!weather ? (
          <p>Data currently unavailable</p>
        ) : (
          <>
          <div className="mb-[12px] flex items-center">
          <Image
            src={`/icons/weather/bwv-${icon}.svg`}
            width={67}
            height={67}
            alt={weather.description}
          />
          <span className="ml-[8px] mr-[16px] text-[12px]">
            <p className="font-medium">{weather.location}</p>
            <p className="font-light">{weather.description}</p>
          </span>
          <div className="flex items-center">
            <p className="mr-[4px] w-[40px] text-[30px] font-light">
              {Math.round(weatherByUnit)}
            </p>
            <span className="flex cursor-pointer flex-col text-[14px]">
              {metrics.map( metric => (
                <p
                className={unit === metric.metric ? 'opacity-100' : 'opacity-25'}
                onClick={() => setUnit(metric.metric)}
                key={metric.symbol}
              >
                {metric.symbol}
              </p>
              ))}
            </span>
          </div>
        </div>
        <div className="flex">
          <div className="flex items-center">
            <Image
              src="/icons/bwv-sunrise-icon.svg"
              alt="sunrise icon"
              width={25}
              height={16}
            />
            <Moment
              className="ml-[10px] mr-[24px] text-[10px] font-medium leading-none"
              format="hh:mm A"
            >
              {weather.sunrise * 1000}
            </Moment>
            <Image
              src="/icons/bwv-sunset-icon.svg"
              alt="sunrise icon"
              width={25}
              height={20}
            />
            <Moment
              className="ml-[10px] text-[10px] font-medium leading-none"
              format="hh:mm A"
            >
              {weather.sunset * 1000}
            </Moment>
          </div>
        </div>
          </>  
        )}
        
      </div>
    </div>
  )
}

export default WeatherWidget
