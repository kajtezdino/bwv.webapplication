import { getSession } from "next-auth/react";
import useSWR from "swr";
import UserProfileApiService from '../modules/user/api.service';

export const useUser = () => {
  const fether = async() => {
    const session: any = await getSession();
    return await UserProfileApiService.getUserProfile((session?.user as any).userId)
  }

  const {data: user, mutate: setUser} = useSWR('user', fether, { fallbackData: {} })

  return [user, setUser]
}